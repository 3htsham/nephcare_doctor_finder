import 'dart:typed_data';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/config/my_firebase.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/tracking_model.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:location/location.dart' as l;

class TrackingController extends ControllerMVC {

  User currentUser;
  List<TrackingModel> trackings = <TrackingModel>[];


  TrackingModel trackingDetails;

  String trackingId;
  String destination="";

  MarkerId myId = MarkerId("Nurse Location");

  GoogleMapPolyline mapPolyline = GoogleMapPolyline(apiKey: GlobalVariables.DIRECTIONS_API_KEY);

  GoogleMapController controller;
  BitmapDescriptor customIcon;
  bool isMapCreated = false;
  String mapStyle;
  List<Marker> allMarkers = [];
  List<Polyline> polylines = <Polyline>[];
  List<LatLng> routeCoords;

  bool isFirstTime = true;

  GlobalKey<ScaffoldState> scaffoldKey;

  TrackingController() {
    this.scaffoldKey = GlobalKey<ScaffoldState>();
  }

  getMyTrackings() async {
    MyFirestore.getMyTrackingRooms(this.currentUser.id.toString()).listen((_querySnap){
      setState((){
        this.trackings.clear();
      });
      if(_querySnap.docs.length >0 ) {
        _querySnap.docs.forEach((_docSnap) {
          Map<String, dynamic> data = _docSnap.data();

          TrackingModel _trkDetls = TrackingModel(
            id: data['id'].toString(),
            nursePro: User(id: data['nursepro']['id'], name: data['nursepro']['name'], image: data['nursepro']['img'], categoryName: data['nursepro']['categoryName']),
            senderPro: User(id: data['senderpro']['id'], name: data['senderpro']['name'], image: data['senderpro']['img'], type: data['senderpro']['type']),
            sender: data['sender'] as GeoPoint,
            nurse: data['nurse'] as GeoPoint,
              description: data["description"],
              pay: data['pay'],
              hireId: data['hireId'],
              isInstant: data['isInstant'] ?? false,
              job: data['job'] != null
                  ? Job(
                      id: data['job']['id'],
                      hours: data['job']['hours'],
                      description: data['job']['description'],
                      title: data['job']['title'])
                  : null);

          setState((){
            this.trackings.add(_trkDetls);
          });

        });
      }
    }, onError: (e){
      print(e);
    });
  }


  getTracking() async {

    MyFirestore.getTrackingRoom(this.trackingId).listen((_docSnap) {
      if(_docSnap.exists) {

        Map<String, dynamic> data = _docSnap.data();

        TrackingModel _trkDetls = TrackingModel(
          id: data['id'].toString(),
          nursePro: User(id: data['nursepro']['id'], name: data['nursepro']['name'], image: data['nursepro']['img'], categoryName: data['nursepro']['categoryName']),
          senderPro: User(id: data['senderpro']['id'], name: data['senderpro']['name'], image: data['senderpro']['img'], type: data['senderpro']['type']),
          sender: data['sender'] as GeoPoint,
          nurse: data['nurse'] as GeoPoint,
            description: data["description"],
            pay: data['pay'],
            hireId: data['hireId'],
            isInstant: data['isInstant'] ?? false,
            job: data['job'] != null
                ? Job(
                id: data['job']['id'],
                hours: data['job']['hours'],
                description: data['job']['description'],
                title: data['job']['title'])
                : null
        );

        setState((){
          this.trackingDetails = _trkDetls;
        });

        LatLng _dest = LatLng(_trkDetls.nurse.latitude, _trkDetls.nurse.longitude);
        getAddressAndLocation(_dest);

        if(this.isFirstTime) {
          setState((){
            this.isFirstTime = false;
          });
          LatLng _dest = LatLng(_trkDetls.sender.latitude, _trkDetls.sender.longitude);
          this.addMarker(_dest);
        }

        addMarkerNurse(_dest);

      } else {
        Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, arguments: 2);
      }
    },
        onError: (e){
          print(e);
        }, onDone: (){
        });
  }

  getLocationPermission() async {
    if(await l.Location.instance.hasPermission() == l.PermissionStatus.granted) {
    } else {
      l.Location().requestPermission().then((value) {
        if (value == l.PermissionStatus.denied) {
          getLocationPermission();
        } else {
        }
      });
    }
  }
  getRouteCoordinates() async {

    try {
      routeCoords = await mapPolyline.getCoordinatesWithLocation(
          origin: LatLng(this.trackingDetails.nurse.latitude,
              this.trackingDetails.nurse.longitude),
          destination: LatLng(this.trackingDetails.sender.latitude,
              this.trackingDetails.sender.longitude),
          mode: RouteMode.driving
      );
    } catch(e) {
      print(e);
    }

    polylines.clear();
    setState(() {
      polylines.add(Polyline(
          polylineId: PolylineId("Route1"),
          color: Colors.blue,
          startCap: Cap.roundCap,
          endCap: Cap.buttCap,
          width: 4,
          points: routeCoords
      ));
    });
  }

  moveCamera(LatLng _loc) {
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: this.trackingDetails == null
          ? LatLng(40.71552837085483, -73.98514218628407)
          : LatLng(double.parse(_loc.latitude.toString()),
          double.parse(_loc.longitude.toString())),
      zoom: 17.0,)));

    getRouteCoordinates();
  }

  void addMarker(LatLng _loc){
    getBytesFromAsset('assets/img/mymappin.png', 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
      setState((){
        allMarkers.add(Marker(
          icon: customIcon,
          markerId: MarkerId("My location"),
          draggable: false,
          infoWindow:
          InfoWindow(title: "You're here", snippet: "your location"),
          position: LatLng(double.parse(_loc.latitude.toString()),
              double.parse(_loc.longitude.toString())),));
      });
    });
  }

  void addMarkerNurse(LatLng _loc) {
    String marker = 'assets/img/mappin.png';
    if(this.trackingDetails.nursePro.categoryName.toString().toLowerCase() == "babysitter") {
      marker = 'assets/img/bmappin.png';
    }
    getBytesFromAsset(marker, 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
      bool isUpdated = false;
      allMarkers.forEach((_mrkr) {
        if(_mrkr.markerId == myId) {
          isUpdated = true;
          setState((){
            _mrkr = Marker(
              icon: customIcon,
              markerId: myId,
              draggable: false,
              infoWindow:
              InfoWindow(title: this.trackingDetails.nursePro.name, snippet: "is here"),
              position: LatLng(double.parse(_loc.latitude.toString()),
                  double.parse(_loc.longitude.toString())),);
          });
        }
      });
      if(!isUpdated) {
        setState((){
          allMarkers.add(Marker(
            icon: customIcon,
            markerId: myId,
            draggable: false,
            infoWindow:
            InfoWindow(title: this.trackingDetails.nursePro.name, snippet: "is here"),
            position: LatLng(double.parse(_loc.latitude.toString()),
                double.parse(_loc.longitude.toString())),));
        });
      }
      moveCamera(_loc);
    });
  }

  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }


  initLocationController(){
    setCustomMapPin();
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  void setCustomMapPin() async {
    getBytesFromAsset('assets/img/mappin.png', 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
    });
  }
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png)).buffer.asUint8List();
  }

  void getAddressAndLocation(LatLng position) async {
    double latitude = position.latitude;
    double longitude = position.longitude;
    Coordinates coordinates = Coordinates(latitude, longitude);

    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

    var address = addresses.first.featureName +", " + addresses.first.locality + " " + addresses.first.countryName;
    print("Getting hospital/user location: " + address);
    setState((){
      this.destination = address;
    });
  }


}