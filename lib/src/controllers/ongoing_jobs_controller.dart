import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/my_firebase.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/job_track.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:doctor_finder/src/repositories/job_repository.dart' as jobRepo;
import 'package:doctor_finder/src/repositories/hire_repository.dart' as hireRepo;
import 'package:doctor_finder/src/repositories/notifications_repository.dart' as notiRepo;

class OnGoingJobController extends ControllerMVC {

  User currentUser;
  User nurse;
  bool isLoading = false;
  JobTrack jobToTrack;
  List<JobTrack> jobTracks = <JobTrack>[];

  bool showWithdrawDialog = false;
  bool showCompleteDialog = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  OnGoingJobController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  getCurrentUser() async {
    userRepo.getCurrentUser().then((value) {
      setState(() {
        currentUser = value;
      });
    });
  }


  void getUserDetails() async {
    userRepo.getUserProfile(this.jobToTrack.nurse.id.toString()).then((_user) {
      if (_user != null) {
        setState(() {
          this.nurse = _user;
        });
      }
    }, onError: (e) {
      print(e);
    });
  }

  getMyOnGoingJobs() async {
    userRepo.getCurrentUser().then((value) {
      setState((){
        currentUser = value;
      });

      MyFirestore.getMyJobTrackingRooms(this.currentUser.id.toString()).listen((_snaps){
        setState((){
          this.jobTracks.clear();
        });
        _snaps.docs.forEach((_doc) {
          Map<String, dynamic> data = _doc.data();

          JobTrack _track = JobTrack(id: data['id'], hireId: data['id'],
              isInstant: data['isInstant'] ?? false,
              description: data['description'], pay: data['pay'], status: data['status'],
              time:  DateTime.fromMillisecondsSinceEpoch(data['time']));

          _track.nurse = User(id: data['nursepro']['id'], name: data['nursepro']['name'],
              image: data['nursepro']['img'], categoryName: data['nursepro']['categoryName']);

          _track.sender = User(id: data['senderpro']['id'], name: data['senderpro']['name'],
              image: data['senderpro']['img']);

          if(data['job'] != null) {
            Job _job = Job(id: data['job']['id'], title: data['job']['title'], description: data['job']['description']);
            _track.job = _job;
          }

          setState((){
            this.jobTracks.add(_track);
          });

        });
      },
          onError: (e){
            print(e);
          },
          onDone: (){}
      );
    });
  }

  getJobToTrackDetails() async {

    MyFirestore.getJobTrackingRoom(this.jobToTrack.id).listen((_docSnap) {
      if(_docSnap.exists) {

        Map<String, dynamic> data = _docSnap.data();

        JobTrack _track = JobTrack(id: data['id'], hireId: data['id'],
            isInstant: data['isInstant'] ?? false,
            description: data['description'], pay: data['pay'], status: data['status'],
            time:  DateTime.fromMillisecondsSinceEpoch(data['time']));

        _track.nurse = User(id: data['nursepro']['id'], name: data['nursepro']['name'],
            image: data['nursepro']['img']);

        _track.sender = User(id: data['senderpro']['id'], name: data['senderpro']['name'],
            image: data['senderpro']['img']);

        if(data['job'] != null) {
          Job _job = Job(id: data['job']['id'], title: data['job']['title'],
              description: data['job']['description'], pay: data['pay']);
          _track.job = _job;
        }

        setState((){
          this.jobToTrack = _track;
        });


      } else {
        // Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, arguments: 2);
      }
    },
        onError: (e){
          print(e);
        }, onDone: (){
        });
  }
  
  withdrawHire() async {
    if(this.currentUser != null && this.nurse != null) {
      setState(() {
        isLoading = true;
        showWithdrawDialog = false;
      });
      if(this.jobToTrack.isInstant) {
        MyFirestore.finishJobTracking(this.jobToTrack.id);

        String title = "Job Hire Withdraw";
        String body = "${this.currentUser.name} withdraw your job hire";
        notiRepo.sendNotification(body, title, this.nurse.firebaseToken);
        Navigator.of(context).pop();
        return;
      }
      hireRepo.withdrawJobHire(this.jobToTrack.hireId.toString()).then((value) {
        setState(() {
          isLoading = false;
        });
        if (value) {
          MyFirestore.finishJobTracking(this.jobToTrack.id);

          String title = "Job Hire Withdraw";
          String body = "${this.currentUser.name} withdraw your job hire";
          notiRepo.sendNotification(body, title, this.nurse.firebaseToken);
          setState(() {
            isLoading = false;
          });
          Navigator.of(context).pop();
        } else {
          showToast("Error withdraw Hire, Please try later.");
        }
      },
          onError: (e) {
            setState(() {
              isLoading = false;
            });
            print(e);
            showToast("Error occurred while withdraw hire.");
          });
    }
  }
  
  completeHire() async {
    if(this.currentUser != null && this.nurse != null) {
      setState(() {
        isLoading = true;
        showCompleteDialog = false;
      });
      if(this.jobToTrack.isInstant) {
        MyFirestore.finishJobTracking(this.jobToTrack.id);

        String title = "Job Completed";
        String body = "${this.currentUser.name} mark your job as completed";
        notiRepo.sendNotification(body, title, this.nurse.firebaseToken);
        setState(() {
          isLoading = false;
        });
        Navigator.of(context).pop();
        return;
      }
      hireRepo.completeHire(this.jobToTrack.hireId.toString()).then((value) {
        setState(() {
          isLoading = false;
        });
        if (value) {
          MyFirestore.finishJobTracking(this.jobToTrack.id);

          String title = "Job Completed";
          String body = "${this.currentUser.name} mark your job as completed";
          notiRepo.sendNotification(body, title, this.nurse.firebaseToken);
          Navigator.of(context).pop();
        } else {
          showToast("Error completing job, Please try later.");
        }
      },
          onError: (e) {
            setState(() {
              isLoading = false;
            });
            print(e);
            showToast("Error occurred while completing job.");
          });
    }
  }

}