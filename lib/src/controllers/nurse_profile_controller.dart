import 'dart:typed_data';
import 'dart:ui';
import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/nurse_repository.dart' as nurseRepo;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class NurseProfileController extends ControllerMVC {

  User currentUser;
  User nurse;
  GoogleMapController controller;
  String mapStyle;
  List<Marker> allMarkers = [];

  GlobalKey<ScaffoldState> scaffoldKey;

  NurseProfileController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getCurrentUser() async {
    userRepo.getCurrentUser().then((user) {
      setState(() {
        this.currentUser = user;
      });
    });
  }

  getProfile() async {
    nurseRepo.getNurseProfile(this.nurse.id).then((_nurse) {
      if (_nurse != null) {
        setState(() {
          this.nurse = _nurse;
        });
      }
    }, onError: (e) {
      print(e);
    });
  }

  addToFavorite(var userId) async {
    nurseRepo.addToFavorite(userId).then((_added) {
      if(_added) {
        setState((){this.nurse.isFavorite = true;});
      }
    });
  }



  initLocationController(){
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }
  moveCamera() {
    if (nurse.long != null && nurse.lat != null) {
      String marker = 'assets/img/mappin.png';

      if(nurse.category != null && nurse.category.name.toString().toLowerCase() == "babysitter") {
        marker = 'assets/img/bmappin.png';
      }

      getBytesFromAsset(marker, 60).then((value) {
        var customIcon = BitmapDescriptor.fromBytes(value);
        setState(() {
          allMarkers.clear();
          allMarkers.add(Marker(
            icon: customIcon,
            markerId: MarkerId('${nurse.id}'),
            draggable: false,
            infoWindow: InfoWindow(title: nurse.name, snippet: "is here"),
            position: LatLng(double.parse(nurse.lat.toString()),
                double.parse(nurse.long.toString())),
          ));
        });
      });
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(double.parse(nurse.lat.toString()),
              double.parse(nurse.long.toString())),
          zoom: 13.0,
          bearing: 45.0,
          tilt: 45.0)));
    }
  }
  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png)).buffer.asUint8List();
  }

}