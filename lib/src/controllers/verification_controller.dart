import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as repo;

class VerificationController extends ControllerMVC {
  User currentUser;

  bool isLoading = false;
  bool isVerified = false;
  TextEditingController controller;

  GlobalKey<ScaffoldState> scaffoldKey;

  VerificationController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.controller = TextEditingController();
  }


  verify() async {
    if(controller.text == null || controller.text.length <1) {
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Enter verification code please")));
    } else {
      setState((){isLoading = true;});
      repo.verifyEmail(this.currentUser.id.toString(), controller.text.toString()).then((value) {
        if(value != null) {
          setState((){isLoading = true; isVerified = true;});
          //Register Success
          Future.delayed(Duration(milliseconds: 200), (){
            if((value.lat != null && value.long != null) &&
                (((value.lat != 0 || value.lat != "0") && (value.long != 0 || value.long != "0"))
                    || (value.lat.length > 0 && value.long.length > 0))) {
              Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, arguments: 2);
            }
            else {
              Navigator.of(context).pushNamedAndRemoveUntil("/UpdateCurrentLocation",
                      (Route<dynamic> route) => false,
                  arguments: RouteArgument(currentUser: value));
            }
          });
        } else {
          setState((){isLoading = false;});
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Invalid Verification Code")));
        }
      },
          onError: (e){
            setState((){isLoading = false;});
            print(e);
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
          });
    }
  }
}