
import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/models/user_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:doctor_finder/src/repositories/settings_repository.dart' as settingsRepo;

class UserController extends ControllerMVC {

  User currentUser;
  GoogleMapController controller;
  String mapStyle;
  List<Marker> allMarkers = [];

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> loginFormKey;

  User user = User();
  bool isLoading = false;
  bool displayPassword = false;

  List<UserType> userTypes = <UserType>[];
  UserType selectedType;

  UserController() {
    loginFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    userTypes.add(UserType(typeValue: GlobalVariables.userType, text: "Get services at home"));
    userTypes.add(UserType(typeValue: GlobalVariables.hospitalType, text: "Get services for my hospital"));
    selectedType = userTypes[0];

    currentUser = User(id: 0, name: "Anne Alison",
        email: "anne@gmail.com", phone: "+1 88 1236 1211",
        image: "https://writestylesonline.com/wp-content/uploads/2019/01/What-To-Wear-For-Your-Professional-Profile-Picture-or-Headshot.jpg",
        address: "67-29 Metropolitan Ave, Flushing, NY 11379, USA", latitude: 40.714859, longitude: -73.891393,
        createdOn: DateTime.parse("20180601T170555") );
  }

  getCurrentUser() async {
    userRepo.getCurrentUser().then((value) {
      setState((){
        this.currentUser = value;
      });
    });
  }


  void login() async {
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      setState((){isLoading = true;});
      if(await settingsRepo.isInternetConnection()) {
        userRepo.login(user).then((value) {
          setState((){isLoading = false;});
          if (value != null) {
            //Login Success
            if(value.genre == 1 || value.genre == "1"){
              if(value.verified) {
                if((value.lat != null && value.long != null) &&
                    (((value.lat != 0 || value.lat != "0") && (value.long != 0 || value.long != "0"))
                        || (value.lat.length > 0 && value.long.length > 0))) {
                  Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, arguments: 2);
                }
                else {
                  Navigator.of(context).pushNamedAndRemoveUntil("/UpdateCurrentLocation",
                          (Route<dynamic> route) => false,
                      arguments: RouteArgument(currentUser: value));
                }
              } else {
                //Go for Verification
                Navigator.of(context).pushNamedAndRemoveUntil(
                    "/VerifyEmail",
                        (Route<dynamic> route) => false,
                    arguments: RouteArgument(currentUser: value));
              }

            } else {
              userRepo.logout();
              scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("You're not allowed to Login to this app")));
            }

          } else {
            scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("Invalid Credentials")));
          }
        }, onError: (e) {
          print(e);
          setState((){isLoading = false;});
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
        });
      } else {
        setState((){isLoading = false;});
        scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("Verify your internet connection")));
      }
    }
  }

  void register() async {
    if(loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      user.type = selectedType.typeValue;
      setState((){isLoading = true;});
      if(await settingsRepo.isInternetConnection()) {
        userRepo.register(user).then((value) {
          setState((){isLoading = false;});
          if (value != null && value.apiToken != null) {

            if(value.verified) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  "/UpdateCurrentLocation",
                      (Route<dynamic> route) => false,
                  arguments: RouteArgument(currentUser: value));
            } else {
              //Go for Verification
              Navigator.of(context).pushNamedAndRemoveUntil(
                  "/VerifyEmail",
                      (Route<dynamic> route) => false,
                  arguments: RouteArgument(currentUser: value));
            }

          } else {
            scaffoldKey.currentState.showSnackBar(
                SnackBar(content: Text("Unable to Signup")));
          }
        }, onError: (e) {
          print(e);
          setState((){isLoading = false;});
          scaffoldKey.currentState.showSnackBar(
              SnackBar(content: Text("Unable to Signup")));
        });
      } else {
        setState((){isLoading = false;});
        scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("Verify your internet connection")));
      }
    }
  }


  initLocationController(){
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }
  moveCamera() {
    if(currentUser.long!= null && currentUser.lat != null) {
      setState(() {
        allMarkers.clear();
        allMarkers.add(Marker(
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          markerId: MarkerId('${currentUser.id}'),
          draggable: false,
          infoWindow:
          InfoWindow(title: currentUser.name, snippet: "You're here"),
          position: LatLng(double.parse(currentUser.lat.toString()),
              double.parse(currentUser.long.toString())),));
      });
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(double.parse(currentUser.lat.toString()),
              double.parse(currentUser.long.toString())),
          zoom: 13.0,
          bearing: 45.0,
          tilt: 45.0)));
    }
  }

}