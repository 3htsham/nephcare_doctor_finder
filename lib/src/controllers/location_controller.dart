import 'dart:typed_data';
import 'dart:ui';

import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:location/location.dart' as l;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:doctor_finder/src/repositories/nurse_repository.dart' as nurseRepo;

class LocationController extends ControllerMVC {

  User currentUser;
  GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: GlobalVariables.PLACES_API_KEY);
  var searchedText = "Search Location";
  bool isLocationPicked = false;
  bool isLoading = false;

  List<User> usersNearby = <User>[];

  GoogleMapController controller;
  BitmapDescriptor customIcon;
  bool isMapCreated = false;
  String mapStyle;
  List<Marker> allMarkers = [];

  PageController pageController;
  int prevPage;

  bool doneRequestingNearby = true;

  GlobalKey<ScaffoldState> scaffoldKey;

  LocationController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    getLocationPermission();
  }

  getNearbyNurses() async {
    Stream<User> stream = await nurseRepo.getNearbyNurses();
    stream.listen((_nurse) {
      setState((){
        usersNearby.add(_nurse);
      });
    },
        onError: (e){
      print(e);
        },
        onDone: (){
          updateMarkers();
        });
  }

  getLocationPermission(){
    l.Location().requestPermission().then((value){
      if(value == l.PermissionStatus.denied) {
        getLocationPermission();
      }
    });
  }

  initLocationController(){
    setCustomMapPin();
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
    pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(onScroll);
    // listenForTopRestaurants();
  }

  void setCustomMapPin() async {
    // customIcon = await BitmapDescriptor.fromAssetImage(
    //     ImageConfiguration(devicePixelRatio: 2.5),
    //     'assets/img/mappin.png');
    getBytesFromAsset('assets/img/mappin.png', 60).then((value) {
      customIcon = BitmapDescriptor.fromBytes(value);
    });
  }
  void onScroll() {
    if (pageController.page.toInt() != prevPage) {
      prevPage = pageController.page.toInt();
      moveCamera();
    }
  }

  Future<String> getJsonFile(String path) async {
    return await rootBundle.loadString(path);
  }
  void setMapStyle(String mapStyle) {
    controller.setMapStyle(mapStyle);
  }

  moveCamera() {
    if(controller != null) {
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: usersNearby.length < 1
              ? LatLng(40.71552837085483, -73.98514218628407)
              : LatLng(double.parse(
              usersNearby[pageController.page.toInt()].lat.toString()),
              double.parse(
                  usersNearby[pageController.page.toInt()].long.toString())),
          zoom: 14.0,
          bearing: 45.0,
          tilt: 45.0)));
    }
  }


  createMarker(context) {
    if (customIcon == null) {
      getBytesFromAsset('assets/img/mappin.png', 14).then((value) {
        customIcon = BitmapDescriptor.fromBytes(value);
      });
    }
  }


  void updateMarkers(){
    // setState((){ doneRequestingRestaurants = true; });
    String marker = 'assets/img/mappin.png';

    usersNearby.forEach((element) {

      if(element.category != null && element.category.name.toString().toLowerCase() == "babysitter") {
        marker = 'assets/img/bmappin.png';
      }

      getBytesFromAsset(marker, 60).then((value) {
        customIcon = BitmapDescriptor.fromBytes(value);
        setState((){
          allMarkers.add(Marker(
            icon: customIcon,
            markerId: MarkerId(element.name),
            draggable: false,
            infoWindow:
            InfoWindow(title: element.name, snippet: element.address),
            position: LatLng(double.parse(element.lat.toString()),
                double.parse(element.long.toString())),));
        });
      });

      moveCamera();

    });
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png)).buffer.asUint8List();
  }

  initiateLocationController(){
     setCustomMapPin();
    rootBundle.loadString('assets/map/new_style.txt').then((string) {
      mapStyle = string;
    });
  }
  // TO GET THE USER LOCATION
  void getUserLocation() async {
    print("Getting user location: 2");
    l.LocationData position = await l.Location().getLocation();
    LatLng pos = LatLng(position.latitude, position.longitude);
    getAddressAndLocation(pos);
  }
  moveCameraToUserPosition() {
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: usersNearby.length < 1
            ? LatLng(40.71552837085483, -73.98514218628407)
            : LatLng(usersNearby[pageController.page.toInt()].latitude, usersNearby[pageController.page.toInt()].longitude),
        zoom: 13.0,
        bearing: 45.0,
        tilt: 45.0)));
  }

  void getAddressAndLocation(LatLng position) async {
    double latitude = position.latitude;
    double longitude = position.longitude;
    currentUser.lat = latitude;
    currentUser.long = longitude;
    Coordinates coordinates = Coordinates(latitude, longitude);
  
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

    var address = addresses.first.featureName +", " + addresses.first.locality + " " + addresses.first.countryName;
    print("Getting user location: 34 " + address);
    updateCurrentMarker(position, address);
  }

  updateCurrentMarker(LatLng location, String address) {
    Marker m = Marker(markerId: MarkerId('1'),
          position: location,
          infoWindow: InfoWindow(
              title: address,
              snippet: "Go Here"
          ),
          icon: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueRed,
          )
      );
      setState(() {
        controller.animateCamera(CameraUpdate.newLatLng(location));
        searchedText = address;
        allMarkers.clear();
        allMarkers.add(m);
        isLocationPicked = true;
      });
  }
  //Display Location Address in Predictions/Suggestions
  Future<Null> displayPrediction(var p) async {
    if (p != null) {
      PlacesDetailsResponse detail = await places.getDetailsByPlaceId(p.placeId);
      var placeId = p.placeId;
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      print("Address is: 5 " + p.description);
      print("Address is: 6");
      print(lat);
      print(lng);
      updateCurrentMarker(LatLng(lat, lng), p.description);
      getAddressAndLocation(LatLng(lat, lng));
    }
  }
  ///When Registering
  void updateUser() async {
    setState((){isLoading = true;});
    userRepo.updateUser(currentUser).then((_user) {
      setState((){
        isLoading = false;
      });
      if(_user != null && _user.apiToken != null && _user.apiToken.length > 0) {
            Navigator.of(context).pushNamedAndRemoveUntil("/Pages", (Route<dynamic> route) => false, 
              arguments: 2);
      } else {
        showToast("Something went wrong");
      }
    }, 
    onError: (e){
      print(e);
      setState((){isLoading = false;});
      showToast("Error Updating Location");
    });
  }

  ///When Editing Profile From Profile
  void updateUserInApp() async {
    setState((){isLoading = true;});
    userRepo.updateUser(currentUser).then((_user) {
      setState((){
        isLoading = false;
      });
      if(_user != null) {
        Navigator.of(context).pop(true);
      } else {
        showToast("Something went wrong");
      }
    },
        onError: (e){
          print(e);
          setState((){isLoading = false;});
          showToast("Error Updating Location");
        });
  }

  Future<String> getAddressFRomLocation(LatLng position) async {
    double latitude = position.latitude;
    double longitude = position.longitude;
    Coordinates coordinates = Coordinates(latitude, longitude);

    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

    var address = addresses.first.featureName +", " + addresses.first.locality + " " + addresses.first.countryName;
    print("Getting user location: 34 " + address);
    return address;
  }

}