import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/config/my_firebase.dart';
import 'package:doctor_finder/src/models/hire_request.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/hire_repository.dart' as repo;
import 'package:doctor_finder/src/repositories/notifications_repository.dart' as notiRepo;

class HireController extends ControllerMVC {

  User nurse;
  User currentUser;
  Job job;
  String hireDetailsMessage = "";
  double hirePay = 0.0;
  bool isScheduled = false;
  String hireDate = "";
  String hireTime = "";

  bool isLoading = false;

  TextEditingController descriptionController;
  TextEditingController hirePayController;
  GlobalKey<FormState> hireFormKey;

  List<HireRequest> requests = <HireRequest>[];

  List<HireRequest> instantRequests = <HireRequest>[];

  HireRequest request;

  GlobalKey<ScaffoldState> scaffoldKey;

  HireController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    hireFormKey = new GlobalKey<FormState>();
    descriptionController = TextEditingController();
    hirePayController = TextEditingController();
  }

  sendHireRequest() async {
    if(this.currentUser.payMethod == null || this.currentUser.payMethod.cardNumber == null ||
        this.currentUser.balance == null || double.parse(this.currentUser.balance.toString()) < this.hirePay) {
      showLongToast("You must add/update your payment method from your wallet and add some amount to your wallet (Wallet balance must be greater than request pay)");
    }
    else if(this.currentUser.type == GlobalVariables.hospitalType && this.job == null) {
      showToast("Your must select a job to continue");
    } else {
      if(this.hireFormKey.currentState.validate()) {
        this.hireFormKey.currentState.save();
        setState((){ isLoading = true; });
        var _jobId = this.job != null ? this.job.id.toString() : null;

        repo.sendHireRequest(this.nurse.id.toString(), _jobId, hireDetailsMessage, hirePay.toString())
            .then((value) {
              if(value != null) {
                ///Now do Firestore stuff and trigger FCM notification
                Map<String, dynamic> hireRequestMap = Map<String, dynamic>();
                hireRequestMap['id'] = value;
                hireRequestMap['jobId'] = this.job != null ? this.job.id : '';

                if(this.job != null) {
                  Map<String, dynamic> jobMap = Map<String, dynamic>();
                  jobMap['id'] = this.job.id;
                  jobMap['title'] = this.job.title;
                  jobMap['description'] = this.job.description;
                  jobMap['hours'] = this.job.hours;

                  hireRequestMap['job'] = jobMap;
                }

                Map<String, dynamic> senderMap = Map<String, dynamic>();
                senderMap['id'] = this.currentUser.id;
                senderMap['name'] = this.currentUser.name;
                senderMap['image'] = this.currentUser.image;
                senderMap['type'] = this.currentUser.type;
                senderMap['location'] = GeoPoint(double.parse(this.currentUser.lat.toString()), double.parse(this.currentUser.long.toString()));

                Map<String, dynamic> nurseMap = Map<String, dynamic>();
                nurseMap['id'] = this.nurse.id;
                nurseMap['name'] = this.nurse.name;
                nurseMap['image'] = this.nurse.image;
                nurseMap['category'] = this.nurse.category.name;

                hireRequestMap['sender'] = senderMap;
                hireRequestMap['nurse'] = nurseMap;
                hireRequestMap['users'] = [this.currentUser.id.toString(), this.nurse.id.toString()];
                hireRequestMap['status'] = 'pending';

                hireRequestMap['description'] = hireDetailsMessage;
                hireRequestMap['pay'] = hirePay;

                hireRequestMap['scheduled'] = this.isScheduled;

                if(isScheduled) {
                  //dd/MM/yyyy HH:mm
                  var dateAndTime = this.hireDate + " " + this.hireTime;
                  DateFormat format = DateFormat("dd/MM/yyyy HH:mm");
                  DateTime tempDate = format.parse(dateAndTime);
                  print(format.format(tempDate));
                  var timeText = tempDate.millisecondsSinceEpoch;
                  hireRequestMap['schedule'] = timeText;
                }

                MyFirestore.createHireRequest(hireRequestMap, value);
                String notificationTitle = "${this.currentUser.name} sent you a hire request";
                notiRepo.sendNotification(hireDetailsMessage, notificationTitle, this.nurse.firebaseToken).then((value) {});
                
                setState((){ isLoading = false; });
                showToast("Hire request sent successfully");
                Navigator.of(scaffoldKey.currentContext).pop();

              } else {
                setState((){ isLoading = false; });
                showToast("Something went wrong");
              }
        },
          onError: (e){
              print(e);
              setState((){ isLoading = false; });
              scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify yur internet connection")));
          }
            );


      }
    }
  }


  getMyHireRequests() async {
    MyFirestore.getHireRequests(this.currentUser.id.toString()).listen((_snaps){
      requests.clear();
      _snaps.docs.forEach((_doc) {
        Map<String, dynamic> data = _doc.data();

        HireRequest _req = HireRequest(id: data['id'], description: data['description'], status: data['status'],);

        if(data['job'] != null) {
          Job _job = Job(id: data['job']['id'], title: data['job']['title'], description: data['job']['description']);
          _req.job = _job;
        }

        User _nurse = User(id: data['nurse']['id'], name: data['nurse']['name'],
            image: data['nurse']['image'], categoryName: data['nurse']['category']);

        _req.nurse = _nurse;

        User _sender = User(id: data['sender']['id'], name: data['sender']['name'],
            image: data['sender']['image'], type: data['sender']['type']);

        _req.sender = _sender;
        _req.time = DateTime.fromMillisecondsSinceEpoch(data['time']);

        setState((){
          this.requests.add(_req);
        });

      });
    },
      onError: (e){
      print(e);
      },
      onDone: (){}
    );
  }

  withdrawRequest() async {
    setState(() {
      isLoading = true;
    });
    repo.withdrawHireRequest(this.request.id).then((value) {
      if(value) {
        MyFirestore.deleteHireRequest(this.request.id);
        Navigator.of(context).pop();
      }
    },
        onError: (e){
      print(e);
      showToast("Error Withdrawing Hire Request");
        });
  }

  //INSTANT REQUESTS
  getMyInstantHireRequests() async {
    MyFirestore.getInstantHireRequests(this.currentUser.id.toString()).listen((_snaps){
      instantRequests.clear();
      _snaps.docs.forEach((_doc) {
        Map<String, dynamic> data = _doc.data();

        HireRequest _req = HireRequest(
          id: data['id'],
          category: data['category'],
          categoryName: data['categoryName'],
          status: data['status'],
          isInstant: true,
        );

        User _sender = User(id: data['sender']['id'], name: data['sender']['name'],
            image: data['sender']['image'], type: data['sender']['type']);

        GeoPoint _senderGeoPoint = (data['sender']['location'] as GeoPoint);
        if(_senderGeoPoint != null) {
          LatLng _senderLoc = LatLng(
              _senderGeoPoint?.latitude, _senderGeoPoint?.longitude);

          _sender.lat = _senderLoc.latitude;
          _sender.long = _senderLoc.longitude;
        }

        _req.sender = _sender;
        _req.time = DateTime.fromMillisecondsSinceEpoch(data['time']);

        if(this.instantRequests.where((element) => element.id == _req.id).isEmpty) {
          setState(() {
            this.instantRequests.add(_req);
          });
        }

      });
      setState((){});
    },
        onError: (e){
          print(e);
        },
        onDone: (){}
    );
  }

  withdrawInstantRequest(HireRequest _req) async {
    // setState(() {
    //   isLoading = true;
    // });
    // repo.withdrawHireRequest(this.request.id).then((value) {
    //   if(value) {
        MyFirestore.deleteInstantHireRequest(_req.id);
    //     Navigator.of(context).pop();
    //   }
    // },
    //     onError: (e){
    //       print(e);
    //       showToast("Error Withdrawing Hire Request");
    //     });
  }

}