import 'package:doctor_finder/src/models/card_type.dart';
import 'package:doctor_finder/src/models/payment_method.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/payment_repository.dart' as repo;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class WalletController extends ControllerMVC {

  PaymentMethod payMethod = PaymentMethod();
  CardType cardType;

  bool isLoading = false;

  User currentUser;

  double amountToAdd = 0.0;

  TextEditingController cardNumberController;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> formKey;

  WalletController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    formKey = new GlobalKey<FormState>();
    cardNumberController = TextEditingController();
  }

  saveCard(){
    if(formKey.currentState.validate())
      {
        formKey.currentState.save();
        setState((){isLoading = true;});
        repo.updatePaymentMethod(payMethod).then((value) {
          setState((){isLoading = false;});
          if(value != null) {
            Navigator.of(scaffoldKey.currentContext).pop(true);
          }
        },
            onError: (e){
          print(e);
          setState((){isLoading = false;});
          showToast("Unable to add card");
        });

      }
  }

  addBalanceToWallet() {
    if(formKey.currentState.validate()) {
      formKey.currentState.save();
      setState((){isLoading = true;});
      repo.enterBalanceWallet(this.amountToAdd.toString()).then((value) {
        setState((){isLoading = false;});
        if(value){
          Navigator.of(scaffoldKey.currentContext).pop(true);
        }
      },
          onError: (e){
            setState((){isLoading = false;});
            print(e);
            showToast("Error adding balance");
      });
    }
  }

  getCurrentUser() async {
    userRepo.getCurrentUser().then((value) {
      setState((){
        this.currentUser = value;
      });
    });
  }

}
