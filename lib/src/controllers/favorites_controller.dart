import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/nurse_repository.dart' as repo;

class FavoritesController extends ControllerMVC {

  User currentUser;
  List<User> users = <User>[];

  bool isNotificationsOn = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  FavoritesController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getFavorites() async {
    Stream<User> stream = await repo.getFavorites();
    stream.listen((_user) {
      setState((){this.users.add(_user);});
    },
        onError: (e){
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
        },
        onDone: (){});
  }

}