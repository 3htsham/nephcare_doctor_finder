import 'dart:math';

import 'package:doctor_finder/src/models/user.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class SettingsController extends ControllerMVC {

  bool isNotificationsOn = false;
  User currentUser;

  GlobalKey<ScaffoldState> scaffoldKey;

  SettingsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  getCurrentUser() async {
    userRepo.getCurrentUser().then((user) {
      setState(() {
        this.currentUser = user;
      });
    });
  }

  switchNotifications(){
    setState((){
      isNotificationsOn = !isNotificationsOn;
    });
    settingsRepo.toggleNotificationsStatus(isNotificationsOn);
    if(isNotificationsOn) {
      //Configure Something
      initFCM();
    } else {
      //Configure Null
      nullFCM();
    }
  }

  getNotificationStatus() async {
    settingsRepo.getNotificationsStatus().then((value) {
      setState((){
        this.isNotificationsOn = value;
      });
    });
  }


  void nullFCM() async {
    FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    if(await _firebaseMessaging.autoInitEnabled()) {
      _firebaseMessaging.setAutoInitEnabled(false);
    }
    await _firebaseMessaging.deleteInstanceID();
    var token = await _firebaseMessaging.getToken();
    print(token);
  }

  void initFCM() async {

    FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.getToken().then((_token) async {
      print(_token);
      this.currentUser.firebaseToken = _token;
      await userRepo.updateUser(currentUser);
    });


    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        onMessage(message);
      },
      // onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

  }

  static void onMessage(Map<String, dynamic> message) async {

    var initializationSettingsAndroid = AndroidInitializationSettings('noti_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);


    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );

    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: null);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    Random rand = Random();
    var _id = rand.nextInt(100000);

    flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              // icon: android?.smallIcon,
              // other properties...
            ),
            iOS: IOSNotificationDetails(
                badgeNumber: 0,
                presentAlert: true,
                presentSound: true,
                presentBadge: true
            )
        ));

  }

  static Future<dynamic> onDidReceiveLocalNotification(int id, String a, String b, String c) {

  }


  static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

}