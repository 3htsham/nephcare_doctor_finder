import 'package:doctor_finder/src/models/category.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/job_repository.dart' as repo;
import 'package:doctor_finder/src/repositories/category_repository.dart' as catRepo;

class JobController extends ControllerMVC {

  User currentUser;
  Job job = Job();
  bool isLoading = false;

  bool showDeleteDialog = false;
  Job jobtoDelete;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> jobFormKey;
  TextEditingController titleController;
  TextEditingController descriptionController;
  TextEditingController hoursController;
  TextEditingController payController;

  List<Job> jobs = <Job>[];
  List<Category> categories = <Category>[];
  Category selectedCategory;
  

  JobController() {
    jobFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    titleController = TextEditingController();
    descriptionController = TextEditingController();
    hoursController = TextEditingController();
    payController = TextEditingController();
  }


  void getCategories(bool isEditing) async {
    setState((){
      isLoading = true;
    });
    if(await isInternetConnection()) {
      setState((){
        categories.clear();
      });
      Stream<Category> stream = await catRepo.getCategories();
      stream.listen((_category) {
        if(_category != null) {
          setState((){
            this.categories.add(_category);
          });
        }
      },
          onError: (e){
            print(e);
            setState((){
              isLoading = false;
            });
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error getting Categories list")));
          },
          onDone: (){
            setState((){
              isLoading = false;
              if(isEditing) {
                categories.forEach((element) {
                  if (element.id.toString() ==
                      this.job.catId.toString()) {
                    selectedCategory = element;
                  }
                });
              } else {
                selectedCategory = categories[0];
              }
            });
          });
    } else {
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }


  void postJob() async {
    if(jobFormKey.currentState.validate()) {
      jobFormKey.currentState.save();
      setState((){isLoading = true;});
      if(await isInternetConnection()) {
        this.job.catId = selectedCategory.id;
        repo.addNewJob(this.job).then((_job) {
          setState((){isLoading = false;});
          if(_job != null) {
            //Upload Success
            showToast("Job posted Successfully");
            Navigator.of(context).pop();
          } else {
            //Upload Failed
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Unable to post job"),));
          }
        }, 
        onError: (e){
          print(e);
          setState((){isLoading = false;});
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong"),));
        });
      } else {
        setState((){isLoading = false;});
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet conenction"),));
      }
    }
  }
  
  void getMyJobs() async {
    Stream<Job> stream = await repo.getMyJobs();
    jobs.clear();
    stream.listen((_job) {
      setState((){
        jobs.add(_job);
      });
    },
        onError: (e){
      print(e);
        },
        onDone: (){});
  }

  void updateJob() async {
    if(jobFormKey.currentState.validate()) {
      jobFormKey.currentState.save();
      setState((){isLoading = true;});
      this.job.catId = selectedCategory.id;
      if(await isInternetConnection()) {
        repo.updateJob(this.job).then((_job) {
          setState((){isLoading = false;});
          if(_job != null) {
            //Upload Success
            showToast("Job updated Successfully");
            Navigator.of(context).pop(true);
          } else {
            //Upload Failed
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Unable to update job"),));
          }
        },
            onError: (e){
              print(e);
              setState((){isLoading = false;});
              scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong"),));
            });
      } else {
        setState((){isLoading = false;});
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection"),));
      }
    }
  }

  void showDeleteJobDialog(Job _job) {
    setState((){
      jobtoDelete = _job;
      showDeleteDialog = true;
    });
  }
  void deleteJob() async {
    setState((){showDeleteDialog = false;});
    repo.deleteJob(jobtoDelete).then((_isDeleted) {
      if(_isDeleted) {
        showToast("Job deleted successfully");
        Navigator.of(context).pop();
      }
    },
        onError: (e){
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong")));
        });
  }

  getJobDetails() async {
    repo.getJobDetails(this.job.id).then((_job) {
      if (_job != null) {
        setState(() {
          this.job = _job;
        });
      }
    }, onError: (e) {
      print(e);
    });
  }
}