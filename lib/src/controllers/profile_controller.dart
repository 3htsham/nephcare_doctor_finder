import 'dart:math';
import 'package:image_picker/image_picker.dart';

import '';
import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class ProfileController extends ControllerMVC {

  User nurse;
  User currentUser;
  bool isLoading = false;
  bool showDialog = false;
  bool isNewImage = false;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> editFormKey;

  ProfileController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    editFormKey = new GlobalKey<FormState>();
  }

  void updateUserNow() async {
    if(editFormKey.currentState.validate()) {
      editFormKey.currentState.save();
      setState(() {
        isLoading = true;
      });
      Stream<User> stream = await userRepo.updateUserProfile(currentUser);
      stream.listen((_user) {
        if (_user != null) {
          Navigator.of(context).pop(true);
        }
      }, onError: (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
        scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text("Error updating profile")));
      }, onDone: () {
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  showPhotoDialog() {
    setState((){
      showDialog = true;
    });
  }
  closeDialog(){
    setState((){
      showDialog = false;
    });
  }

  void getCameraImage() async {
    closeDialog();
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      var _identifier = pickedFile.path;
      var _name = "Image_${_rand.nextInt(100000)}";
      setState((){
        this.currentUser.isNewPhoto = true;
        this.currentUser.newPhotoIdentifier = _identifier;
        this.currentUser.newPhotoName = _name;
      });
    }
  }
  void getGalleryImage() async {
    closeDialog();
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      var _identifier = pickedFile.path;
      var _name = "Image_${_rand.nextInt(100000)}";
      setState((){
        this.currentUser.isNewPhoto = true;
        this.currentUser.newPhotoIdentifier = _identifier;
        this.currentUser.newPhotoName = _name;
      });
    }
  }


}