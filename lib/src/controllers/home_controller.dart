import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_finder/src/models/category.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:doctor_finder/src/repositories/nurse_repository.dart' as nurseRepo;
import 'package:doctor_finder/config/my_firebase.dart';
import 'package:doctor_finder/src/repositories/category_repository.dart' as catRepo;

class HomeController extends ControllerMVC {

  User currentUser;
  List<User> nurses = <User>[];
  bool doneRequestingNearby = false;

  List<User> nursesList = <User>[];
  List<User> babysittersList = <User>[];
  bool showingNurses = true;

  bool isTrackings = false;

  List<Category> categories = <Category>[];
  Category selectedCategory;
  bool isLoading = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  HomeController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  void getCategories() async {
    setState((){
      isLoading = true;
    });
    if(await isInternetConnection()) {
      setState((){
        categories.clear();
      });
      Stream<Category> stream = await catRepo.getCategories();
      stream.listen((_category) {
        if(_category != null) {
          setState((){
            this.categories.add(_category);
          });
        }
      },
          onError: (e){
            print(e);
            setState((){
              isLoading = false;
            });
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error getting Categories list")));
          },
          onDone: (){
            setState((){
              isLoading = false;
              if(categories.length > 0) {
                selectedCategory = categories[0];
              }
            });
          });
    } else {
      setState((){
        isLoading = false;
      });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }

  getNearbyNurses() async {
    setState((){doneRequestingNearby = false;});
    Stream<User> stream = await nurseRepo.getNearbyNurses();
    stream.listen((_nurse) {
      setState((){
        nurses.add(_nurse);
        if(_nurse.category.name.toLowerCase() == "babysitter") {
          babysittersList.add(_nurse);
        } else {
          nursesList.add(_nurse);
        }
      });
    }, 
    onError: (e){
      print(e);
      setState((){doneRequestingNearby = true;});
      scaffoldKey.currentState?.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }, 
    onDone: (){
      setState((){doneRequestingNearby = true;});
    });
  }

  getMyTrackings() async {
    MyFirestore.getMyTrackingRooms(this.currentUser.id.toString()).listen((_querySnap){
      if(_querySnap.docs.length >0 ) {
        setState((){
          this.isTrackings = true;
        });
      } else {
        setState((){
          this.isTrackings = false;
        });
      }
    }, onError: (e){
      print(e);
    });
  }

  // getMyInstantTrackings() async {
  //   MyFirestore.getMyTrackingRooms(this.currentUser.id.toString()).listen((_querySnap){
  //     if(_querySnap.docs.length >0 ) {
  //       setState((){
  //         this.isTrackings = true;
  //       });
  //     } else {
  //       setState((){
  //         this.isTrackings = false;
  //       });
  //     }
  //   }, onError: (e){
  //     print(e);
  //   });
  // }

  createHireRequestForNearbyNurses() async {
    try {
      setState(() {
        this.isLoading = true;
      });

      ///Now do Firestore stuff and trigger FCM notification
      Map<String, dynamic> hireRequestMap = Map<String, dynamic>();
      final _id = Uuid().generateV4();
      hireRequestMap['id'] = _id;
      // hireRequestMap['category_status'] = ['${this.selectedCategory.id}', 'pending'];
      hireRequestMap['status'] = 'pending';
      hireRequestMap['category'] = '${this.selectedCategory.id}';
      hireRequestMap['categoryName'] = '${this.selectedCategory.name}';

      Map<String, dynamic> senderMap = Map<String, dynamic>();
      senderMap['id'] = this.currentUser.id;
      senderMap['name'] = this.currentUser.name;
      senderMap['image'] = this.currentUser.image;
      senderMap['type'] = this.currentUser.type;
      senderMap['location'] = GeoPoint(
          double.parse(this.currentUser.lat.toString()),
          double.parse(this.currentUser.long.toString()));

      hireRequestMap['sender'] = senderMap;
      hireRequestMap['senderID'] = this.currentUser.id;


      MyFirestore.createInstantHireRequest(hireRequestMap, requestId: _id);

      setState(() {
        isLoading = false;
      });
      showToast("Hire request sent successfully");
    } catch (e) {
      setState((){ isLoading = false; });
      showToast("Something went wrong");
    }

  }

}