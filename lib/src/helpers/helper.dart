class Helper {
  static getJobData(Map<String, dynamic> data){
    return data['job'];
  }

  static getNursesData(Map<String, dynamic> data){
    return data['nearby_nurses'];
  }
  static getFavoritesData(Map<String, dynamic> data){
    return data['favorites'];
  }

  static getMessageData(Map<String, dynamic> data) {
    return data['message'] ?? [];
  }

  static getJobsData(Map<String, dynamic> data) {
    return data['jobs'] ?? [];
  }
  static getUserData(Map<String, dynamic> data) {
    return data['user'] ?? [];
  }
  static getCategoriesData(Map<String, dynamic> data) {
    return data['categories'] ?? [];
  }
}