import 'package:doctor_finder/src/models/category.dart';
import 'package:doctor_finder/src/models/payment_method.dart';
import 'package:doctor_finder/src/models/qualification.dart';
import 'package:doctor_finder/src/models/specification.dart';

class User {

  bool isNewPhoto = false;
  var newPhotoIdentifier = "";
  var newPhotoName = "";

  double rating;
  DateTime createdOn;

  var speciality;

  var latitude;
  var longitude;
  var address;
  var categoryName;

  ////////////////////////////////
  var id;
  var name="";
  var email;
  var password;

  var emailVerifiedAt;
  var apiToken;
  var type;//2 for Hospital, 1 for User
  var cat; //Category
  var genre; //2 For Nurse, 1 for Hospital/User
  var catId;
  var lat;
  var long;
  var location;
  var licenseNo;
  var phone;
  var image = "";
  String firebaseToken;
  String updatedAt;
  String createdAt;
  Category category;
  List<Qualification> qualifications;
  List<Specification> specifications;
  var distance;

  var isFavorite = false;
  bool verified;

  var balance;
  PaymentMethod payMethod;

  User({
    this.categoryName,
    this.apiToken,
        this.name,
        this.email,
        this.password,
        this.type,
        this.createdOn,
        this.category,
        this.updatedAt,
        this.createdAt,
        this.id,
        this.phone,
        this.image, this.speciality,
        this.location, this.latitude, this.longitude,
        this.address, this.rating, this.cat});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    verified = json['verified'] ?? true;
    emailVerifiedAt = json['email_verified_at'];
    type = json['type'];
    genre = json['genre'];
    catId = json['cat_id'];
    apiToken = json['api_token'];
    lat = json['lat'];
    long = json['long'];
    location = json['location'];
    licenseNo = json['license_no'];
    phone = json['phone'];
    image = json['image'];
    firebaseToken = json['firebase_token'];
    distance = json['distance'] ?? "";
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isFavorite = json['is_favorite'] ?? false;
    balance = json['balance'];
    this.payMethod = PaymentMethod.fromJson(json);
    if (json['qualifications'] != null) {
      qualifications = new List<Qualification>();
      json['qualifications'].forEach((v) {
        qualifications.add(new Qualification.fromJson(v));
      });
    }
    if (json['specifications'] != null) {
      specifications = new List<Specification>();
      json['specifications'].forEach((v) {
        specifications.add(new Specification.fromJson(v));
      });
    }
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toMap() {
    final Map<String, String> data = new Map<String, String>();
    this.id != null ? data['id'] = this.id.toString() : null;
    this.name != null ? data['name'] = this.name.toString() : null;
    this.email != null ? data['email'] = this.email.toString() : null;
    this.password != null ? data['password'] = this.password.toString() : null;
    this.type != null ? data['type'] = type.toString() ?? "1" : null;
    this.phone != null ? data['phone'] = phone.toString() ?? "" : null;
    this.firebaseToken != null ? data['firebase_token'] = firebaseToken.toString() ?? "" : null;
    data['genre'] = "1";
    this.catId != null ? data['cat_id'] = this.catId.toString() : null;
    this.apiToken != null ? data['api_token'] = this.apiToken.toString() : null;
    this.lat != null ? data['lat'] = double.parse(this.lat.toString()).toStringAsFixed(6) : null;
    this.long != null ? data['long'] = double.parse(this.long.toString()).toStringAsFixed(6) : null;
    return data;
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    this.id != null ? data['id'] = this.id.toString() : null;
    this.name != null ? data['name'] = this.name.toString() : null;
    this.email != null ? data['email'] = this.email.toString() : null;
    this.password != null ? data['password'] = this.password.toString() : null;
    this.type != null ? data['type'] = type.toString() ?? "1" : null;
    this.image != null ? data['image'] = image.toString() ?? "1" : null;
    this.phone != null ? data['phone'] = phone.toString() ?? "" : null;
    this.firebaseToken != null ? data['firebase_token'] = firebaseToken.toString() ?? "" : null;
    data['genre'] = "1";
    this.catId != null ? data['cat_id'] = this.catId.toString() : null;
    this.apiToken != null ? data['api_token'] = this.apiToken.toString() : null;
    this.lat != null ? data['lat'] = double.parse(this.lat.toString()).toStringAsFixed(6) : null;
    this.long != null ? data['long'] = double.parse(this.long.toString()).toStringAsFixed(6) : null;
    return data;
  }


  Map<String, String> toSaveJson() {
    final Map<String, String> data = new Map<String, String>();
    this.id != null ? data['id'] = this.id.toString() : null;
    this.name != null ? data['name'] = this.name.toString() : null;
    this.email != null ? data['email'] = this.email.toString() : null;
    this.password != null ? data['password'] = this.password.toString() : null;
    this.type != null ? data['type'] = type.toString() ?? "1" : null;
    this.image != null ? data['image'] = image.toString() ?? "1" : null;
    this.phone != null ? data['phone'] = phone.toString() ?? "" : null;
    this.firebaseToken != null ? data['firebase_token'] = firebaseToken.toString() ?? "" : null;
    data['genre'] = "1";
    this.catId != null ? data['cat_id'] = this.catId.toString() : null;
    this.apiToken != null ? data['api_token'] = this.apiToken.toString() : null;
    this.lat != null ? data['lat'] = double.parse(this.lat.toString()).toStringAsFixed(6) : null;
    this.long != null ? data['long'] = double.parse(this.long.toString()).toStringAsFixed(6) : null;
    this.balance != null ? data['balance'] = this.balance.toString() : null;
    if(this.payMethod != null && this.payMethod.cardNumber != null) {
      data['card_name'] = this.payMethod.cardName.toString();
      data['card_number'] = this.payMethod.cardNumber.toString();
      data['card_cvc'] = this.payMethod.cvc.toString();
      data['card_expiry'] = this.payMethod.expiry.toString();
    }
    return data;
  }

}