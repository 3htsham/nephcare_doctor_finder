class Specification {
  String specName;
  String exp;

  Specification({this.specName, this.exp});

  Specification.fromJson(Map<String, dynamic> json) {
    specName = json['spec_name'];
    exp = json['exp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['spec_name'] = this.specName;
    data['exp'] = this.exp;
    return data;
  }
}