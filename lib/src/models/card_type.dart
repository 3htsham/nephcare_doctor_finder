enum CardType {
  MasterCard,
  Visa,
  Verve,
  Discover,
  AmericanExpress,
  DinersClub,
  Jcb,
  Others, // Any other card issuer
  Invalid // We'll use this when the card is invalid
}