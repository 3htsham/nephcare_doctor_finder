import 'package:doctor_finder/src/models/chat.dart';
import 'package:doctor_finder/src/models/hire_request.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/job_track.dart';
import 'package:doctor_finder/src/models/user.dart';

class RouteArgument {

  int currentTab;
  String title;

  User currentUser;

  Chat chat;
  User user;
  Job job;
  bool getUserDetails = false;

  HireRequest request;

  String trackingReqId;

  bool isAddingPaymentFromJob = false;

  var onGoingJobId;
  JobTrack jobToTrack;

  RouteArgument({this.currentTab, this.chat, this.request,
    this.getUserDetails, this.title, this.user,
    this.trackingReqId,
    this.currentUser, this.job, this.isAddingPaymentFromJob = false,
    this.onGoingJobId,
    this.jobToTrack,});

}