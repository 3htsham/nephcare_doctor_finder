class PaymentMethod {

  var cardName;
  var cardNumber;
  var cvc;
  var expiry;

  PaymentMethod();

  PaymentMethod.fromJson(Map<String, dynamic> json) {
    cardName = json['card_name'];
    cardNumber = json['card_number'];
    cvc = json['card_cvc'];
    expiry = json['card_expiry'];
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['card_name'] = this.cardName;
    data['card_number'] = this.cardNumber;
    data['card_cvc'] = this.cvc;
    data['card_expiry'] = this.expiry;
    return data;
  }

}