class Message {
  var chatId;
  var type;
  bool isRead;
  var txt;
  String updatedAt;
  String createdAt;
  var id;
  var receiverId;

  DateTime time;

  Message(
      {this.chatId,
      this.type,
      this.isRead,
      this.txt,
      this.updatedAt,
      this.createdAt,
      this.receiverId,
      this.time,
      this.id});

  Message.fromJson(Map<String, dynamic> json) {
    chatId = json['chat_id'];
    type = json['type'];
    isRead = json['isRead'];
    txt = json['txt'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chat_id'] = this.chatId;
    data['type'] = this.type;
    data['txt'] = this.txt;
    data['receiver_id'] = this.receiverId;
    data['id'] = this.id;
    return data;
  }
}