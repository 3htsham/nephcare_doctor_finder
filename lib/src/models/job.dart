import 'package:doctor_finder/src/models/user.dart';

class Job {
  var id;
  var userId;
  var title;
  var pay;
  var hours;
  var description;
  var catId;
  int status;
  String createdAt;
  String updatedAt;
  List<User> applicants;

  Job(
      {this.id,
      this.userId,
      this.title,
      this.pay,
      this.hours,
      this.description,
      this.status,
      this.createdAt,
      this.updatedAt});

  Job.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    title = json['title'];
    pay = json['pay'];
    hours = json['hours'];
    description = json['description'];
    catId = json['category_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['applicants'] != null) {
      applicants = new List<User>();
      json['applicants'].forEach((v) {
        applicants.add(new User.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    id != null ? data['id'] = this.id : null;
    userId != null ? data['user_id'] = this.userId : null;
    data['title'] = this.title;
    data['pay'] = this.pay;
    data['hours'] = this.hours;
    data['category_id'] = this.catId;
    data['description'] = this.description;
    data['status'] = this.status;
    return data;
  }
}