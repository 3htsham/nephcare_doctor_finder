import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/user.dart';

class HireRequest {

  var id;
  var description;
  Job job;
  User sender;
  User nurse;
  List<String> users;
  var status;

  DateTime time;

  String category;
  String categoryName;
  String senderId;
  bool isInstant = false;


  HireRequest({
    this.id,
    this.description,
    this.time,
    this.job,
    this.sender,
    this.nurse,
    this.users,
    this.status,
    this.isInstant = false,
    this.category,
    this.categoryName,
    this.senderId,
  });

}