import 'package:doctor_finder/src/models/user.dart';

class Chat {
  
  var fireStoreChatId ;
  bool isRead;

  User user;
  DateTime time;
  var lastMessage;


  var id;
  var ownerId;
  var opponentId;
  String createdAt;
  String updatedAt;
  var unread;
  User opponent;

  Chat({this.id, 
      this.fireStoreChatId,
      this.isRead,
      this.user, 
      this.time, 
      this.lastMessage,
      this.ownerId,
      this.opponentId,
      this.createdAt,
      this.updatedAt,
      this.unread,
      this.opponent});

  Chat.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ownerId = json['owner_id'];
    opponentId = json['opponent_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    unread = json['unread'];
    opponent = json['opponent'] != null
        ? new User.fromJson(json['opponent'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['owner_id'] = this.ownerId;
    data['opponent_id'] = this.opponentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['unread'] = this.unread;
    if (this.opponent != null) {
      data['opponent'] = this.opponent.toJson();
    }
    return data;
  }

}