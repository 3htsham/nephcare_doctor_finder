import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/elements/job/my_job_list_item.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/controllers/job_controller.dart';

class MyJobsWidget extends StatefulWidget {
  RouteArgument argument;
  MyJobsWidget({this.argument});
  @override
  _MyJobsWidgetState createState() => _MyJobsWidgetState();
}

class _MyJobsWidgetState extends StateMVC<MyJobsWidget> {

  JobController _con;

  _MyJobsWidgetState() : super(JobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
    _con.getMyJobs();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("My Jobs", style: textTheme.headline5,),
        elevation: 0,
        backgroundColor: theme.scaffoldBackgroundColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){ Navigator.of(context).pop(); },
          icon: Icon(CupertinoIcons.back, color: theme.accentColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _con.jobs.isEmpty
                ? CircularLoadingWidget(height: 100)
                : ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: _con.jobs.length,
                    itemBuilder: (context, index) {
                      return MyJobListItem(
                        job: _con.jobs[index],
                        user: _con.currentUser,
                        onTap: (){
                          Navigator.of(context).pushNamed("/JobDetails",
                              arguments: RouteArgument(job: _con.jobs[index], currentUser: _con.currentUser)).then((value) {
                            _con.getMyJobs();
                          });
                        },
                        onEdit: () {
                          Navigator.of(context)
                              .pushNamed("/EditJob",
                                  arguments:
                                      RouteArgument(job: _con.jobs[index], currentUser: _con.currentUser))
                              .then((value) {
                            if (value != null) {
                              if (value) {
                                _con.getMyJobs();
                              }
                            }
                          });
                        },
                      );
                    }),
          ],
        ),
      ),
    );
  }

}