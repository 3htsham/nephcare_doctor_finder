import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/job_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class JobDetialsWidget extends StatefulWidget {
  RouteArgument argument;
  JobDetialsWidget({this.argument});
  @override
  _JobDetialsWidgetState createState() => _JobDetialsWidgetState();
}

class _JobDetialsWidgetState extends StateMVC<JobDetialsWidget> {

  JobController _con;

  _JobDetialsWidgetState() : super(JobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.job = widget.argument.job;
    _con.currentUser = widget.argument.currentUser;
    _con.getJobDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(_con.job.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return WillPopScope(
      onWillPop: () async {
        if(_con.showDeleteDialog) {
          setState((){_con.showDeleteDialog = false;});
          return false;
        }
        return true;
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: _appbar(),
        body: Container(
          height: size.height,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Stack(
            children: [

              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(Icons.arrow_right, color: theme.accentColor),
                          Text(_con.job.title, style: textTheme.headline3),
                        ],
                      ),
                      SizedBox(height: 10),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Spacer(),
                          Row(
                            children: [
                              Icon(CupertinoIcons.clock, size: 16, color: config.Colors().textColor(1),),
                              SizedBox(width: 5,),
                              Text('${_con.job.hours ?? 0} hours', style: textTheme.caption,)
                            ],
                          ),
                          SizedBox(width: 25,),
                          Row(
                            children: [
                              Icon(Icons.attach_money, size: 16, color: config.Colors().textColor(1),),
                              SizedBox(width: 5,),
                              Text(_con.job.pay, style: textTheme.caption,)
                            ],
                          ),
                          SizedBox(width: 10,),
                        ],
                      ),
                      SizedBox(height: 5),

                      Row(
                          children: [
                            ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: _con.currentUser != null && _con.currentUser.image != null && _con.currentUser.image.length > 0
                                    ? FadeInImage.assetNetwork(
                                  placeholder: "assets/img/placeholders/profile_placeholder.png",
                                  image: _con.currentUser.image,
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover,
                                )
                                    : Image.asset(
                                  "assets/img/placeholders/profile_placeholder.png",
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover,
                                )
                            ),
                            SizedBox(width: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    child: Text(_con.currentUser.name ?? "Job Poster",
                                      style: textTheme.bodyText2.merge(TextStyle(fontSize: 12)),
                                    )
                                ),
                                SizedBox(height: 5),
                                Container(
                                    child: Text('Posted on $day at $time', style: textTheme.caption.merge(TextStyle(fontSize: 10)))),
                              ],
                            ),
                          ]
                      ),

                      SizedBox(height: 10),
                      Text('Job Details', style: textTheme.headline4),
                      SizedBox(height: 10),

                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          child: Text(_con.job.description ?? "", style: textTheme.bodyText1)),

                      SizedBox(height: 10),

                      ListTile(
                        title: Text("Applicants", style: textTheme.headline5,),
                        leading: GradientIcon(
                          Icons.person_pin_rounded,
                          size: 25.0,
                          gradient: LinearGradient(
                            colors: <Color>[
                              config.Colors().mainColor(1),
                              config.Colors().mainColorLight
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                      ),

                      _con.job.applicants != null &&
                              _con.job.applicants.length > 0
                          ? ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              itemCount: _con.job.applicants.length,
                              itemBuilder: (context, index) {
                                User applicant = _con.job.applicants[index];

                                return InkWell(
                                  onTap: (){
                                    Navigator.of(context).pushNamed("/DoctorProfile",
                                        arguments: RouteArgument(user: applicant, getUserDetails: true, currentUser: _con.currentUser));
                                  },
                                  child: Container(
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                            borderRadius: BorderRadius.circular(100),
                                            child: CachedNetworkImage(
                                              imageUrl: applicant.image,
                                              width: 50,
                                              height: 50,
                                              fit: BoxFit.cover,
                                            )
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(child: Text(applicant.name, style: textTheme.subtitle1.merge(TextStyle(fontSize: 14)),)),
                                        IconButton(icon: GradientIcon(
                                          Icons.person_add,
                                          size: 20.0,
                                          gradient: LinearGradient(
                                            colors: <Color>[
                                              config.Colors().mainColor(1),
                                              config.Colors().mainColorLight
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                          ),
                                        ),
                                            onPressed: (){
                                              Navigator.of(context).pushNamed("/HireNurse",
                                                  arguments: RouteArgument(currentUser: _con.currentUser, user: applicant, job: _con.job));
                                            }),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            )
                          : SizedBox(
                              height: 0,
                            ),
                    ],
                  ),
                ),
              ),


              _con.showDeleteDialog
                  ? Container(
                width: size.width,
                height: size.height,
                color: theme.scaffoldBackgroundColor.withOpacity(0.5),
                child: Center(
                  child: NeuContainer(
                    width: 180,
                    height: 120,
                    borderRadius: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Center(
                        child: Column(
                          children: [
                            SizedBox(height: 5,),
                            Text("Delete This Job", style: textTheme.headline5,),
                            SizedBox(height: 20,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                NeuButtonContainer(
                                  onPressed: _con.deleteJob,
                                  height: 40,
                                  borderRadius: 20,
                                  width: 40,
                                  child: Center(
                                    child: Icon(
                                      CupertinoIcons.check_mark,
                                      color: theme.accentColor,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 10,),
                                NeuButtonContainer(
                                  onPressed: (){
                                    setState(() {_con.showDeleteDialog = false;});
                                  },
                                  height: 40,
                                  borderRadius: 20,
                                  width: 40,
                                  child: Center(
                                    child: Icon(
                                      CupertinoIcons.clear,
                                      color: theme.accentColor,
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )
                  : SizedBox(height: 0, width: 0,),

            ],
          ),
        ),
      ),
    );
  }


  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Job Details",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            Navigator.of(context).pop();
          }
      ),
      actions: [
        IconButton(icon: Icon(CupertinoIcons.trash_fill, color: Theme.of(context).accentColor,),
            onPressed: (){
          _con.showDeleteJobDialog(_con.job);
        })
      ],
    );
  }
}
