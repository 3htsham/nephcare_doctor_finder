import 'package:doctor_finder/src/controllers/job_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/elements/job/edit_job_form.dart';
import 'package:doctor_finder/src/elements/job/job_form.dart';
import 'package:doctor_finder/src/models/category.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/neu_button.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:doctor_finder/src/neumorphic/nue_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class EditJobWidget extends StatefulWidget {
  RouteArgument argument;
  EditJobWidget({@required this.argument});
  @override
  _EditJobWidgetState createState() => _EditJobWidgetState();
}

class _EditJobWidgetState extends StateMVC<EditJobWidget> {

  JobController _con;

  _EditJobWidgetState() : super(JobController()){
    _con = controller;
  }

  @override
  initState(){
    _con.job = widget.argument.job;
    _con.getCategories(true);
   super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("My Jobs", style: textTheme.headline5,),
        elevation: 0,
        backgroundColor: theme.scaffoldBackgroundColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){ Navigator.of(context).pop(); },
          icon: Icon(CupertinoIcons.back, color: theme.accentColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _con.categories.isNotEmpty ? EditJobFormWidget(con: _con) : SizedBox(height: 0,),

            SizedBox(height: 15,),

            _con.categories.isNotEmpty
                ? Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: NeuTextFieldContainer(
                borderRadius: BorderRadius.circular(15),
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                textField: Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      itemCount: _con.categories.length,
                      itemBuilder: (context, index) {
                        Category category = _con.categories[index];
                        return InkWell(
                          onTap: (){
                            if(!_con.isLoading) {
                              setState((){
                                _con.selectedCategory = category;
                              });
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(category.name, style: textTheme.bodyText1,),
                                Spacer(),
                                NeuBtnCheckBox(
                                  onPressed: (){
                                    if(!_con.isLoading) {
                                      setState((){
                                        _con.selectedCategory = category;
                                      });
                                    }
                                  },
                                  state: _con.selectedCategory != null && _con.selectedCategory.id==category.id,
                                  borderRadius: 100,
                                  width: 30,
                                  height: 30,
                                  child: Icon(CupertinoIcons.check_mark, color: _con.selectedCategory != null && _con.selectedCategory.id==category.id ? theme.accentColor : Colors.white,),
                                )
                              ],
                            ),
                          ),
                        );
                      }
                  ),
                ),
              ),
            )
                : CircularLoadingWidget(height: 100),


            SizedBox(height: 15,),

            _con.isLoading ? NeuContainer(
                borderRadius: 100,
                width: 50,
                height: 50,
                child: CircularProgressIndicator()
            )
                : NeuButton(
              width: 150,
              height: 50,
              text: "Update Job".toUpperCase(),
              textStyle: textTheme.subtitle2,
              borderRadius: 100,
              onPressed: (){
                _con.updateJob();
              },
            ),

            SizedBox(height: 20)
          ],
        ),
      ),
    );
  }
}
