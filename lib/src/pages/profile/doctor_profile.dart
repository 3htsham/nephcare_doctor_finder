import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/nurse_profile_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class DoctorProfile extends StatefulWidget {
  RouteArgument argument;

  DoctorProfile({this.argument});

  @override
  _DoctorProfileState createState() => _DoctorProfileState();
}

class _DoctorProfileState extends StateMVC<DoctorProfile> {

  NurseProfileController _con;

  _DoctorProfileState() : super(NurseProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.initLocationController();
    _con.nurse = widget.argument.user;
    if(widget.argument.getUserDetails) {
      _con.getProfile();
    }
    super.initState();
    _con.getCurrentUser();
  }


  @override
  Widget build(BuildContext context) {
    var mediaSize = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return RefreshIndicator(
      onRefresh: () async {},
      child: Scaffold(
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40, right: 10, left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: NeuButtonContainer(
                        onPressed: (){
                          Navigator.of(context).pop();
                        },
                        width: 40,
                        height: 40,
                        borderRadius: 100,
                        child: Center(
                          child: GradientIcon(
                            CupertinoIcons.back,
                            size: 25.0,
                            gradient: LinearGradient(
                              colors: <Color>[
                                config.Colors().mainColor(1),
                                config.Colors().mainColorLight
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/img/logo.png", width: 40,),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: NeuButtonContainer(
                        onPressed: (){
                          if(!_con.nurse.isFavorite || _con.nurse.isFavorite == "false") {
                          ///Add to favorite
                            _con.addToFavorite(_con.nurse.id);
                          } else {
                            ///Remove from Favorites
                          }
                        },
                        width: 40,
                        height: 40,
                        borderRadius: 100,
                        child: Center(
                          child: GradientIcon(
                            (_con.nurse.isFavorite || _con.nurse.isFavorite == "true")
                                ? CupertinoIcons.heart_solid
                                : CupertinoIcons.heart,
                            size: 25.0,
                            gradient: LinearGradient(
                              colors: <Color>[
                                config.Colors().mainColor(1),
                                config.Colors().mainColorLight
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10,),
              Stack(
                children: [
                  Center(
                      child: Stack(
                        children: [
                          CachedNetworkImage(
                            imageUrl: _con.nurse.image,
                            height: 300,
                            width: mediaSize.width,
                            fit: BoxFit.cover,
                          ),
                          Container(
                            height: 300,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  config.Colors().scaffoldColor(1),
                                  config.Colors().scaffoldColor(0.3),
                                  config.Colors().scaffoldColor(0),
                                  config.Colors().scaffoldColor(0),
                                  config.Colors().scaffoldColor(0.3),
                                  config.Colors().scaffoldColor(1),
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                stops: [0, 0.1, 0.3, 0.8, 0.9, 1]
                              )
                            ),
                          )
                        ],
                      )
                  ),

                ],
              ),
              SizedBox(height: 20,),
              Text(
                "Welcome to My Profile",
                style: textTheme.headline1.merge(TextStyle(fontSize: 24)),
              ),
              SizedBox(height: 10,),
              Text(
                "I'm ${_con.nurse.name}",
                style: textTheme.headline5,
              ),
              Text(
                _con.nurse.category != null ? "(${_con.nurse.category.name})" : "",
                style: textTheme.subtitle2,
              ),
              SizedBox(height: 20,),
              Text(
                "Distance",
                style: textTheme.bodyText2,
              ),
              Text(
                  (_con.nurse.distance != null && _con.nurse.distance.toString().length>0) ? "${double.parse(_con.nurse.distance.toString()).toStringAsFixed(2)} KMs" : "",
                style: textTheme.headline1.merge(TextStyle(fontSize: 24)),
              ),
              SizedBox(height: 10,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuContainer(
                  width: mediaSize.width,
                  borderRadius: 10,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Icon(
                              CupertinoIcons.news_solid,
                              color: theme.accentColor,
                            ),
                            SizedBox(width: 10),
                            Text(
                              "Qualification/s",
                              style: textTheme.headline1.merge(TextStyle(fontSize: 18)),
                            ),
                          ],
                        ),

                        _con.nurse.qualifications != null && _con.nurse.qualifications.length > 0
                            ? ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: _con.nurse.qualifications.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              dense: true,
                              leading: GradientIcon(
                                Icons.arrow_forward,
                                size: 20.0,
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    config.Colors().mainColor(1),
                                    config.Colors().mainColorLight
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              title: Text(
                                _con.nurse.qualifications[index].degree,
                                style: textTheme.bodyText1,),
                              subtitle: Text(
                                _con.nurse.qualifications[index].institute + " " + '(${_con.nurse.qualifications[index].year})',
                                style: textTheme.bodyText2,),
                            );
                          },
                        )
                            : SizedBox(height: 0)

                      ],
                    ),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuContainer(
                  width: mediaSize.width,
                  borderRadius: 10,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.stars,
                              color: theme.accentColor,
                            ),
                            SizedBox(width: 10),
                            Text(
                              "Specifications or Experience",
                              style: textTheme.headline1.merge(TextStyle(fontSize: 18)),
                            ),
                          ],
                        ),

                        _con.nurse.specifications != null && _con.nurse.specifications.length > 0
                            ? ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: _con.nurse.specifications.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              dense: true,
                              leading: GradientIcon(
                                Icons.arrow_forward,
                                size: 20.0,
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    config.Colors().mainColor(1),
                                    config.Colors().mainColorLight
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              title: Text(
                                _con.nurse.specifications[index].specName,
                                style: textTheme.bodyText1,),
                              subtitle: Text(
                                'Experience ${_con.nurse.specifications[index].exp} year/s',
                                style: textTheme.bodyText2,),
                            );
                          },
                        )
                            : SizedBox(height: 0)

                      ],
                    ),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuContainer(
                  width: mediaSize.width,
                  borderRadius: 10,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Icon(
                              CupertinoIcons.location_solid,
                              color: theme.accentColor,
                            ),
                            SizedBox(width: 10),
                            Text(
                              "Location on Map",
                              style: textTheme.headline1.merge(TextStyle(fontSize: 18)),
                            ),
                          ],
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                          child: Container(
                            height: 130,
                            child: GoogleMap(
                              onMapCreated: (GoogleMapController controller) {
                                _con.controller = controller;
                                _con.controller.setMapStyle(_con.mapStyle);
                                _con.moveCamera();
                              },
                              mapType: MapType.normal,
                              initialCameraPosition: _con.nurse != null
                                  ? CameraPosition(
                                  target: LatLng(
                                      _con.nurse.lat != null ?
                                      double.parse(_con.nurse.lat.toString()) : 40.7237765,
                                      _con.nurse.long != null ?
                                      double.parse(_con.nurse.long.toString()) : -74.017617), zoom: 13.0)
                                  : CameraPosition(
                                  target: LatLng(40.7237765, -74.017617), zoom: 13.0),
                              markers: Set.from(_con.allMarkers),
                            ),
                          ),
                        )

                      ],
                    ),
                  ),
                ),
              ),


              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    NeuButtonContainer(
                      onPressed: (){
                        Navigator.of(context).pushNamed("/HireNurse",
                            arguments: RouteArgument(currentUser: _con.currentUser, user: _con.nurse,));
                      },
                      width: 130,
                      child: Text("Hire me now", style: textTheme.subtitle1,),
                      borderRadius: 15,
                    ),
                    NeuButtonContainer(
                      onPressed: (){
                        Navigator.of(context).pushNamed("/OpenChat", arguments: RouteArgument(currentUser: _con.currentUser, user: _con.nurse));
                      },
                      width: 130,
                      child: Text("Message", style: textTheme.subtitle1,),
                      borderRadius: 15,
                    ),
                  ],
                ),
              ),

              FlatButton(
                onPressed: (){
                  Navigator.of(context).pop();
                },
                child: Text("Skip", style: textTheme.bodyText1,),
              ),

              SizedBox(height: 20,),
            ],
          ),
        )
      ),
    );
  }
}
