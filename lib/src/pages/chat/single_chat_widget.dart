import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart' as c;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/controllers/chat_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/elements/chat/my_message_item.dart';
import 'package:doctor_finder/src/elements/chat/opponent_message_item.dart';
import 'package:doctor_finder/src/models/message.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';

class SingleChatWidget extends StatefulWidget {
  RouteArgument argument;

  SingleChatWidget({this.argument});

  @override
  _SingleChatWidgetState createState() => _SingleChatWidgetState();

}

class _SingleChatWidgetState extends StateMVC<SingleChatWidget> {

  ChatController _con;

  _SingleChatWidgetState() : super(ChatController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    if(widget.argument.chat != null) {
      _con.chat = widget.argument.chat;
      _con.getOpponentUser(widget.argument.chat.opponentId);
    } else {
      _con.user = widget.argument.user;
      _con.getOpponentUser(widget.argument.user.id);
    }
    super.initState();
    if(widget.argument.chat != null) {
      _con.getChatMessages();
    } else {
      _con.getChatMessagesByUserId();
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
          title: Text(_con.chat == null ? _con.user.name ?? "Send Message" : _con.chat.opponent.name ?? "Send Message", style: textTheme.subtitle1,),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor), 
            onPressed: (){
                Navigator.of(context).pop();
              }
            ),
          actions: [
            PopupMenuButton(
              color: theme.scaffoldBackgroundColor,
              onSelected: (value) {
                _con.onOptions(value, true, _con.chat);
              },
              icon: Icon(Icons.more_horiz_outlined, color: theme.primaryColorDark,),
              itemBuilder: (context) => List.generate(_con.options.length, (index) {
                return PopupMenuItem(
                    value: _con.options[index],
                    child: Text(_con.options[index], style: textTheme.bodyText2,));
              })
            ),
          ],
        ),
        body: Stack(
          children: [

            SingleChildScrollView(
              controller: _con.scrollController,
              child: Column(
                children: [
                   _con.loadingMessages
                        ? CircularLoadingWidget(height: 100,)
                        : _con.messages.isEmpty
                        ? Container(
                            height: size.height-100,
                            child: Center(
                              child: Opacity(
                                opacity: 0.2,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Icon(Icons.chat_bubble, 
                                        size: 40, color: theme.focusColor,),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text(
                                      "No messages Yet",
                                      style: textTheme.bodyText1,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ) 
                          : ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      padding: EdgeInsets.only(bottom: 70),
                      itemCount: _con.messages.length,
                        itemBuilder: (context, index) {

                          Message message = _con.messages[index];

                          bool isMe = message.type == 1;

                          var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
                          var dateTime = _con.messages[index].time;
                          // var day = DateFormat('EEEE').format(dateTime);
                          var time = DateFormat('hh:mm aa').format(dateTime);

                          var constraints = BoxConstraints(maxWidth: (size.width / 2) + 15);
                          var boxDecoration = BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(isMe ? 100 : 0),
                                  topRight: Radius.circular(100),
                                  bottomRight: Radius.circular(isMe ? 0 : 100),
                                  bottomLeft: Radius.circular(100)),
                              color: isMe ? theme.accentColor : theme.primaryColor);

                          var textualPadding =
                          const EdgeInsets.symmetric(vertical: 10.0, horizontal: 12);

                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                            child: InkWell(
                              onTap: (){},
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                textDirection: isMe ? c.TextDirection.rtl : c.TextDirection.ltr,
                                children: [
                                  isMe
                                      ?
                                  MyMessageWidgetItem(
                                    link:  _con.currentUser != null && _con.currentUser.image != null 
                                            && _con.currentUser.image.length > 0 ? _con.currentUser.image : null,
                                    message: message,
                                  )
                                  : OpponentMessageWidgetItem(
                                      link: _con.chat.opponent != null && _con.chat.opponent.image != null 
                                          && _con.chat.opponent.image.length > 0 ? _con.chat.opponent.image : null,
                                      message: message
                                    ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                      width: 50,
                                      child: Text(
                                        "$time",
                                        style: textTheme.caption.merge(TextStyle(fontSize: 10)),
                                      ))
                                ],
                              ),
                            ),
                          );
                        }
                    ),


                    SizedBox(height: 50),


                ],
              )
            ),


            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                color: theme.scaffoldBackgroundColor,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(width: 5,),
                    
                    Expanded(
                      flex: 1,
                      child: NeuTextFieldContainer(
                        borderRadius: BorderRadius.circular(10),
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        textField: TextFormField(
                            controller: _con.textController,
                            textInputAction: TextInputAction.newline,
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            style: textTheme.bodyText1.merge(TextStyle(color: theme.accentColor)),
                            decoration: InputDecoration(
                                hintText: "Type your message",
                                border: InputBorder.none,
                            ),
                          ),
                      ),
                    ),
                    SizedBox(width: 5,),
                    NeuContainer(
                      width: 50,
                      height: 50,
                      borderRadius: 100,
                      child: IconButton(
                        onPressed: (){},
                        icon: Icon(Icons.attach_file, color: theme.primaryColorDark,),
                        splashColor: theme.focusColor,
                      )),
                    SizedBox(width: 5,),
                    NeuContainer(
                      width: 50,
                      height: 50,
                      borderRadius: 100,
                      child: IconButton(
                        onPressed: _con.sendANewMessage,
                        icon: Icon(Icons.send, color: theme.accentColor),
                        splashColor: theme.accentColor,
                      )),
                  ],
                ),
              ),
            ),




             ///Loader
            _con.loadingChats
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
            : Positioned(bottom: 10, child: SizedBox(height: 0)),
          ],
        ),
    );
  }

}