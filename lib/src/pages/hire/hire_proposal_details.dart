import 'package:doctor_finder/src/controllers/hire_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class HireProposalDetails extends StatefulWidget {
  RouteArgument argument;
  HireProposalDetails({this.argument});

  @override
  _HireProposalDetailsState createState() => _HireProposalDetailsState();
}

class _HireProposalDetailsState extends StateMVC<HireProposalDetails> {

  HireController _con;

  _HireProposalDetailsState() : super(HireController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.request = widget.argument.request;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = _con.request.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return WillPopScope(
      onWillPop: () async {
        if(_con.isLoading) {
          return false;
        }
        return true;
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: _appbar(),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 7),
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(width: 0.5, color: theme.accentColor)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text("Sent to: ", style: textTheme.headline5,),
                          SizedBox(width: 50),
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.request.nurse, getUserDetails: true));
                              },
                              child: Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: FadeInImage.assetNetwork(
                                      placeholder: "assets/img/placeholders/profile_placeholder.png",
                                      image: _con.request.nurse.image,
                                      width: 40,
                                      height: 40,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(_con.request.nurse.name,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: textTheme.subtitle1,),
                                        Text(_con.request.nurse.categoryName.toString().toUpperCase(),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: textTheme.caption,)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),

                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("For Job", style: textTheme.headline5),
                          SizedBox(height: 5,),
                          Container(height: 0.3, width: size.width, color: theme.focusColor,),
                          SizedBox(height: 5,),
                          Row(
                            children: [
                              Icon(Icons.arrow_right, color: theme.accentColor,),
                              SizedBox(width: 5,),
                              Text(_con.request.job.title, style: textTheme.subtitle1,),
                            ],
                          ),
                          SizedBox(height: 5,),
                          Text(_con.request.job.description,
                              style: textTheme.bodyText2),

                          SizedBox(height: 10,),

                          Container(height: 0.3, width: size.width, color: theme.focusColor,),

                          SizedBox(height: 5,),
                          Row(
                            children: [
                              Icon(Icons.message, color: theme.accentColor,),
                              SizedBox(width: 5,),
                              Text("Your Message...", style: textTheme.subtitle1,),
                            ],
                          ),
                          SizedBox(height: 5,),
                          Text(_con.request.description,
                              style: textTheme.bodyText2),

                          SizedBox(height: 5,),

                          Container(height: 0.3, width: size.width, color: theme.focusColor,),

                          SizedBox(height: 10,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('on $day at $time', style: textTheme.caption,),
                              Text(_con.request.status, style: textTheme.caption,)
                            ],
                          ),

                          SizedBox(height: 5,),

                        ],
                      )

                    ],
                  ),
                ),

                SizedBox(height: 15),


                _con.isLoading
                    ? NeuButtonContainer(
                        onPressed: () {},
                        width: 50,
                        height: 50,
                        borderRadius: 100,
                        child: CircularProgressIndicator(),
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          NeuButtonContainer(
                            onPressed: () {
                              _con.withdrawRequest();
                            },
                            width: 180,
                            height: 50,
                            borderRadius: 100,
                            child: Center(
                              child: Text(
                                "WITHDRAW",
                                style: textTheme.subtitle1,
                              ),
                            ),
                          )
                        ],
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Proposal Details",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
