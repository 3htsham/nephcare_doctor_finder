import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/job_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class MyJobsSelectionWidget extends StatefulWidget {
  RouteArgument argument;

  MyJobsSelectionWidget({this.argument});

  @override
  _MyJobsSelectionWidgetState createState() => _MyJobsSelectionWidgetState();
}

class _MyJobsSelectionWidgetState extends StateMVC<MyJobsSelectionWidget> {

  JobController _con;

  _MyJobsSelectionWidgetState() : super(JobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
    _con.getMyJobs();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Select a Job", style: textTheme.headline5,),
        elevation: 0,
        backgroundColor: theme.scaffoldBackgroundColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){ Navigator.of(context).pop(); },
          icon: Icon(CupertinoIcons.back, color: theme.accentColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _con.jobs.isEmpty
                ? CircularLoadingWidget(height: 100)
                : ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: _con.jobs.length,
              itemBuilder: (context, index) {

                Job job = _con.jobs[index];

                var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
                var dateTime = format.parse(job.createdAt);
                var day = DateFormat('EEEE').format(dateTime);
                var time = DateFormat('hh:mm aa').format(dateTime);

                return InkWell(
                  onTap: (){
                    ///On View Job
                    Navigator.of(context).pushNamed("/JobDetails",
                        arguments: RouteArgument(job: _con.jobs[index], currentUser: _con.currentUser));
                  },
                  child: Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(flex: 1,
                                      child: Text(job.title ?? "",
                                          style: textTheme.headline5)
                                  ),
                                  IconButton(
                                    onPressed: (){
                                      ///On Select
                                      Navigator.of(context).pop(job);
                                    },
                                    icon: Icon(CupertinoIcons.checkmark_circle_fill, size: 18, color: theme.accentColor,),
                                  )
                                ],
                              ),
                              SizedBox(height: 7),
                              Row(
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(100),
                                        child: _con.currentUser.image != null && _con.currentUser.image.length > 0
                                            ? CachedNetworkImage(
                                          imageUrl: _con.currentUser.image,
                                          width: 20,
                                          height: 20,
                                          fit: BoxFit.cover,
                                        )
                                            : Image.asset(
                                          "assets/img/placeholders/profile_placeholder.png",
                                          width: 20,
                                          height: 20,
                                          fit: BoxFit.cover,
                                        )
                                    ),
                                    SizedBox(width: 10),
                                    Container(
                                        constraints: BoxConstraints(maxWidth: 120),
                                        child: Text(_con.currentUser.name ?? "Job Poster",
                                          style: textTheme.bodyText2,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                    ),
                                    SizedBox(width: 10),
                                    Text('$day at $time', style: textTheme.caption),
                                  ]
                              ),
                              SizedBox(height: 10),
                              Container(
                                width: size.width,
                                child: Text(job.description ?? "",
                                    maxLines: 3,
                                    overflow: TextOverflow.fade,
                                    style: textTheme.bodyText1),
                              ),
                              SizedBox(height: 7),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Icon(CupertinoIcons.clock, size: 16, color: config.Colors().textColor(1),),
                                      SizedBox(width: 5,),
                                      Text('${job.hours ?? 0} hours', style: textTheme.caption,)
                                    ],
                                  ),
                                  SizedBox(width: 25,),
                                  Row(
                                    children: [
                                      Icon(Icons.attach_money, size: 16, color: config.Colors().textColor(1),),
                                      SizedBox(width: 5,),
                                      Text(job.pay, style: textTheme.caption,)
                                    ],
                                  ),
                                ],
                              ),

                              SizedBox(height: 10),
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                height: 0.2, color: theme.focusColor,
                              )
                            ]
                        ),
                      )
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
