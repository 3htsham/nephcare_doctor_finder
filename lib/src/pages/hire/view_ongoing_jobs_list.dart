import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/controllers/ongoing_jobs_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';

class OngoingJobsListsWidget extends StatefulWidget {
  RouteArgument argument;

  OngoingJobsListsWidget({this.argument});

  @override
  _OngoingJobsListsWidgetState createState() => _OngoingJobsListsWidgetState();
}

class _OngoingJobsListsWidgetState extends StateMVC<OngoingJobsListsWidget> {

  OnGoingJobController _con;

  _OngoingJobsListsWidgetState() : super(OnGoingJobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getMyOnGoingJobs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [

              _con.jobTracks.isEmpty
                  ? CircularLoadingWidget(height: 100)
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: _con.jobTracks.length,
                itemBuilder: (context, index) {

                  var dateTime = _con.jobTracks[index].time;
                  var day = DateFormat('EEEE').format(dateTime);
                  var time = DateFormat('hh:mm aa').format(dateTime);



                  return InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed("/ViewOnGoingJobDetails", arguments:
                        RouteArgument(currentUser: _con.currentUser, jobToTrack: _con.jobTracks[index]));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 7),
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(width: 0.5, color: theme.accentColor)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text("Hired: ", style: textTheme.headline5,),
                              SizedBox(width: 50),
                              InkWell(
                                onTap: (){
                                  Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.jobTracks[index].nurse, getUserDetails: true));
                                },
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: _con.jobTracks[index].sender.image == null
                                          ? Image.asset(
                                        "assets/img/placeholders/profile_placeholder.png",
                                        width: 50,
                                        height: 50,
                                        fit: BoxFit.cover,
                                      )
                                          : FadeInImage.assetNetwork(
                                        placeholder: "assets/img/placeholders/profile_placeholder.png",
                                        image: _con.jobTracks[index].nurse.image,
                                        width: 50,
                                        height: 50,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    SizedBox(width: 10,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(_con.jobTracks[index].nurse.name, style: textTheme.subtitle1,),
                                        Text(_con.jobTracks[index].nurse.category.toString() == GlobalVariables.hospitalType ? "Hospital" : "",
                                          style: textTheme.caption,)
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: 10,),

                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Budget on Hold: ", style: textTheme.headline5),
                              Text('\$${_con.jobTracks[index].pay.toString()}', style: textTheme.bodyText1,)
                            ],
                          ),

                          SizedBox(height: 10,),

                          _con.jobTracks[index].job != null ?
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("For Job", style: textTheme.headline5),
                              SizedBox(height: 5,),
                              Container(height: 0.3, width: size.width, color: theme.focusColor,),
                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.arrow_right, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text(_con.jobTracks[index].job.title, style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.jobTracks[index].job.description,
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: textTheme.bodyText2),
                            ],
                          )
                              :
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Details", style: textTheme.headline5),
                              SizedBox(height: 5,),
                              Container(height: 0.3, width: size.width, color: theme.focusColor,),
                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.arrow_right, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text("Message:", style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.jobTracks[index].description,
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: textTheme.bodyText2),
                            ],
                          ),

                          SizedBox(height: 10,),
                          Text('Started on $day at $time', style: textTheme.caption,),

                        ],
                      ),
                    ),
                  );
                },
              ),

            ],
          ),
        ),
      ),
    );

  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("OnGoing Jobs",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
