import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/controllers/ongoing_jobs_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class OngoingJobDetailsWidget extends StatefulWidget {
  RouteArgument argument;

  OngoingJobDetailsWidget({this.argument});

  @override
  _OngoingJobDetailsWidgetWidgetState createState() => _OngoingJobDetailsWidgetWidgetState();
}

class _OngoingJobDetailsWidgetWidgetState extends StateMVC<OngoingJobDetailsWidget> {

  OnGoingJobController _con;

  _OngoingJobDetailsWidgetWidgetState() : super(OnGoingJobController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.jobToTrack = widget.argument.jobToTrack;
    _con.getCurrentUser();
    _con.getUserDetails();
    _con.getJobToTrackDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var dateTime = _con.jobToTrack.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);


    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                children: [

                  Container(
                    margin: EdgeInsets.symmetric(vertical: 7),
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(width: 0.5, color: theme.accentColor)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text("Sent to: ", style: textTheme.headline5,),
                            SizedBox(width: 50),
                            Expanded(
                              child: InkWell(
                                onTap: (){
                                  Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.jobToTrack.nurse, getUserDetails: true));
                                },
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: FadeInImage.assetNetwork(
                                        placeholder: "assets/img/placeholders/profile_placeholder.png",
                                        image: _con.jobToTrack.nurse.image,
                                        width: 40,
                                        height: 40,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    SizedBox(width: 10,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(_con.jobToTrack.nurse.name,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: textTheme.subtitle1,),
                                          Text(_con.jobToTrack.nurse.categoryName ?? "",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: textTheme.caption,)
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _con.jobToTrack.job != null ? Text("For Job", style: textTheme.headline5) : SizedBox(height: 0),
                            SizedBox(height: _con.jobToTrack.job != null ? 5 : 0,),
                            _con.jobToTrack.job != null ? Container(height: 0.3, width: size.width, color: theme.focusColor,) : SizedBox(height: 0),
                            _con.jobToTrack.job != null ? SizedBox(height: 5,) : SizedBox(height: 0),
                            _con.jobToTrack.job != null ? Row(
                              children: [
                                Icon(Icons.arrow_right, color: theme.accentColor,),
                                SizedBox(width: 5,),
                                Text(_con.jobToTrack.job.title, style: textTheme.subtitle1,),
                              ],
                            ) : SizedBox(height: 0),
                            SizedBox(height: _con.jobToTrack.job != null ? 5 : 0,),
                            _con.jobToTrack.job != null ? Text(_con.jobToTrack.job.description,
                                style: textTheme.bodyText2) : SizedBox(height: 0),

                            SizedBox(height: _con.jobToTrack.job != null ? 10 : 0,),

                            Container(height: 0.3, width: size.width, color: theme.focusColor,),

                            SizedBox(height: 5,),
                            Row(
                              children: [
                                Icon(Icons.message, color: theme.accentColor,),
                                SizedBox(width: 5,),
                                Text("Your Message...", style: textTheme.subtitle1,),
                              ],
                            ),
                            SizedBox(height: 5,),
                            Text(_con.jobToTrack.description,
                                style: textTheme.bodyText2),

                            SizedBox(height: 5,),

                            Container(height: 0.3, width: size.width, color: theme.focusColor,),

                            SizedBox(height: 10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Started on $day at $time', style: textTheme.caption,),
                                Text(_con.jobToTrack.status, style: textTheme.caption,)
                              ],
                            ),

                            SizedBox(height: 5,),

                          ],
                        )

                      ],
                    ),
                  ),


                  SizedBox(height: 15),

                  _con.isLoading
                      ? NeuButtonContainer(
                    onPressed: () {},
                    width: 50,
                    height: 50,
                    borderRadius: 100,
                    child: CircularProgressIndicator(),
                  )
                      : Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [


                      NeuButtonContainer(
                        onPressed: () {
                          setState((){
                            _con.showWithdrawDialog  =true;
                          });
                        },
                        width: 230,
                        height: 50,
                        borderRadius: 100,
                        child: Center(
                          child: Text(
                            "WITHDRAW HIRE",
                            style: textTheme.subtitle1,
                          ),
                        ),
                      ),

                      SizedBox(height: 15),

                      NeuButtonContainer(
                        onPressed: () {
                          setState((){
                            _con.showCompleteDialog  =true;
                          });
                        },
                        width: 230,
                        height: 50,
                        borderRadius: 100,
                        child: Center(
                          child: Text(
                            "MARK AS COMPLETE",
                            style: textTheme.subtitle1,
                          ),
                        ),
                      ),

                    ],
                  )

                ],
              ),
            ),
          ),


          _con.showCompleteDialog
              ? Container(
            width: size.width,
            height: size.height,
            color: config.Colors().scaffoldColor(0.5),
            child: Center(
              child: NeuContainer(
                width: 230,
                height: 190,
                borderRadius: 10,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    children: [
                      Text("Complete Job", style: textTheme.subtitle1),
                      SizedBox(height: 5,),
                      Container(height: 0.3, width: 150, color: config.Colors().mainColor(1),),
                      SizedBox(height: 5,),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Are you sure you want to mark this job as complete?\n\n"
                            "The on-hold balance of \$${_con.jobToTrack.pay} will be transferred to nurse's account.",
                          style: textTheme.bodyText2,),
                      ),

                      Spacer(),

                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          children: [

                            NeuButtonContainer(
                              onPressed: (){
                                _con.completeHire();
                              },
                              width: 80,
                              height: 40,
                              borderRadius: 10,
                              child: Center(
                                child: GradientIcon(CupertinoIcons.checkmark_circle_fill, size: 22, gradient: config.Colors().iconGradient(),),
                              ),
                            ),

                            Spacer(),

                            NeuButtonContainer(
                              onPressed: (){
                                setState((){
                                  _con.showCompleteDialog = false;
                                });
                              },
                              width: 80,
                              height: 40,
                              borderRadius: 10,
                              child: Center(
                                child: GradientIcon(CupertinoIcons.clear_circled_solid, size: 20, gradient: config.Colors().iconGradient(),),
                              ),
                            )
                          ],
                        ),
                      )

                    ],
                  ),
                ),
              ),
            ),
          ) : SizedBox(height: 0, width: 0,),

          _con.showWithdrawDialog
              ? Container(
            width: size.width,
            height: size.height,
            color: config.Colors().scaffoldColor(0.5),
            child: Center(
              child: NeuContainer(
                width: 230,
                height: 150,
                borderRadius: 10,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    children: [
                      Text("Withdraw Hire", style: textTheme.subtitle1),
                      SizedBox(height: 5,),
                      Container(height: 0.3, width: 150, color: config.Colors().mainColor(1),),
                      SizedBox(height: 5,),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Are you sure you want to withdraw this job hire?",
                          style: textTheme.bodyText2,),
                      ),

                      Spacer(),

                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          children: [
                            NeuButtonContainer(
                              onPressed: (){
                                _con.withdrawHire();
                              },
                              width: 80,
                              height: 40,
                              borderRadius: 10,
                              child: Center(
                                child: GradientIcon(CupertinoIcons.checkmark_circle_fill, size: 22, gradient: config.Colors().iconGradient(),),
                              ),
                            ),

                            Spacer(),

                            NeuButtonContainer(
                              onPressed: (){
                                setState((){_con.showWithdrawDialog = false;});
                              },
                              width: 80,
                              height: 40,
                              borderRadius: 10,
                              child: Center(
                                child: GradientIcon(CupertinoIcons.clear_circled_solid, size: 20, gradient: config.Colors().iconGradient(),),
                              ),
                            )
                          ],
                        ),
                      )

                    ],
                  ),
                ),
              ),
            ),
          )
              : SizedBox(height: 0, width: 0,),

        ],
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("OnGoing Jobs",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
