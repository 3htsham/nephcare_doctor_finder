import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/src/controllers/hire_controller.dart';
import 'package:doctor_finder/src/elements/other/date_formatter.dart';
import 'package:doctor_finder/src/elements/other/time_formatter.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class HireNowWidget extends StatefulWidget {
  RouteArgument argument;

  HireNowWidget({this.argument});

  @override
  _HireNowWidgetState createState() => _HireNowWidgetState();
}

class _HireNowWidgetState extends StateMVC<HireNowWidget> {

  HireController _con;

  _HireNowWidgetState() : super(HireController()) {
    _con = controller;
  }


  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.nurse = widget.argument.user;
    if( widget.argument.job != null) {
      _con.job = widget.argument.job;
      _con.hirePay = double.parse(widget.argument.job.pay.toString());
      _con.hirePayController.text = widget.argument.job.pay;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isLoading) {
          return false;
        }
        return true;
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        appBar: _appbar(),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("You're about to hire...", style: textTheme.headline5,),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 0.5, color: theme.accentColor),
                        borderRadius: BorderRadius.circular(5)
                      ),
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pushNamed("/DoctorProfile",
                              arguments: RouteArgument(user: _con.nurse, getUserDetails: true, currentUser: _con.currentUser));
                        },
                        child: Container(
                          child: Row(
                            children: [
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: CachedNetworkImage(
                                    imageUrl: _con.nurse.image,
                                    width: 50,
                                    height: 50,
                                    fit: BoxFit.cover,
                                  )
                              ),
                              SizedBox(width: 10),
                              Expanded(child: Text(_con.nurse.name, style: textTheme.subtitle1.merge(TextStyle(fontSize: 14)),)),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),

                _con.currentUser.type == GlobalVariables.hospitalType
                    ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20,),
                    Text("Against Job...", style: textTheme.headline5,),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                          border: Border.all(width: 0.5, color: theme.accentColor),
                          borderRadius: BorderRadius.circular(5)
                      ),
                      child: _con.job == null
                          ? InkWell(
                        onTap: (){
                          Navigator.of(context).pushNamed("/JobSelection",
                            arguments: RouteArgument(currentUser: _con.currentUser)
                          ).then((_job) {
                            if(_job != null) {
                              setState((){
                                _con.job = _job;
                                _con.hirePay = double.parse(_con.job.pay.toString());
                                _con.hirePayController.text = _con.job.pay.toString();
                              });
                            }
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                          child: Row(
                            children: [
                              Text("Select a job to hire nurse", style: textTheme.bodyText1,),
                              Spacer(),
                              Icon(CupertinoIcons.forward, color: theme.accentColor,)
                            ],
                          ),
                        ),
                      )
                          :
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(child: Text(_con.job.title, style: textTheme.subtitle1,)),
                              IconButton(
                                onPressed: (){
                                  if(!_con.isLoading) {
                                    Navigator.of(context).pushNamed(
                                        "/JobSelection",
                                        arguments: RouteArgument(
                                            currentUser: _con.currentUser)
                                    ).then((_job) {
                                      if (_job != null) {
                                        setState(() {
                                          _con.job = _job;
                                        });
                                      }
                                    });
                                  }
                                },
                                icon: Icon(
                                  CupertinoIcons.pen,
                                  color: theme.accentColor,
                                  size: 18,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 5,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Icon(CupertinoIcons.clock, size: 16, color: config.Colors().textColor(1),),
                                  SizedBox(width: 5,),
                                  Text('${_con.job.hours ?? 0} hours', style: textTheme.caption,)
                                ],
                              ),
                              SizedBox(width: 25,),
                              Row(
                                children: [
                                  Icon(Icons.attach_money, size: 16, color: config.Colors().textColor(1),),
                                  SizedBox(width: 5,),
                                  Text(_con.job.pay, style: textTheme.caption,)
                                ],
                              ),
                              SizedBox(width: 10,),
                            ],
                          ),
                          SizedBox(height: 5,),
                          Text(_con.job.description, style: textTheme.bodyText2, maxLines: 2, overflow: TextOverflow.ellipsis,),
                        ],
                      ),
                    ),
                  ],
                )
                : SizedBox(height: 0,),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20,),
                    Text("More details...", style: textTheme.headline5,),
                    SizedBox(height: 10,),
                    Form(
                      key: _con.hireFormKey,
                      child: Column(
                        children: [
                          NeuTextFieldContainer(
                            borderRadius: BorderRadius.circular(10),
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled: !_con.isLoading,
                              controller: _con.descriptionController,
                              minLines: 7,
                              maxLines: null,
                              textInputAction: TextInputAction.newline,
                              validator: (val) => val.isEmpty ? "Field can't be empty":null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Type details about job here...",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(Icons.menu, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (value) {
                                _con.hireDetailsMessage = value;
                              },
                            ),
                          ),

                          SizedBox(height: 20),

                          _con.currentUser.type != GlobalVariables.hospitalType
                          ? NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled:  !_con.isLoading,
                              controller: _con.hirePayController,
                              validator: (val) => (val.isEmpty && _con.currentUser.type != GlobalVariables.hospitalType) ? "Enter Valid Email":null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Email",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(CupertinoIcons.mail, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val){
                                _con.hirePay = double.parse(val);
                              },
                            ),
                          )
                          : SizedBox(height: 0),

                          SizedBox(height: _con.currentUser.type != GlobalVariables.hospitalType ? 20 : 0),

                          ListTile(
                            title: Text("Schedule Hire", style: textTheme.headline5,),
                            subtitle: Text("Set a schedule for the day when you want to hire her."),
                            leading: GradientIcon(CupertinoIcons.clock_solid, size: 22, gradient: config.Colors().iconGradient(),),
                            trailing: InkWell(
                              onTap: (){
                                setState((){
                                  _con.isScheduled = !_con.isScheduled;
                                });
                              },
                              child: NeuTextFieldContainer(
                                textField: AnimatedContainer(
                                  duration: Duration(milliseconds: 600),
                                  width: 20,
                                  height: 20,
                                  decoration: BoxDecoration(
                                      color: _con.isScheduled ? theme.accentColor : theme.scaffoldBackgroundColor,
                                      borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                            color: config.Colors().shadowLight,
                                            blurRadius: 3,
                                            offset: Offset(-0.5, 0.5)
                                        ),
                                        BoxShadow(
                                          color: config.Colors().shadowDark,
                                          blurRadius: 3,
                                          offset: Offset(0.5, -0.5),
                                        ),
                                      ]
                                  ),
                                  margin: EdgeInsets.only(top: 3, bottom: 3, right: _con.isScheduled ? 3 : 20, left: _con.isScheduled ? 20 : 3),
                                ),
                              ),
                            ),
                          ),


                          _con.isScheduled
                              ? Column(
                                  children: [
                                    SizedBox(height: 20),
                                    NeuTextFieldContainer(
                                      borderRadius: BorderRadius.circular(10),
                                      padding: EdgeInsets.symmetric(horizontal: 5),
                                      textField: TextFormField(
                                        inputFormatters: [
                                          DateTextInputFormatter()
                                        ],
                                        validator: (val) =>
                                            ((_con.isScheduled &&
                                                        val.isEmpty) ||
                                                    (_con.isScheduled &&
                                                        val.length < 10))
                                                ? "Enter a valid date"
                                                : null,
                                        style: textTheme.bodyText1.merge(
                                            TextStyle(
                                                color: config.Colors()
                                                    .textColor(1))),
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Schedule date",
                                          hintStyle: textTheme.bodyText1.merge(
                                              TextStyle(
                                                  color: theme.hintColor)),
                                          prefixIcon: Icon(
                                            Icons.calendar_today,
                                            color: config.Colors().textColor(1),
                                          ),
                                          prefixIconConstraints: BoxConstraints(
                                              maxWidth: 100, minWidth: 50),
                                        ),
                                        onSaved: (value) {
                                          _con.hireDate = value;
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    NeuTextFieldContainer(
                                      borderRadius: BorderRadius.circular(10),
                                      padding: EdgeInsets.symmetric(horizontal: 5),
                                      textField: TextFormField(
                                        inputFormatters: [
                                          TimeTextInputFormatter()
                                        ],
                                        validator: (val) => ((_con.isScheduled && val.isEmpty) || (_con.isScheduled && val.length < 5))
                                                ? "Time not valid"
                                                : null,
                                        style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Schedule time (24hr format 00:00)",
                                          hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                          prefixIcon: Icon(Icons.calendar_today,color: config.Colors().textColor(1),),
                                          prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                        ),
                                        onSaved: (value) {
                                          _con.hireTime = value;
                                        },
                                      ),
                                    ),
                                  ],
                                )
                              : SizedBox(height: 0, width: 0),
                        ],
                      ),
                    )
                  ],
                ),

                SizedBox(height: 20,),

                _con.isLoading ? NeuContainer(
                    borderRadius: 100,
                    width: 50,
                    height: 50,
                    child: CircularProgressIndicator()
                )
                    : NeuButton(
                  width: 220,
                  height: 50,
                  text: "Send Hire Request".toUpperCase(),
                  textStyle: textTheme.subtitle2,
                  borderRadius: 100,
                  onPressed: (){
                    _con.sendHireRequest();
                  },
                ),


                SizedBox(height: 20),


              ],
            ),
          ),
        ),
      ),
    );
  }


  Widget _appbar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Hire",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
