import 'package:doctor_finder/src/controllers/hire_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/models/hire_request.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class MyHireProposalsWidget extends StatefulWidget {
  RouteArgument argument;

  MyHireProposalsWidget({this.argument});

  @override
  _MyHireProposalsWidgetState createState() => _MyHireProposalsWidgetState();
}

class _MyHireProposalsWidgetState extends StateMVC<MyHireProposalsWidget> {

  HireController _con;

  _MyHireProposalsWidgetState() : super(HireController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.getMyHireRequests();
    _con.getMyInstantHireRequests();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [
              _con.instantRequests.isEmpty
                  ? SizedBox()
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: _con.instantRequests.length,
                itemBuilder: (context, index) {
                  final _req = _con.instantRequests[index];

                  // var dateTime = _con.requests[index].time;
                  // var day = DateFormat('EEEE').format(dateTime);
                  // var time = DateFormat('hh:mm aa').format(dateTime);

                  return _buildInstantReqItem(_req);
                },
              ),
              _con.requests.isEmpty
                  ? CircularLoadingWidget(height: 100)
                  : ListView.builder(
                shrinkWrap: true,
                  primary: false,
                itemCount: _con.requests.length,
                itemBuilder: (context, index) {
                  final _req = _con.requests[index];

                  // var dateTime = _con.requests[index].time;
                  // var day = DateFormat('EEEE').format(dateTime);
                  // var time = DateFormat('hh:mm aa').format(dateTime);

                  return _buildNormalReqItem(_req);


                  /*
                  return InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed("/HireRequestDetails", arguments: RouteArgument(currentUser: _con.currentUser, request: _con.requests[index]));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 7),
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(width: 0.5, color: theme.accentColor)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text("Sent to: ", style: textTheme.headline5,),
                              SizedBox(width: 50),
                              Expanded(
                                child: InkWell(
                                  onTap: (){
                                    Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.requests[index].nurse, getUserDetails: true));
                                  },
                                  child: Row(
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(100),
                                        child: FadeInImage.assetNetwork(
                                            placeholder: "assets/img/placeholders/profile_placeholder.png",
                                            image: _con.requests[index].nurse.image,
                                          width: 40,
                                          height: 40,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      SizedBox(width: 10,),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(_con.requests[index].nurse.name,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: textTheme.subtitle1,),
                                            Text(_con.requests[index].nurse.categoryName.toString().toUpperCase(),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: textTheme.caption,)
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("For Job", style: textTheme.headline5),
                              SizedBox(height: 5,),
                              Container(height: 0.3, width: size.width, color: theme.focusColor,),
                              SizedBox(height: 5,),
                              Row(
                                children: [
                                  Icon(Icons.arrow_right, color: theme.accentColor,),
                                  SizedBox(width: 5,),
                                  Text(_con.requests[index].job.title, style: textTheme.subtitle1,),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Text(_con.requests[index].job.description,
                                  maxLines: 2,
                                  overflow: TextOverflow.fade,
                                  style: textTheme.bodyText2),

                              SizedBox(height: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('on $day at $time', style: textTheme.caption,),
                                  Text(_con.requests[index].status, style: textTheme.caption,)
                                ],
                              ),


                            ],
                          )

                        ],
                      ),
                    ),
                  );
                  */
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNormalReqItem(HireRequest _req) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = _req.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return InkWell(
      onTap: (){
        Navigator.of(context).pushNamed("/HireRequestDetails", arguments: RouteArgument(currentUser: _con.currentUser, request: _req));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7),
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 0.5, color: theme.accentColor)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text("Sent to: ", style: textTheme.headline5,),
                SizedBox(width: 50),
                Expanded(
                  child: InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _req.nurse, getUserDetails: true));
                    },
                    child: Row(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: FadeInImage.assetNetwork(
                            placeholder: "assets/img/placeholders/profile_placeholder.png",
                            image: _req.nurse.image,
                            width: 40,
                            height: 40,
                            fit: BoxFit.cover,
                          ),
                        ),
                        SizedBox(width: 10,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(_req.nurse.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: textTheme.subtitle1,),
                              Text(_req.nurse.categoryName.toString().toUpperCase(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: textTheme.caption,)
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("For Job", style: textTheme.headline5),
                SizedBox(height: 5,),
                Container(height: 0.3, width: size.width, color: theme.focusColor,),
                SizedBox(height: 5,),
                Row(
                  children: [
                    Icon(Icons.arrow_right, color: theme.accentColor,),
                    SizedBox(width: 5,),
                    Text(_req.job.title, style: textTheme.subtitle1,),
                  ],
                ),
                SizedBox(height: 5,),
                Text(_req.job.description,
                    maxLines: 2,
                    overflow: TextOverflow.fade,
                    style: textTheme.bodyText2),

                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('on $day at $time', style: textTheme.caption,),
                    Text(_req.status, style: textTheme.caption,)
                  ],
                ),


              ],
            )

          ],
        ),
      ),
    );
  }

  Widget _buildInstantReqItem(HireRequest _req) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = _req.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return InkWell(
      onTap: (){
        // Navigator.of(context).pushNamed("/HireRequestDetails", arguments: RouteArgument(currentUser: _con.currentUser, request: _req));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7),
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 7),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 0.5, color: theme.accentColor)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(Icons.stream, color: theme.accentColor,),
                SizedBox(width: 20),
                Text("Created Instant Request for ", style: textTheme.headline5,),
                Spacer(),
                TextButton(
                    onPressed: (){
                      //TODO: Withdraw request
                      _con.withdrawInstantRequest(_req);
                    },
                    child: Text("Withdraw", style: textTheme.caption,)
                )
              ],
            ),
            SizedBox(height: 10,),
            Text('${_req.categoryName ?? ''}', style: textTheme.bodyText1,),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('on $day at $time', style: textTheme.caption,),
                Text(_req.status, style: textTheme.caption,)
              ],
            ),

          ],
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Hire Proposals",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            if(!_con.isLoading) {
              Navigator.of(context).pop();
            }
          }
      ),
      actions: [],
    );
  }
}
