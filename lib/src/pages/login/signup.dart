import 'package:doctor_finder/src/controllers/user_controller.dart';
import 'package:doctor_finder/src/models/user_type.dart';
import 'package:doctor_finder/src/neumorphic/neu_button.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:doctor_finder/src/neumorphic/nue_check_box.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends StateMVC<SignUp> {

  UserController _con;

  _SignUpState(): super(UserController()){
    _con = controller;
  }



  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
      key: _con.scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: Container(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 30,),
                  Hero(
                    tag: "Logo",
                    child: Align(
                        alignment: Alignment.center,
                        child: Image.asset("assets/img/logo.png", width: 100,)
                    ),
                  ),
                  SizedBox(height: 15,),
                  Hero(tag: "welcome" ,child: Text("FIND AROUND YOU!", style: textTheme.headline5,)),
                  SizedBox(height: 25,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30),
                    alignment: Alignment.center,
                    child: Form(
                      key: _con.loginFormKey,
                      child: Column(
                        children: [
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled:  !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Name must not be empty":null,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Name",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(CupertinoIcons.person, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                              ),
                              onSaved: (val){
                                _con.user.name = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),
                          NeuTextFieldContainer(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              textField: TextFormField(
                                enabled:  !_con.isLoading,
                                validator: (val) => val.isEmpty ? "Enter Valid Email":null,
                                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Email",
                                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                  prefixIcon: Icon(CupertinoIcons.mail, color: config.Colors().textColor(1),),
                                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                ),
                                onSaved: (val){
                                  _con.user.email = val;
                                },
                              ),
                            ),
                          SizedBox(height: 15,),
                          NeuTextFieldContainer(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            textField: TextFormField(
                              enabled:  !_con.isLoading,
                              validator: (val) => val.isEmpty ? "Password must not be empty":null,
                              obscureText: !_con.displayPassword,
                              style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: "Password",
                                hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                                prefixIcon: Icon(CupertinoIcons.padlock, color: config.Colors().textColor(1),),
                                prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                                suffixIcon: IconButton(
                                  padding: EdgeInsets.all(0),
                                  onPressed: (){
                                    setState((){
                                      _con.displayPassword = !_con.displayPassword;
                                    });
                                  },
                                  icon: Icon(_con.displayPassword
                                      ? Icons.visibility_off_outlined
                                      : Icons.visibility_outlined,
                                    color: theme.focusColor,),
                                )
                              ),
                              onSaved: (val){
                                _con.user.password = val;
                              },
                            ),
                          ),
                          SizedBox(height: 15,),

                          Container(
                            alignment: Alignment.center,
                            child: Column(
                              children: [
                                NeuTextFieldContainer(
                                  borderRadius: BorderRadius.circular(15),
                                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                  textField: Container(
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    child: ListView.builder(
                                        primary: false,
                                        shrinkWrap: true,
                                        padding: EdgeInsets.symmetric(vertical: 10),
                                        itemCount: _con.userTypes.length,
                                        itemBuilder: (context, index) {
                                          UserType utype = _con.userTypes[index];
                                          return InkWell(
                                            onTap: (){
                                              if(!_con.isLoading) {
                                                setState((){
                                                  _con.selectedType = utype;
                                                });
                                              }
                                            },
                                            child: Container(
                                              padding: EdgeInsets.symmetric(vertical: 5),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Text(utype.text, style: textTheme.bodyText1,),
                                                  Spacer(),
                                                  NeuBtnCheckBox(
                                                    onPressed: (){
                                                      setState((){
                                                        _con.selectedType = utype;
                                                      });
                                                    },
                                                    state: _con.selectedType != null && _con.selectedType.typeValue==utype.typeValue,
                                                    borderRadius: 100,
                                                    width: 30,
                                                    height: 30,
                                                    child: Icon(CupertinoIcons.check_mark, color: _con.selectedType != null && _con.selectedType.typeValue==utype.typeValue ? theme.accentColor : Colors.white,),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        }
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15,),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),

                  // SizedBox(height: 15,),
                  _con.isLoading
                      ? NeuContainer(
                          borderRadius: 100,
                          width: 50,
                          height: 50,
                          child: CircularProgressIndicator())
                      : Hero(
                          tag: "LoginSignupButton",
                          child: NeuButton(
                            width: 150,
                            height: 50,
                            text: "SIGNUP".toUpperCase(),
                            textStyle: textTheme.subtitle2,
                            borderRadius: 100,
                            onPressed: _con.register,
                          ),
                        ),
                  SizedBox(height: 20,),
                  Hero(tag: "OR",child: Text("OR", style: textTheme.caption,)),
                  SizedBox(height: 10,),
                  InkWell(
                    onTap: () {
                      if (!_con.isLoading) {
                        Navigator.of(context).pushReplacementNamed("/Login");
                      }
                    },
                    splashColor: theme.primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("LOGIN", style: textTheme.bodyText1,),
                    ),
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),
          ),
        )
    );
  }
}
