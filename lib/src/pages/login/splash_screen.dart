import 'dart:async';
import 'dart:ui';

import 'package:doctor_finder/generated/i18n.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:lottie/lottie.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/controllers/controller.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends StateMVC<SplashScreen> {
  Controller _con;
  final player = AudioPlayer();

  _SplashScreenState() :super(Controller()) {
    _con = controller;
  }

  @override
  void initState() {
    playAudio();
    super.initState();
    loadData();
  }

  playAudio() {
    player.setVolume(1);
    player.setAsset("assets/heart_beat.mp3");
    player.play();
  }

  @override
  void dispose() {
    player?.stop();
    super.dispose();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(milliseconds: 3000), onDoneLoading);
  }

  onDoneLoading() async {
    if (userRepo.currentUser.apiToken == null) {
      Navigator.of(context).pushReplacementNamed('/Login');
    } else {
      if (userRepo.currentUser.verified) {

        if ((userRepo.currentUser.lat != null && userRepo.currentUser.long != null) &&
            (((userRepo.currentUser.lat != 0 || userRepo.currentUser.lat != "0") &&
                    (userRepo.currentUser.long != 0 || userRepo.currentUser.long != "0"))
                || (userRepo.currentUser.lat.length > 0 && userRepo.currentUser.long.length > 0))) {

          Navigator.of(context).pushNamedAndRemoveUntil("/Pages",
                  (Route<dynamic> route) => false, arguments: 2);

        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(
              "/UpdateCurrentLocation", (Route<dynamic> route) => false,
              arguments: RouteArgument(currentUser: userRepo.currentUser));
        }

      } else {
        userRepo.logout().then((value) {
          Navigator.of(context).pushReplacementNamed('/Login');
        });
      }

    }
  }



  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
      key: _con.scaffoldKey,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.center,
              child: Hero(
                tag: "Logo",
                  child: Image.asset("assets/img/logo.png", width: 120,)
              ),
            ),
            SizedBox(height: 20,),
            Text(
              S.of(context).appName.toUpperCase(),
              style: textTheme.headline3,
            ),

            Lottie.asset('assets/ecg.json', width: 300)

            // ColorFiltered(
            //     colorFilter: ColorFilter.mode(Theme.of(context).accentColor, BlendMode.color),
                // child: Lottie.asset('assets/ecg.json', width: 300)),
          ],
        ),
      ),
    );
  }
}
