import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/tracking_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class TrackingsListWidget extends StatefulWidget {
  RouteArgument argument;

  TrackingsListWidget({this.argument});

  @override
  _TrackingsListWidgetState createState() => _TrackingsListWidgetState();
}

class _TrackingsListWidgetState extends StateMVC<TrackingsListWidget> {

  TrackingController _con;

  _TrackingsListWidgetState() : super(TrackingController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.getMyTrackings();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: config.Colors().appBar(context, "Trackings list"),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [

              _con.trackings.isEmpty
              ? CircularLoadingWidget(height: 100,)
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: _con.trackings.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Row(
                      children: [

                        Expanded(
                          child: Row(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: CachedNetworkImage(
                                  imageUrl: _con.trackings[index].nursePro.image,
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(width: 10,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(_con.trackings[index].nursePro.name, style: textTheme.subtitle1,),
                                  Text(_con.trackings[index].nursePro.categoryName ?? "", style: textTheme.caption,)
                                ],
                              )
                            ],
                          ),
                        ),

                        IconButton(
                            icon: Icon(CupertinoIcons.location_solid, color: theme.accentColor,),
                            onPressed: (){
                              Navigator.of(context).pushNamed("/TrackLocation", arguments: RouteArgument(trackingReqId: _con.trackings[index].id));
                            }
                        ),

                      ],
                    ),
                  );
                },
              )

            ],
          ),
        ),
      ),
    );
  }
}
