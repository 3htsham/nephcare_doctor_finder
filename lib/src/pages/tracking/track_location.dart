import 'package:doctor_finder/src/controllers/tracking_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class TrackLocationWidget extends StatefulWidget {

  RouteArgument argument;

  TrackLocationWidget({this.argument});

  @override
  _TrackLocationWidgetState createState() => _TrackLocationWidgetState();
}

class _TrackLocationWidgetState extends StateMVC<TrackLocationWidget> {

  TrackingController _con;

  _TrackLocationWidgetState() : super(TrackingController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.trackingId = widget.argument.trackingReqId;
    _con.getLocationPermission();
    _con.initLocationController();
    super.initState();
    _con.getTracking();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      body: Stack(
        children: [

          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              onMapCreated: (GoogleMapController controller) {
                _con.isMapCreated = true;
                _con.controller = controller;
                _con.controller.setMapStyle(_con.mapStyle);
                _con.getTracking();
              },
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                  target: LatLng(40.7237765, -74.017617), zoom: 13.0),

              onTap: (pos) {
                print(pos);
              },
              markers: Set.from(_con.allMarkers),
              polylines: Set.from(_con.polylines),
            ),
          ),

          Align(
            alignment: Alignment.topCenter,
            child: Container(
              padding: EdgeInsets.only(top: 40, right: 20, left: 20),
              height: 160,
              width: size.width,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [
                        theme.scaffoldBackgroundColor.withOpacity(1),
                        theme.scaffoldBackgroundColor.withOpacity(0.9),
                        theme.scaffoldBackgroundColor.withOpacity(0.8),
                        theme.scaffoldBackgroundColor.withOpacity(0.6),
                        theme.scaffoldBackgroundColor.withOpacity(0),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0, 0.5, 0.7, 0.9, 1]
                  )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      IconButton(icon: Icon(CupertinoIcons.back, color: theme.accentColor,), onPressed: (){Navigator.of(context).pop();}),
                      Text("Nurse Location...", style: textTheme.subtitle1,),
                    ],
                  ),
                  SizedBox(height: 10,),
                  IgnorePointer(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GradientIcon(
                          CupertinoIcons.location_solid,
                          size: 25.0,
                          gradient: LinearGradient(
                            colors: <Color>[
                              config.Colors().mainColor(1),
                              config.Colors().mainColorLight
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        SizedBox(width: 10,),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          width: size.width-80,
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.5, color: theme.focusColor),
                              borderRadius: BorderRadius.circular(3)
                          ),
                          child: Text(
                            _con.destination,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: textTheme.bodyText1,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
}
