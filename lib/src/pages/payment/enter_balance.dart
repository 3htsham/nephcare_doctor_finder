import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;
import 'package:doctor_finder/src/controllers/wallet_controller.dart';

class EnterBalanceWidget extends StatefulWidget {
  RouteArgument argument;
  EnterBalanceWidget({this.argument});

  @override
  _EnterBalanceWidgetState createState() => _EnterBalanceWidgetState();
}

class _EnterBalanceWidgetState extends StateMVC<EnterBalanceWidget> {

  WalletController _con;

  _EnterBalanceWidgetState() : super(WalletController()) {
    _con = controller;
  }

  @override
  void initState() {
    // _con.currentUser = widget.argument.currentUser;
    _con.getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Add Wallet Balance", style: textTheme.headline5,),
        elevation: 0,
        backgroundColor: theme.scaffoldBackgroundColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){ Navigator.of(context).pop(); },
          icon: Icon(CupertinoIcons.back, color: theme.accentColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [

              SizedBox(height: 20),

              Container(
                width: 250,
                child: Text("Enter amount below to add balance to your app wallet/account."
                    "\nThe amount will be deducted from the card account you have attached with your account.",
                  style: textTheme.caption,
                  textAlign: TextAlign.center,
                ),
              ),

              SizedBox(height: 30),

              Form(
                key: _con.formKey,
                child: Column(
                  children: [

                    NeuTextFieldContainer(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      textField: TextFormField(
                        enabled:  !_con.isLoading,
                        validator: (val) => val.isEmpty ? "Amount can't be empty":null,
                        style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Amount",
                          hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                          prefixIcon: Icon(Icons.attach_money, color: config.Colors().textColor(1),),
                          prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                        ),
                        onSaved: (val){
                          _con.amountToAdd = double.parse(val);
                        },
                      ),
                    ),
                  ],
                ),
              ),


              SizedBox(height: 30),

              _con.isLoading ? NeuContainer(
                  borderRadius: 100,
                  width: 50,
                  height: 50,
                  child: CircularProgressIndicator()
              )
                  : NeuButton(
                    width: 150,
                    height: 50,
                    text: "Add to wallet".toUpperCase(),
                    textStyle: textTheme.subtitle2,
                    borderRadius: 100,
                    onPressed: (){
                      _con.addBalanceToWallet();
                    },
                  ),

            ],
          ),
        ),
      ),
    );
  }
}
