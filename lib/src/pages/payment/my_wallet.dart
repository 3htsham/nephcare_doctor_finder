import 'package:doctor_finder/config/card_utils.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/repositories/settings_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/src/controllers/wallet_controller.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class MyWalletWidget extends StatefulWidget {
  RouteArgument argument;
  MyWalletWidget({this.argument});

  @override
  _MyWalletWidgetState createState() => _MyWalletWidgetState();
}

class _MyWalletWidgetState extends StateMVC<MyWalletWidget> {

  WalletController _con;

  _MyWalletWidgetState() : super(WalletController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.getCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    var isPaymentExists = _con.currentUser.payMethod != null &&
            _con.currentUser.payMethod.cardNumber != null &&
            _con.currentUser.payMethod.cardNumber.length > 3;

    var balance = _con.currentUser.balance != null ? _con.currentUser.balance : 0.0;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("My Wallet", style: textTheme.headline5,),
        elevation: 0,
        backgroundColor: theme.scaffoldBackgroundColor,
        centerTitle: true,
        leading: IconButton(
          onPressed: (){ Navigator.of(context).pop(); },
          icon: Icon(CupertinoIcons.back, color: theme.accentColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            SizedBox(height: 30),

            Container(child: Center(child: Text("Wallet".toUpperCase(), style: textTheme.headline1.merge(TextStyle(letterSpacing: 1,)),))),

            SizedBox(height: 15),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Container(child: Center(child: Text("Your Balance:  \$$balance".toUpperCase(), style: textTheme.subtitle1,))),

                  NeuButtonContainer(
                    height: 30,
                    width: 50,
                    borderRadius: 100,
                    onPressed: () {
                      if(isPaymentExists) {
                        Navigator.of(context).pushNamed("/AddWalletBalance").then((value) {
                          _con.getCurrentUser();
                        });
                      } else {
                        showToast("Add Payment Card First");
                      }
                    },
                    child: Center(
                      child: Text(
                        "+",
                        style: textTheme.subtitle1.merge(TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 10),



            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: NeuContainer(
                width: size.width,
                borderRadius: 10,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(
                          "Cards",
                          style: textTheme.headline5,
                        ),
                        leading: GradientIcon(
                          Icons.credit_card,
                          size: 22,
                          gradient: config.Colors().iconGradient(),
                        ),
                      ),

                      isPaymentExists
                          ? Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: Container(
                                    height: 150,
                                    width: 240,
                                    decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.8),
                                        borderRadius: BorderRadius.circular(15)),
                                    child: Stack(
                                      children: [

                                        Transform.translate(
                                          offset: const Offset(-15.0, -20.0),
                                          child: Container(
                                            width: 120,
                                            height: 120,
                                            decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.1),
                                                borderRadius: BorderRadius.circular(100)),
                                          ),
                                        ),

                                        Transform.translate(
                                          offset: const Offset(150.0, 100.0),
                                          child: Container(
                                            width: 105,
                                            height: 105,
                                            decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.3),
                                                borderRadius: BorderRadius.circular(100)),
                                          ),
                                        ),

                                        Transform.translate(
                                          offset: const Offset(-23.0, -20.0),
                                          child: Container(
                                            width: 105,
                                            height: 105,
                                            decoration: BoxDecoration(
                                                color: Colors.white.withOpacity(0.3),
                                                borderRadius: BorderRadius.circular(100)),
                                          ),
                                        ),

                                        Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Center(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    SizedBox(width: 0),
                                                    Text(
                                                      "Your Card",
                                                      style: textTheme.bodyText2
                                                          .merge(TextStyle(
                                                              color: Colors
                                                                  .white60)),
                                                    )
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      _con.currentUser.payMethod.cardName
                                                          .toUpperCase(),
                                                      style: textTheme.bodyText2
                                                          .merge(TextStyle(
                                                              color: Colors
                                                                  .white60)),
                                                    ),
                                                    SizedBox(width: 0),
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      "**** **** **** ${_con.currentUser.payMethod.cardNumber.substring(_con.currentUser.payMethod.cardNumber.toString().length-4, _con.currentUser.payMethod.cardNumber.toString().length)}",
                                                      style: textTheme.bodyText2
                                                          .merge(TextStyle(
                                                              color: Colors
                                                                  .white60)),
                                                    ),

                                                    CardUtils.getCardImage(CardUtils
                                                        .getCardTypeFrmNumber(
                                                        _con.currentUser.payMethod.cardNumber)),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10),
                                NeuButtonContainer(
                                  onPressed: () {
                                    Navigator.of(context).pushNamed("/AddPaymentMethod",
                                        arguments: RouteArgument(isAddingPaymentFromJob: false))
                                        .then((value){
                                      if(value != null) {
                                        if(value) {
                                          _con.getCurrentUser();
                                        }
                                      }
                                    });
                                  },
                                  borderRadius: 10,
                                  width: size.width,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      "CHANGE",
                                      style: textTheme.subtitle2
                                          .merge(TextStyle(fontSize: 12)),
                                    ),
                                  ),
                                )
                              ],
                            )
                          : Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 10, top: 10),
                              child: NeuButton(
                                width: 150,
                                height: 40,
                                text: "Add Card".toUpperCase(),
                                textStyle: textTheme.subtitle2,
                                borderRadius: 100,
                                onPressed: () {
                                  Navigator.of(context).pushNamed(
                                      "/AddPaymentMethod",
                                      arguments: RouteArgument(
                                          isAddingPaymentFromJob: false)).then((value) {
                                            _con.getCurrentUser();
                                  });
                                },
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );

  }
}
