import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              SizedBox(height: 20),
              Text("You can contact at", style: textTheme.headline3,),
              SizedBox(height: 20),
              ListTile(
                title: Text("Our Headquarter", style: textTheme.headline5,),
                subtitle: Text("Washington, US", style: textTheme.bodyText2,),
                leading: Icon(CupertinoIcons.location_solid, color: theme.accentColor,),
              ),

              Divider(),

              ListTile(
                title: Text("Email Us", style: textTheme.headline5,),
                subtitle: Text("support@nephcare.com", style: textTheme.bodyText2,),
                leading: Icon(CupertinoIcons.mail_solid, color: theme.accentColor,),
                onTap: () async {
                  final Uri _emailLaunchUri = Uri(
                      scheme: 'mailto',
                      path: 'smith@example.com',
                      queryParameters: {
                        'subject': 'Example Subject & Symbols are allowed!'
                      }
                  );
                  if( await canLaunch(_emailLaunchUri.toString())) {
                    await launch(_emailLaunchUri.toString());
                  }
                },
              )

            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Contact Support",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
            Navigator.of(context).pop();
          }
      ),
      actions: [],
    );
  }
}
