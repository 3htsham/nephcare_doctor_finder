import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/src/controllers/user_controller.dart';
import 'package:doctor_finder/src/elements/profile/address_profile_widget.dart';
import 'package:doctor_finder/src/elements/profile/email_profile_widget.dart';
import 'package:doctor_finder/src/elements/profile/my_jobs_profile_widget.dart';
import 'package:doctor_finder/src/elements/profile/phone_profile_widget.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class ProfileWidget extends StatefulWidget {
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfileWidget>
    with AutomaticKeepAliveClientMixin  {

  UserController _con;

  _ProfileWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.initLocationController();
    super.initState();
    _con.getCurrentUser();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text("Account", style: textTheme.headline5,),
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7),
              child: Row(
                children: [
                  NeuTextFieldContainer(
                    padding: EdgeInsets.all(7),
                    textField: NeuContainer(
                      borderRadius: 100,
                      width: 70,
                      height: 70,
                      child: Padding(
                        padding: EdgeInsets.all(3),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: _con.currentUser != null && _con.currentUser.image != null && _con.currentUser.image.length > 0
                          ? FadeInImage.assetNetwork(
                              placeholder: "assets/img/placeholders/profile_placeholder.png",
                              image: _con.currentUser.image,
                              width: 70,
                              height: 70,
                              fit: BoxFit.cover,
                          ) : Image.asset(
                              "assets/img/placeholders/profile_placeholder.png",
                              width: 70,
                              height: 70,
                              fit: BoxFit.cover,
                            ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(width: 100, child: Text(_con.currentUser != null ? _con.currentUser.name ?? "" : "", style: textTheme.headline6, maxLines: 1, overflow: TextOverflow.ellipsis,)),
                        Text(_con.currentUser != null 
                        ? _con.currentUser.type.toString() == GlobalVariables.hospitalType
                        ? "Hospital" 
                        : "" : "", style: textTheme.caption,)
                      ],
                    ),
                  ),
                  IconButton(
                    onPressed: (){
                      Navigator.of(context).pushNamed("/EditProfile", arguments: RouteArgument(currentUser: _con.currentUser))
                          .then((value) {
                            if(value != null) {
                              if(value) {
                                _con.getCurrentUser();
                              }
                            }
                      });
                    },
                    icon: Icon(CupertinoIcons.pencil, color: theme.accentColor,),
                  ),
                ],
              ),
            ),

            MyJobsProfileWidget(con: _con),

            
            EmailProfileWidget(con: _con),

            PhoneProfileWidget(con: _con),

            AddressProfileWidget(con: _con, onTap: (){
              Navigator.of(context).pushNamed("/EditMyLocation",
                  arguments: RouteArgument(currentUser: _con.currentUser)).then((value) {
                    if(value != null) {
                      if(value) {
                        _con.getCurrentUser();
                      }
                    }
              });
            },),
            
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: NeuContainer(
                width: size.width,
                borderRadius: 10,
                child: Column(
                  children: [

                    ListTile(
                      onTap: (){
                        Navigator.of(context).pushNamed("/Favorites", arguments: RouteArgument(currentUser: _con.currentUser));
                      },
                      title: Text("Favorites", style: textTheme.headline5,),
                      leading: GradientIcon(CupertinoIcons.heart_solid, size: 22, gradient: config.Colors().iconGradient(),),
                      trailing: Icon(CupertinoIcons.forward, size: 18,),
                    ),

                    ListTile(
                      onTap: (){
                        userRepo.logout().then((value) {
                          Navigator.of(context).pushNamedAndRemoveUntil("/Login", (Route<dynamic> route) => false);
                        });
                      },
                      title: Text("Logout", style: textTheme.headline5,),
                      leading: GradientIcon(Icons.power_settings_new, size: 22, gradient: config.Colors().iconGradient(),),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(height: 60,)

          ],
        ),
      ),
    );
  }
}
