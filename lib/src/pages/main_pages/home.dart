import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/home_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/elements/nurse/nurse_carousel_item_home.dart';
import 'package:doctor_finder/src/elements/star_rating.dart';
import 'package:doctor_finder/src/models/category.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container_btn_with_state.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:doctor_finder/src/neumorphic/nue_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:doctor_finder/config/app_config.dart' as config;
import 'package:flutter/cupertino.dart' as c;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends StateMVC<HomePage>
    with AutomaticKeepAliveClientMixin {

  HomeController _con;

  bool showCategoriesDialog = false;
  
  _HomePageState() : super(HomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getNearbyNurses();
    _con.getCategories();
    userRepo.getCurrentUser().then((user) {
      setState(() {
        userRepo.currentUser = user;
        _con.currentUser = user;
        _con.getMyTrackings();
      });
    });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // at = _rand.nextInt(_con.posts.length);
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return RefreshIndicator(
      onRefresh: () async {},
        child: Scaffold(
          body: Stack(
            children: [
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [


                    Padding(
                      padding: const EdgeInsets.only(top: 40, right: 10, left: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: NeuButtonContainer(
                              onPressed: (){
                                Navigator.of(context).pushNamed("/MyHireRequests", arguments: RouteArgument(currentUser: _con.currentUser));
                              },
                              width: 40,
                              height: 40,
                              borderRadius: 100,
                              child: Center(
                                child: GradientIcon(
                                  Icons.person_add_alt,
                                  size: 25.0,
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      config.Colors().mainColor(1),
                                      config.Colors().mainColorLight
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: Text(
                        // "Find your Nurse",
                        "What are you looking for?",
                        style: textTheme.headline1.merge(TextStyle(fontSize: 24)),
                      ),
                    ),
                    SizedBox(height: 15,),

                    // Padding(
                    //   padding: EdgeInsets.symmetric(horizontal: 25),
                    //   child: NeuTextFieldContainer(
                    //     padding: EdgeInsets.symmetric(horizontal: 5),
                    //     textField: TextFormField(
                    //       obscureText: true,
                    //       style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                    //       decoration: InputDecoration(
                    //         border: InputBorder.none,
                    //         hintText: "Search here...",
                    //         hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                    //         prefixIcon: Icon(c.CupertinoIcons.search, color: theme.focusColor,),
                    //         prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                    //       ),
                    //       onSaved: (val){
                    //         // email = val;
                    //       },
                    //     ),
                    //   ),
                    // ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          NeuBtnContainerWithState(
                            onPressed: (){
                              setState(() {
                                _con.showingNurses = !_con.showingNurses;
                              });
                              },
                            state: _con.showingNurses,
                            borderRadius: 10,
                            height: 50,
                            width: (size.width - 65) / 2,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.asset("assets/img/nurse.png", height: 35,),
                                Text("Nurse", style: textTheme.subtitle1,)
                              ],
                            ),
                          ),
                          NeuBtnContainerWithState(
                            onPressed: (){
                              setState(() {
                                _con.showingNurses = !_con.showingNurses;
                              });
                            },
                            state: !_con.showingNurses,
                            borderRadius: 10,
                            height: 50,
                            width: (size.width - 65) / 2,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.asset("assets/img/babysitter.png", height: 35,),
                                Text("BabySitter", style: textTheme.subtitle1,)
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: _con.isTrackings ? 15 : 0 ),

                    _con.isTrackings ? Center(
                      child: NeuButtonContainer(
                          onPressed: (){
                            Navigator.of(context).pushNamed("/TrackingsList", arguments: RouteArgument(currentUser: _con.currentUser));
                          },
                        borderRadius: 100,
                        width: MediaQuery.of(context).size.width-30,
                        height: 50,
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.watch_later_outlined, color: theme.accentColor, size: 16,),
                              SizedBox(width: 5),
                              Text("You've some ongoing trackings", style: textTheme.subtitle1,),
                            ],
                          ),
                        ),
                          ),
                    ) : SizedBox(height: 0),

                    SizedBox(height: 25,),
                    //TODO: Implement Instant Hire Here
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Near you", style: textTheme.headline6.merge(TextStyle(color: Colors.black54)),),
                          _con.isLoading
                              ? NeuButtonContainer(
                            onPressed: (){},
                            width: 40,
                            height: 40,
                            borderRadius: 100,
                            child: Center(
                              child: GradientIcon(
                                CupertinoIcons.refresh,
                                size: 25.0,
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    config.Colors().mainColor(1),
                                    config.Colors().mainColorLight
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                            ),
                          )
                              : NeuButtonContainer(
                            borderRadius: 100,
                            height: 40,
                            width: 120,
                            onPressed: (){
                              setState((){
                                this.showCategoriesDialog = true;
                              });
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(),
                                Text("Instant Hire", style: textTheme.bodyText2.merge(TextStyle(color: theme.accentColor)),),
                                ImageIcon(
                                  AssetImage('assets/img/nurse_icon.png',),
                                  color: theme.accentColor,
                                  size: 24,
                                ),
                                SizedBox(),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      height: 300,
                      child:
                      !_con.doneRequestingNearby
                          ?  CircularLoadingWidget(height: 100)
                          : _con.nurses.length > 0
                            ? _con.showingNurses
                          ? ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _con.nursesList.length,
                          itemBuilder: (context, index) {
                            return NurseCarouselItemHome(
                              nurse: _con.nursesList[index],
                              onTap: (){
                                Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.nursesList[index], getUserDetails: false));
                              },
                              onPhoneTap: (){
                                if(_con.nursesList[index].phone != null && _con.nursesList[index].phone.toString().length > 0) {
                                  ///Launch the phone
                                }
                              },
                            );
                          })
                          : ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: _con.babysittersList.length,
                                itemBuilder: (context, index) {
                                  return NurseCarouselItemHome(
                                    nurse: _con.babysittersList[index],
                                    onTap: (){
                                      Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.babysittersList[index], getUserDetails: false));
                                    },
                                    onPhoneTap: (){
                                      if(_con.babysittersList[index].phone != null && _con.babysittersList[index].phone.toString().length > 0) {
                                        ///Launch the phone
                                      }
                                    },
                                  );
                                })
                            : SizedBox(
                                height: 0,
                              )
                    ),

                    SizedBox(height: 10,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("More profiles", style: textTheme.headline3,),
                          IconButton(
                            onPressed: (){},
                            icon: Icon(c.CupertinoIcons.chevron_down),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: ListView(
                        scrollDirection: Axis.vertical,
                        primary: false,
                        shrinkWrap: true,
                        children: [
                          _con.nurses.isEmpty
                              ? CircularLoadingWidget(height: 100)
                              : ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: _con.nurses.length,
                            itemBuilder: (context, index) {
                              User nurse = _con.nurses[index];
                              return Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: InkWell(
                                  onTap: (){
                                    Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: _con.nurses[index], getUserDetails: false));
                                  },
                                  child: NeuContainer(
                                    height: 75,
                                    width: 200,
                                    borderRadius: 15,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(width: 10,),
                                        CircleAvatar(
                                          backgroundColor: Colors.lightBlueAccent.withOpacity(0.5),
                                          radius: 25,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: ClipRRect(
                                              borderRadius: BorderRadius.circular(100),
                                              child: nurse.image != null && nurse.image.length >0
                                                  ? CachedNetworkImage(
                                                imageUrl: nurse.image,
                                                width: 50,
                                                height: 50,
                                                fit: BoxFit.cover,
                                              )
                                                  : Image.asset("assets/img/placeholders/profile_placeholder.png",
                                                width: 50,
                                                height: 50,
                                                fit: BoxFit.cover,),
                                            )
                                          ),
                                        ),
                                        SizedBox(width: 15,),
                                        Expanded(
                                          flex: 1,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(nurse.name, style: textTheme.subtitle1,),
                                              SizedBox(height: 5,),
                                              Text(nurse.category.name, style: textTheme.caption,)
                                            ],
                                          ),
                                        ),
                                        NeuContainer(
                                          height: 150,
                                          borderRadius: 15,
                                          child: IconButton(
                                            onPressed: (){
                                              Navigator.of(context).pushNamed("/OpenChat", arguments: RouteArgument(currentUser: _con.currentUser, user: nurse));
                                            },
                                            icon: Icon(
                                              CupertinoIcons.chat_bubble,
                                              color: theme.accentColor,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 60,),


                  ],
                ),
              ),


              this.showCategoriesDialog
              ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: config.Colors().scaffoldColor(0.5),
                child: Center(
                  child: NeuContainer(
                    width: 300,
                    height: 350,
                    borderRadius: 10,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Column(
                        children: [
                          Text("Choose a Category", style: textTheme.subtitle1),
                          SizedBox(height: 5,),
                          Container(height: 0.3, width: 150, color: config.Colors().mainColor(1),),
                          SizedBox(height: 5,),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: NeuTextFieldContainer(
                              borderRadius: BorderRadius.circular(15),
                              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                              textField: Container(
                                padding: EdgeInsets.only(left: 10, right: 10),
                                child: ListView.builder(
                                    primary: false,
                                    shrinkWrap: true,
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    itemCount: _con.categories.length,
                                    itemBuilder: (context, index) {
                                      Category category = _con.categories[index];
                                      return InkWell(
                                        onTap: (){
                                          setState((){
                                            _con.selectedCategory = category;
                                          });
                                        },
                                        child: Container(
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(category.name, style: textTheme.bodyText1,),
                                              Spacer(),
                                              NeuBtnCheckBox(
                                                onPressed: (){
                                                  setState((){
                                                    _con.selectedCategory = category;
                                                  });
                                                },
                                                state: _con.selectedCategory != null && _con.selectedCategory.id==category.id,
                                                borderRadius: 100,
                                                width: 30,
                                                height: 30,
                                                child: Icon(CupertinoIcons.check_mark, color: _con.selectedCategory != null && _con.selectedCategory.id==category.id ? theme.accentColor : Colors.white,),
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    }
                                ),
                              ),
                            ),
                          ),

                          Spacer(),
                          Text("Create Request?", style: textTheme.subtitle2),
                          SizedBox(height: 5,),

                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              children: [

                                Spacer(),
                                NeuButtonContainer(
                                  onPressed: (){
                                    setState((){
                                      this.showCategoriesDialog = false;
                                    });
                                  },
                                  width: 80,
                                  height: 40,
                                  borderRadius: 10,
                                  child: Center(
                                    child: GradientIcon(CupertinoIcons.clear, size: 22, gradient: config.Colors().iconGradient(),),
                                  ),
                                ),

                                Spacer(),

                                NeuButtonContainer(
                                  onPressed: (){
                                    setState((){
                                      this.showCategoriesDialog = false;
                                    });
                                    _con.createHireRequestForNearbyNurses();
                                  },
                                  width: 80,
                                  height: 40,
                                  borderRadius: 10,
                                  child: Center(
                                    child: GradientIcon(CupertinoIcons.checkmark, size: 20, gradient: config.Colors().iconGradient(),),
                                  ),
                                ),

                                Spacer(),
                              ],
                            ),
                          )

                        ],
                      ),
                    ),
                  ),
                ),
              )
                  : IgnorePointer(
                child: SizedBox(),
              )

            ],
          ),
        )
    );
  }


  Widget _appbar(){
    return AppBar(
      elevation: 0,

      backgroundColor: Colors.transparent,
      automaticallyImplyLeading: false,
      actions: [

      ],
    );
  }

}











