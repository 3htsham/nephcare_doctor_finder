import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/favorites_controller.dart';
import 'package:doctor_finder/src/elements/CircularLoadingWidget.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class FavoritesWidget extends StatefulWidget {

  RouteArgument argument;
  FavoritesWidget({this.argument});

  @override
  _FavoritesWidgetState createState() => _FavoritesWidgetState();
}

class _FavoritesWidgetState extends StateMVC<FavoritesWidget> {

  FavoritesController _con;

  _FavoritesWidgetState() : super(FavoritesController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
    _con.getFavorites();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [
              _con.users.isEmpty
                  ? CircularLoadingWidget(height: 100,)
                  : ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: _con.users.length,
                itemBuilder: (context, index) {
                  User user = _con.users[index];

                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: InkWell(
                      onTap: (){
                        Navigator.of(context).pushNamed("/DoctorProfile",
                            arguments: RouteArgument(user: user, getUserDetails: true, currentUser: _con.currentUser));
                      },
                      child: Container(
                        child: Row(
                          children: [
                            ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: CachedNetworkImage(
                                  imageUrl: user.image,
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.cover,
                                )
                            ),
                            SizedBox(width: 10),
                            Expanded(child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(user.name, style: textTheme.subtitle1.merge(TextStyle(fontSize: 14)),),
                                SizedBox(height: 5),
                                Text(user.category.name.toUpperCase(), style: textTheme.caption,),
                              ],
                            )),

                          ],
                        ),
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text("Favorites",
        style: Theme.of(context).textTheme.headline4.merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      centerTitle: true,
      leading: IconButton(
          icon: Icon(CupertinoIcons.back, color: Theme.of(context).accentColor),
          onPressed: (){
              Navigator.of(context).pop();
          }
      ),
      actions: [],
    );
  }
}
