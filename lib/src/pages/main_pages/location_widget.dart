import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/controllers/location_controller.dart';
import 'package:doctor_finder/src/elements/star_rating.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class LocationWidget extends StatefulWidget {
  @override
  _LocationWidgetState createState() => _LocationWidgetState();
}

class _LocationWidgetState extends StateMVC<LocationWidget>
    with AutomaticKeepAliveClientMixin  {

  LocationController _con;

  _LocationWidgetState() : super(LocationController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.initLocationController();
    _con.getNearbyNurses();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _con.createMarker(context);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              onMapCreated: (GoogleMapController controller) {
                _con.isMapCreated = true;
                _con.controller = controller;
                _con.controller.setMapStyle(_con.mapStyle);
                _con.moveCamera();
              },
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                  target: LatLng(40.7237765, -74.017617), zoom: 13.0),

              // markers: markers,
              onTap: (pos) {
                print(pos);
                Marker m = Marker(
                    markerId: MarkerId('1'), icon: _con.customIcon, position: pos);
                setState(() {
                  _con.allMarkers.add(m);
                });
              },
              markers: Set.from(_con.allMarkers),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 200.0,
              margin: EdgeInsets.only(bottom: 30),
              width: MediaQuery.of(context).size.width,
              child: PageView.builder(
                controller: _con.pageController,
                itemCount: !_con.doneRequestingNearby ? 1 : _con.usersNearby.length,
                itemBuilder: (BuildContext context, int index) {
                  return !_con.doneRequestingNearby
                      // ? _restaurantLoading(index)
                  ? SizedBox(height: 0,)
                      : _nearbyList(index);
                },
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Container(
                  height: 75.0,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: theme.scaffoldBackgroundColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 30.0),
                    child: Center(
                      child: Text(
                        "Nearby Nurses",
                        style: textTheme.subtitle1,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                height: 40.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: <Color>[
                      theme.scaffoldBackgroundColor.withOpacity(0.1),
                      theme.scaffoldBackgroundColor.withOpacity(0.6),
                      theme.scaffoldBackgroundColor,
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _nearbyList(index) {
    User user = _con.usersNearby[index];
    var _id = _con.usersNearby[index].id;
    String _image = _con.usersNearby[index].image;
    String _title = _con.usersNearby[index].name;
    String _distance = double.parse(_con.usersNearby[index].distance.toString()).toStringAsFixed(2);
    var _category = _con.usersNearby[index].category != null ? _con.usersNearby[index].category.name : "";
    var _rating = 3.9;

    return AnimatedBuilder(
      animation: _con.pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_con.pageController.position.haveDimensions) {
          value = _con.pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 125.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: Padding(
        padding:
        const EdgeInsets.only(left: 0.0, right: 8.0, top: 5.0, bottom: 5.0),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed("/DoctorProfile", arguments: RouteArgument(user: user, getUserDetails: false));
          },
          child: NeuContainer(
            height: 140.0,
            width: 340.0,
            borderRadius: 15,
            child: Row(
              children: <Widget>[
                Material(
                  color: Colors.transparent,
                  child: Container(
                        height: 140.0,
                        width: 110.0,
                        decoration: BoxDecoration(
                            // color: Color(0xFF1E2026),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0)),
                            image: DecorationImage(
                                image: NetworkImage(_image),
                                fit: BoxFit.cover)
                        ),
                      ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: 150.0,
                          child: Text(
                            _title,
                            style: Theme.of(context).textTheme.subtitle1,
                            maxLines: 1,
                            overflow: TextOverflow.fade,
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              size: 14.0,
                              color: Colors.black54,
                            ),
                            Container(
                              width: 140.0,
                              child: Text(
                                'Distance $_distance km',
                                style: Theme.of(context).textTheme.bodyText2,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Container(
                          width: 140.0,
                          child: Text(
                            _category,
                            style: Theme.of(context).textTheme.headline5.merge(TextStyle(fontSize: 12, fontWeight: FontWeight.w400)),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                StarRating(rating: _rating, size: 14,),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
