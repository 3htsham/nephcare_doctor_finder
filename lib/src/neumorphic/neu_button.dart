import 'package:doctor_finder/src/neumorphic/concave_decoration.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class NeuButton extends StatefulWidget {

  final bool isChosen;
  final bool isPill;
  final VoidCallback onPressed;
  final String text;
  final Color textColor;
  final double textSize;
  final double borderRadius;
  final double width;
  final double height;
  TextStyle textStyle;

  NeuButton({
    Key key,
    @required this.text,
    this.textColor,
    this.textStyle,
    this.textSize,
    this.isPill = false,
    this.width = 100,
    this.height = 50,
    @required this.onPressed,
    this.isChosen = false,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  _NeuButtonState createState() => _NeuButtonState();
}

class _NeuButtonState extends State<NeuButton> {
  bool _isPressed = false;

  @override
  void didUpdateWidget(NeuButton oldWidget) {
    if (oldWidget.isChosen != widget.isChosen) {
      setState(() => _isPressed = widget.isChosen);
    }
    super.didUpdateWidget(oldWidget);
  }

  void _onPointerDown(PointerDownEvent event) {
    setState(() => _isPressed = true);
    Future.delayed(Duration(milliseconds: 10), (){
      widget.onPressed();
    });
  }

  void _onPointerUp(PointerUpEvent event) {
    setState(() => _isPressed = widget.isChosen);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final squareSideLength = width / 5;
    final buttonWidth = squareSideLength * (widget.isPill ? 2.2 : 1);
    final buttonSize = Size(buttonWidth, squareSideLength);

    final innerShadow = ConcaveDecoration(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(widget.borderRadius),
      ),
      colors: [config.Colors().shadowLight, config.Colors().shadowDark],
      depth: 5,
    );

    final outerShadow = BoxDecoration(
      // border: Border.all(color: neumorphicTheme.borderColor),
      borderRadius: BorderRadius.circular(widget.borderRadius),
      color: config.Colors().scaffoldColor(1),
      boxShadow: config.Colors().containerShadow(),
    );

    return Listener(
      onPointerDown: _onPointerDown,
        onPointerUp: _onPointerUp,
        child: AnimatedContainer(
          width: widget.width,
          height: widget.height,
          duration: Duration(milliseconds: 50),
          decoration: _isPressed ? innerShadow : outerShadow,
          child: Align(
            alignment: Alignment.center,
            child: Text(
              widget.text,
              style: widget.textStyle == null ? Theme.of(context).textTheme.subtitle2 : widget.textStyle,
            ),
          ),
        ),
    );
  }
}
