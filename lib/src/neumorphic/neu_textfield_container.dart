import 'package:doctor_finder/src/neumorphic/concave_decoration.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class NeuTextFieldContainer extends StatelessWidget {

  Widget textField;
  EdgeInsetsGeometry padding;
  BorderRadius borderRadius;

  NeuTextFieldContainer({this.textField,
    this.borderRadius, this.padding = const EdgeInsets.all(0.0)});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      decoration: ConcaveDecoration(
        shape: RoundedRectangleBorder(borderRadius: borderRadius ?? BorderRadius.circular(100)),
        colors: [config.Colors().shadowLight, config.Colors().shadowDark],
        depth: 6
      ),
      child: textField,
    );
  }
}
