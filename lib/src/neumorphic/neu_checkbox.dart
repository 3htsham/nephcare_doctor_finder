import 'package:doctor_finder/src/neumorphic/concave_decoration.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class NeuCheckBox extends StatefulWidget {

  final bool isChosen;
  final bool isPill;
  final VoidCallback onPressed;
  final double borderRadius;
  final double width;
  final double height;

  NeuCheckBox({
    Key key,
    this.isPill = false,
    this.width = 100,
    this.height = 50,
    @required this.onPressed,
    this.isChosen = false,
    this.borderRadius = 0,
  }) : super(key: key);

  @override
  _NeuCheckBoxState createState() => _NeuCheckBoxState();
}

class _NeuCheckBoxState extends State<NeuCheckBox> {
  bool _isPressed = false;

  @override
  void didUpdateWidget(NeuCheckBox oldWidget) {
    if (oldWidget.isChosen != widget.isChosen) {
      setState(() => _isPressed = widget.isChosen);
    }
    super.didUpdateWidget(oldWidget);
  }

  void _onPressed() {
    setState(() => _isPressed = !_isPressed);
    widget.onPressed();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final squareSideLength = width / 5;
    final buttonWidth = squareSideLength * (widget.isPill ? 2.2 : 1);
    final buttonSize = Size(buttonWidth, squareSideLength);

    final innerShadow = ConcaveDecoration(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(widget.borderRadius),
      ),
      colors: [config.Colors().mainColor(1), config.Colors().mainColorLight],
      depth: 5,
    );

    final outerShadow = BoxDecoration(
      // border: Border.all(color: neumorphicTheme.borderColor),
      borderRadius: BorderRadius.circular(widget.borderRadius),
      color: config.Colors().scaffoldColor(1),
      boxShadow: config.Colors().containerShadow(),
    );

    return InkWell(
      onTap: _onPressed,
      child: AnimatedContainer(
        width: widget.width,
        height: widget.height,
        duration: Duration(milliseconds: 50),
        decoration: _isPressed ? innerShadow : outerShadow,
        child: Align(
          alignment: Alignment.center,
          child: _isPressed
              ? GradientIcon(
                  Icons.check,
                  size: 20.0,
                  gradient: LinearGradient(
                    colors: <Color>[
                      config.Colors().mainColor(1),
                      config.Colors().mainColorLight
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                )
              : Icon(Icons.check, size: 20, color:  config.Colors().textColor(1),),
        ),
      ),
    );
  }
}
