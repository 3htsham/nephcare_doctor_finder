import 'package:doctor_finder/src/controllers/job_controller.dart';
import 'package:doctor_finder/src/neumorphic/neu_textfield_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;


class EditJobFormWidget extends StatelessWidget {
  JobController con;

  EditJobFormWidget({@required this.con});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Form(
        key: con.jobFormKey,
        child: Column(
          children: [

            SizedBox(height: 20),

            NeuTextFieldContainer(
              padding: EdgeInsets.symmetric(horizontal: 5),
              textField: TextFormField(
                enabled:  !con.isLoading,
                initialValue: con.job.title ?? "",
                validator: (val) => val.isEmpty ? "Title can't be empty":null,
                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Job Title",
                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                  prefixIcon: Icon(Icons.bolt, color: config.Colors().textColor(1),),
                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                ),
                onSaved: (val){
                  con.job.title = val;
                },
              ),
            ),

            SizedBox(height: 15),

            NeuTextFieldContainer(
              borderRadius: BorderRadius.circular(10),
              padding: EdgeInsets.symmetric(horizontal: 5),
              textField: TextFormField(
                enabled: !con.isLoading,
                initialValue: con.job.description ?? "",
                minLines: 7,
                maxLines: null,
                textInputAction: TextInputAction.newline,
                validator: (val) => val.isEmpty ? "Description can't be empty":null,
                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Job description",
                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                  prefixIcon: Icon(Icons.menu, color: config.Colors().textColor(1),),
                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                ),
                onSaved: (val){
                  con.job.description = val;
                },
              ),
            ),

            SizedBox(height: 10),

            NeuTextFieldContainer(
              padding: EdgeInsets.symmetric(horizontal: 5),
              textField: TextFormField(
                enabled: !con.isLoading,
                initialValue: con.job.hours ?? "",
                validator: (val) => val.isEmpty ? "Hours":null,
                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "No. of hours to work",
                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                  prefixIcon: Icon(CupertinoIcons.clock, color: config.Colors().textColor(1),),
                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                ),
                onSaved: (val){
                  con.job.hours = val;
                },
              ),
            ),

            SizedBox(height: 10),

            NeuTextFieldContainer(
              padding: EdgeInsets.symmetric(horizontal: 5),
              textField: TextFormField(
                enabled: !con.isLoading,
                initialValue: con.job.pay ?? "",
                validator: (val) => val.isEmpty ? "Pay can't be empty":null,
                style: textTheme.bodyText1.merge(TextStyle(color: config.Colors().textColor(1))),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "How much you'll pay for those hours?",
                  hintStyle: textTheme.bodyText1.merge(TextStyle(color: theme.hintColor)),
                  prefixIcon: Icon(Icons.attach_money, color: config.Colors().textColor(1),),
                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                ),
                onSaved: (val){
                  con.job.pay = val;
                },
              ),
            ),

          ],
        ),
      ),
    );
  }
}