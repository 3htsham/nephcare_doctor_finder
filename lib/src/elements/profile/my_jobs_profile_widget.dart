import 'package:doctor_finder/config/global_variables.dart';
import 'package:doctor_finder/src/controllers/user_controller.dart';
import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;


class MyJobsProfileWidget extends StatelessWidget {

  UserController con;

  MyJobsProfileWidget({this.con});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: NeuContainer(
          width: size.width,
          borderRadius: 10,
          child: Column(
            children: [

              con.currentUser != null &&
                    con.currentUser.type.toString() ==
                        GlobalVariables.hospitalType
                ? ListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed("/MyJobs",
                          arguments:
                              RouteArgument(currentUser: con.currentUser));
                    },
                    title: Text(
                      "My Jobs",
                      style: textTheme.headline5,
                    ),
                    leading: GradientIcon(
                      Icons.group_add,
                      size: 22,
                      gradient: config.Colors().iconGradient(),
                    ),
                    trailing: Icon(
                      CupertinoIcons.forward,
                      size: 18,
                    ),
                  )
                : SizedBox(height: 0),
            con.currentUser != null &&
                    con.currentUser.type.toString() ==
                        GlobalVariables.hospitalType
                ? ListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed("/AddNewJob");
                    },
                    title: Text(
                      "Add New Job",
                      style: textTheme.headline5,
                    ),
                    leading: GradientIcon(
                      CupertinoIcons.add_circled_solid,
                      size: 22,
                      gradient: config.Colors().iconGradient(),
                    ),
                    trailing: Icon(
                      CupertinoIcons.forward,
                      size: 18,
                    ),
                  )
                : SizedBox(height: 0),

              ListTile(
                onTap: () {
                  Navigator.of(context).pushNamed("/ViewOnGoingJobsList",
                      arguments: RouteArgument(currentUser: con.currentUser));
                },
                title: Text(
                  "OnGoing Jobs",
                  style: textTheme.headline5,
                ),
                leading: GradientIcon(
                  Icons.person_pin,
                  size: 22,
                  gradient: config.Colors().iconGradient(),
                ),
                trailing: Icon(
                  CupertinoIcons.forward,
                  size: 18,
                ),
              ),

            ListTile(
              onTap: () {
                Navigator.of(context).pushNamed("/MyWallet",
                    arguments:
                    RouteArgument(currentUser: con.currentUser));
              },
              title: Text(
                "My Wallet",
                style: textTheme.headline5,
              ),
              leading: GradientIcon(
                Icons.account_balance_wallet,
                size: 22,
                gradient: config.Colors().iconGradient(),
              ),
              trailing: Icon(
                CupertinoIcons.forward,
                size: 18,
              ),
            ),
          ],
          ),
        ),
      );
  }
}