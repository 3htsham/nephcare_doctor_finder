import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/neumorphic/gradient_icon.dart';
import 'package:doctor_finder/src/neumorphic/neu_button_container.dart';
import 'package:doctor_finder/src/neumorphic/neu_container.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class NurseCarouselItemHome extends StatelessWidget {
  User nurse;
  int maxSpecItem = 5;
  VoidCallback onTap;
  VoidCallback onPhoneTap;

  NurseCarouselItemHome({this.nurse, this.onTap, this.onPhoneTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Padding(
      padding: const EdgeInsets.only(left: 15, top: 10, right: 10, bottom: 15),
      child: InkWell(
        onTap: this.onTap,
        child: NeuContainer(
          width: 300,
          height: 280,
          borderRadius: 10,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: CachedNetworkImage(
                  imageUrl: nurse.image,
                  imageBuilder: (context, imageProvider) => Container(
                    height: 200,
                    width: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topRight: Radius.circular(10)),
                      image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                    ),
                  ),
                ),
                )
              ),
              Container(
                width: 300,
                height: 280,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      config.Colors().scaffoldColor(1),
                      config.Colors().scaffoldColor(1),
                      config.Colors().scaffoldColor(0.5),
                      config.Colors().scaffoldColor(0)
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    stops: [0.4, 0.5, 0.6, 0.7]
                  )
                ),
              ),
              Container(
                width: 300,
                height: 280,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          config.Colors().scaffoldColor(1),
                          config.Colors().scaffoldColor(1),
                          config.Colors().scaffoldColor(0.5),
                          config.Colors().scaffoldColor(0)
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        stops: [0.2, 0.3, 0.4, 0.7]
                    )
                ),
              ),


              Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, bottom: 10, left: 10, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          nurse.name,
                          style: textTheme.headline3,
                        ),
                        SizedBox(height: 5,),
                        //StarRating(rating: 4.7, size: 14,),
                        SizedBox(height: 5,),
                        Text(
                          nurse.category.name,
                          style: textTheme.bodyText2,),
                        // SizedBox(height: 5,),
                        // Text(
                        //   "Midtown, West California",
                        //   style: textTheme.caption,),

                        // SizedBox(height: 10,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [

                            nurse.qualifications != null && nurse.qualifications.length > 0 ?
                            ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: nurse.qualifications.length < maxSpecItem ? nurse.qualifications.length : maxSpecItem,
                                itemBuilder: (context, index) {
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    GradientIcon(
                                      Icons.arrow_forward,
                                      size: 20.0,
                                      gradient: LinearGradient(
                                        colors: <Color>[
                                          config.Colors().mainColor(1),
                                          config.Colors().mainColorLight
                                        ],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                      ),
                                    ),
                                    Text(
                                      nurse.qualifications[index].degree,
                                      style: textTheme.bodyText1,),
                                  ],
                                );
                                }
                            )
                            : SizedBox(height: 0,),

                            nurse.qualifications.length < maxSpecItem && nurse.specifications != null && nurse.specifications.length > 0
                                ? ListView.builder(
                                shrinkWrap: true,
                                primary: false,
                                padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                                itemCount: (maxSpecItem-nurse.qualifications.length) >= nurse.specifications.length
                                    ? nurse.specifications.length
                                    : (maxSpecItem-nurse.qualifications.length),
                                itemBuilder: (context, index) {
                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      GradientIcon(
                                        Icons.arrow_forward,
                                        size: 20.0,
                                        gradient: LinearGradient(
                                          colors: <Color>[
                                            config.Colors().mainColor(1),
                                            config.Colors().mainColorLight
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                        ),
                                      ),
                                      Text(
                                        nurse.specifications[index].specName,
                                        style: textTheme.bodyText1,),

                                    ],
                                  );
                                }
                            )
                                : SizedBox(height: 0,),

                            SizedBox(height: 5,),


                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: NeuButtonContainer(
                                    onPressed: this.onPhoneTap,
                                    width: 30,
                                    height: 30,
                                    borderRadius: 100,
                                    child: Center(
                                      child: GradientIcon(
                                        Icons.phone_in_talk,
                                        size: 20.0,
                                        gradient: LinearGradient(
                                          colors: <Color>[
                                            Colors.red,
                                            Colors.redAccent
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Text(nurse.phone ?? "", style: textTheme.subtitle1,),

                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
