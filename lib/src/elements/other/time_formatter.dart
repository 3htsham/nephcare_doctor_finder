import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TimeTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {

    //this fixes backspace bug
    if (oldValue.text.length >= newValue.text.length) {
      return newValue;
    } else if (oldValue.text.length <5) {
      var timeText = _addSeparators(newValue.text, ':');


      if(timeText.substring(timeText.lastIndexOf(":")+1, timeText.length).length == 2) {
        try {
          DateFormat format = DateFormat("HH:mm");
          DateTime tempDate = format.parse(timeText);
          timeText = format.format(tempDate);
        } catch (e) {
          print(e);
        }
      }

      print(timeText);
      var newVal = newValue.copyWith(
          text: timeText, selection: updateCursorPosition(timeText));
      return newVal;
    }
  }

  String _addSeparators(String value, String separator) {
    value = value.replaceAll(':', '');
    var newString = '';
    for (int i = 0; i < value.length; i++) {
      newString += value[i];
      if (i == 1) {
        newString += separator;
      }
      // if (i == 3) {
      //   newString += separator;
      // }
    }
    return newString;
  }

  TextSelection updateCursorPosition(String text) {
    return TextSelection.fromPosition(TextPosition(offset: text.length));
  }
}