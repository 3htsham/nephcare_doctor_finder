import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {

    //this fixes backspace bug
    if (oldValue.text.length >= newValue.text.length) {
      return newValue;
    } else if (oldValue.text.length <10) {
      var dateText = _addSeparators(newValue.text, '/');


      if(dateText.substring(dateText.lastIndexOf("/")+1, dateText.length).length == 4) {
        try {
          DateFormat format = DateFormat("dd/MM/yyyy");
          DateTime tempDate = format.parse(dateText);
          dateText = format.format(tempDate);
        } catch (e) {
          print(e);
        }
      }

      print(dateText);
      var newVal = newValue.copyWith(
          text: dateText, selection: updateCursorPosition(dateText));
      return newVal;
    }
  }

  String _addSeparators(String value, String separator) {
    value = value.replaceAll('/', '');
    var newString = '';
    for (int i = 0; i < value.length; i++) {
      newString += value[i];
      if (i == 1) {
        newString += separator;
      }
      if (i == 3) {
        newString += separator;
      }
    }
    return newString;
  }

  TextSelection updateCursorPosition(String text) {
    return TextSelection.fromPosition(TextPosition(offset: text.length));
  }
}