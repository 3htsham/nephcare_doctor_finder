import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class CardMonthInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var newText = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = new StringBuffer();
    for (int i = 0; i < newText.length; i++) {
      buffer.write(newText[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 2 == 0 && nonZeroIndex != newText.length) {
        buffer.write('/');
      }
    }

    var string = buffer.toString();

    if(string.substring(string.lastIndexOf("/")+1, string.length).length == 2) {
      try {
        DateFormat format = DateFormat("dd/MM/yy");
        var str = "01/" + string;
        DateTime tempDate = format.parse(str);
        str = format.format(tempDate);
        DateFormat frmt = DateFormat("MM/yy");
        print(str);
        string = frmt.format(tempDate);
      } catch (e) {
        print(e);
      }
    }

    return newValue.copyWith(
        text: string,
        selection: new TextSelection.collapsed(offset: string.length));
  }
}