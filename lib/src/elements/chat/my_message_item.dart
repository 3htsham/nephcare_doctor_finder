import 'package:flutter/material.dart';
import 'package:doctor_finder/src/elements/chat/chat_profile_icon.dart';
import 'package:flutter/cupertino.dart' as c;
import 'package:doctor_finder/src/models/message.dart';
import 'package:doctor_finder/config/app_config.dart' as config;

class MyMessageWidgetItem extends StatelessWidget {


  var link;
  Message message;

  MyMessageWidgetItem({this.link, this.message});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var constraints = BoxConstraints(maxWidth: (size.width / 2) + 15);
     var boxDecoration = BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(0),
                    bottomLeft: Radius.circular(20)),);

    var textualPadding = const EdgeInsets.symmetric(vertical: 10.0, horizontal: 12);

    return Row(
      textDirection: c.TextDirection.rtl,
      children: [
        ChatProfileIcon(
          link: this.link,
        ),

        SizedBox(
          width: 10,
        ),

        Container(
          constraints: constraints,
          decoration: boxDecoration.copyWith(
            color: config.Colors().scaffoldColor(1),
            boxShadow: config.Colors().containerShadow(),),
          child: Padding(
            padding: textualPadding,
            child: Text(
              message.txt ?? "",
              style: textTheme.bodyText2,
            ),
          ),
        ),
      ],
    );
  }
}