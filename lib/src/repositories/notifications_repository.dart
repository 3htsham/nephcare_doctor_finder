import 'dart:convert';

import 'package:doctor_finder/config/global_variables.dart';
import 'package:http/http.dart' as http;


Future<void> sendNotification(String notiBody, String notiTitle, String targetFCMToken) async {
  final String _url = 'https://fcm.googleapis.com/fcm/send';
  Map<String, String> _headers = Map<String, String>();
  _headers['Content-Type'] = 'application/json';
  _headers['Authorization'] = 'key=${GlobalVariables.FCM_SERVER_TOKEN}';

  Map<String, dynamic> _body = Map<String, dynamic>();
  _body['notification'] = {
    'body': notiBody,
    'title': notiTitle,
    'sound': 'default'
  };
  _body['priority'] = 'high';
  _body['data'] = {
    'click_action': 'FLUTTER_NOTIFICATION_CLICK',
    'id': '1',
    'status': 'done'
  };
  _body['to'] = targetFCMToken;

  await http.post(_url, headers: _headers, body: jsonEncode(_body),);

}