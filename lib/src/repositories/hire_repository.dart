import 'dart:convert';
import 'dart:io';
import 'package:doctor_finder/src/helpers/helper.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:global_configuration/global_configuration.dart';

Future<String> sendHireRequest(String nurseId, var jobId, String description, String hirePay) async {
  var hireId;
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}hire/request';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['nurse_id'] = nurseId;
  if(jobId != null) {
    bodyMap['job_id'] = jobId;
  }
  bodyMap['description'] = description;
  bodyMap['amount'] = hirePay;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      var body = response.body;
      hireId = json.decode(response.body)['hireRequest']['id'];
    }
  }
  return hireId.toString();

}

Future<bool> withdrawHireRequest(var hireRequestId) async {
  bool isWithdraw = false;
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}reject/request';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['hire_request_id'] = hireRequestId;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      var body = response.body;
      isWithdraw = true;
    }
  }
  return isWithdraw;

}

///This is Hire ID (will get that from when a nurse accepts a hire request),
///not HireRequestId

Future<bool> withdrawJobHire(var hireId) async {
  bool isWithdraw = false;
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}hire/reject';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['hire_id'] = hireId;
  // bodyMap['user_id'] = userId;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      var body = response.body;
      isWithdraw = true;
    }
  }
  return isWithdraw;

}


Future<String> sendInstantHireRequest() async {
  // var hireId;
  // User _currentUser = await userRepo.getCurrentUser();
  // final String url = '${GlobalConfiguration().getString('api_base_url')}hire/request';
  // final client = new http.Client();
  // Map<String, String> bodyMap = Map<String, String>();
  // bodyMap['api_token'] = _currentUser.apiToken;
  // bodyMap['nurse_id'] = nurseId;
  // if(jobId != null) {
  //   bodyMap['job_id'] = jobId;
  // }
  // bodyMap['description'] = description;
  // bodyMap['amount'] = hirePay;
  //
  // final response = await client.post(
  //   url,
  //   headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  //   body: json.encode(bodyMap),
  // );
  // if(response.statusCode == 200) {
  //   if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
  //     var body = response.body;
  //     hireId = json.decode(response.body)['hireRequest']['id'];
  //   }
  // }
  // return hireId.toString();

}

///Will be called when a Job is finished
Future<bool> completeHire(var hireRequestId) async {
  bool isDone = false;
  User _currentUser = await userRepo.getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}hire/complete';
  final client = new http.Client();
  Map<String, String> bodyMap = Map<String, String>();
  bodyMap['api_token'] = _currentUser.apiToken;
  bodyMap['hire_id'] = hireRequestId;

  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      var body = response.body;
      isDone = true;
    }
  }
  return isDone;
}
