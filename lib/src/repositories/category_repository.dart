import 'dart:convert';
import 'package:doctor_finder/src/helpers/helper.dart';
import 'package:doctor_finder/src/models/category.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

Future<Stream<Category>> getCategories() async {
  final String url = '${GlobalConfiguration().getString('api_base_url')}all/categories';
  final client = new http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getCategoriesData(data))
      .expand((data) => (data as List))
      .map((data) =>
      Category.fromJson(data));
}