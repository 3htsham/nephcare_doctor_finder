import 'dart:convert';
import 'dart:io';
import 'package:doctor_finder/src/helpers/helper.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:doctor_finder/src/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_parser/http_parser.dart' as parser;

User currentUser = User();

Future<User> login(User user) async {
  final String url = '${GlobalConfiguration().getString('api_base_url')}user/login';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(user.toJson()),
  );
  if (response.statusCode == 200) {
    print(response.body);
    setCurrentUser(response.body);
    currentUser = User.fromJson(json.decode(response.body)['user']);
  }
  return currentUser;
}

Future<User> register(User user) async {
  final String url = '${GlobalConfiguration().getString('api_base_url')}user/register';
  final client = new http.Client();
  var reqBody = user.toJson();
  reqBody['genre'] = "1";
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(reqBody),
  );
  if (response.statusCode == 200) {
    var body = json.decode(response.body);
    print(response.body);
    if(json.decode(response.body)['user'] != null) {
      setCurrentUser(response.body);
      currentUser = User.fromJson(json.decode(response.body)['user']);
    }
  }
  return currentUser;
}

Future<User> getUserProfile(var _userId) async {
  User _user;
  var _user_id = '?user_id=$_userId';
  final String url = '${GlobalConfiguration().getString('api_base_url')}get/profile$_user_id';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  if (response.statusCode == 200) {
    var body = json.decode(response.body)['user'];
    User _newUser = User.fromJson(json.decode(response.body)['user']);
    _user = _newUser;
  }
  return _user;
}

Future<User> updateUser(User _user) async {
  User _currentUser = await getCurrentUser();
  User _updatedUser;
  final String url = '${GlobalConfiguration().getString('api_base_url')}user/update';
  final client = new http.Client();
  _user.apiToken = _currentUser.apiToken;
  var bodyMap = _user.toMap();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  
  if (response.statusCode == 200) {
    User _newUser = User.fromJson(json.decode(response.body)['user']);
    User _user = await getCurrentUser();
    _newUser.apiToken = _user.apiToken;
    Map userMap = Map();
    userMap['user'] = _newUser.toSaveJson();
    setCurrentUser(json.encode(userMap));
    _updatedUser = _user;
  } else {
    print(response.body);
  }
  return _updatedUser;
}

Future<Stream<User>> updateUserProfile(User _user) async {
  User _currentUser = await getCurrentUser();
  final String url = '${GlobalConfiguration().getString('api_base_url')}user/update';
  final client = new http.Client();
  _user.apiToken = _currentUser.apiToken;

  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));
  
  request.fields.addAll(_user.toMap());
  
  if(_user.isNewPhoto) {
    var file = await http.MultipartFile.fromPath("image", _user.newPhotoIdentifier, filename: _user.newPhotoName, contentType: parser.MediaType.parse("application/octet-stream"));
    request.files.add(file);
  }
  
  var response = await request.send();
  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
        print(data);
        var userData = Helper.getUserData(data);
        User _newUser = User.fromJson(userData);
        User _user = _currentUser;
        _newUser.apiToken = _user.apiToken;
        Map userMap = Map();
        userMap['user'] = _newUser.toSaveJson();
        setCurrentUser(json.encode(userMap));
        currentUser = _user;
        return currentUser;
      });
}


Future<User> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
//  prefs.clear();
  if (prefs.containsKey('current_user')) {
    currentUser = User.fromJson(json.decode(await prefs.get('current_user')));
  }
  return currentUser;
}

void setCurrentUser(jsonString) async {
  if (json.decode(jsonString)['user'] != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('current_user', json.encode(json.decode(jsonString)['user']));
  }
}


Future<void> logout() async {
  currentUser = new User();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('current_user');
}

Future<User> verifyEmail(String _userId, String _verificationCode) async {
  User _user;
  var _user_id = '?user_id=$_userId';
  var _code = '&code=$_verificationCode';
  final String url = '${GlobalConfiguration().getString('api_base_url')}user/verify$_user_id$_code';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  if (response.statusCode == 200) {
    if(json.decode(response.body)['user'] != null) {
      var body = json.decode(response.body)['user'];
      setCurrentUser(json.encode(body));
      User _newUser = User.fromJson(json.decode(response.body)['user']);
      _user = _newUser;
    }
  }
  return _user;
}