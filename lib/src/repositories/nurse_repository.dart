import 'dart:convert';

import 'package:doctor_finder/src/helpers/helper.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

Future<Stream<User>> getNearbyNurses() async {
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}nearby/nurse$_apiToken';
  final client = new http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getNursesData(data))
      .expand((element) =>
  element)
      .map((data) {
        var d = data;
        return User.fromJson(data);}
      );
}

Future<bool> addToFavorite(var userId) async {
  bool isAdded = true;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  var _userId = '&nurse_id=$userId';
  final String url = '${GlobalConfiguration().getString('api_base_url')}add/favorite$_apiToken$_userId';
  final client = http.Client();
  final response = await client.post(url);
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      isAdded = true;
    }
  }
  return isAdded;
}

Future<User> getNurseProfile(var userId) async {
  User profile;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  var _userId = '&user_id=$userId';
  final String url = '${GlobalConfiguration().getString('api_base_url')}get/profile$_apiToken$_userId';
  final client = http.Client();
  final response = await client.post(url);
  if(response.statusCode == 200) {
    print(response.body);
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      profile = User.fromJson(json.decode(response.body)['user']);
    }
  }
  return profile;
}


Future<Stream<User>> getFavorites() async {
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}get/favorites$_apiToken';
  final client = new http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getFavoritesData(data))
      .expand((element) =>
  element)
      .map((data) {
    var d = data;
    return User.fromJson(data);}
  );
}