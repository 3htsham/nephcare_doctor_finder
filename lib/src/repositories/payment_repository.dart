import 'dart:convert';
import 'dart:io';
import 'package:doctor_finder/src/helpers/helper.dart';
import 'package:doctor_finder/src/models/payment_method.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:doctor_finder/src/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_parser/http_parser.dart' as parser;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;


Future<User> updatePaymentMethod(PaymentMethod payMethod) async {
  User user;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}hospital/card/credential$_apiToken';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(payMethod.toJson()),
  );
  if (response.statusCode == 200) {
    print(response.body);
    user = User.fromJson(json.decode(response.body)['hospital']);
    User newUser = await userRepo.getUserProfile(user.id.toString());
    Map userMap = Map();
    newUser.apiToken = _currentUser.apiToken;
    userMap['user'] = newUser.toSaveJson();
    userRepo.setCurrentUser(json.encode(userMap));
  }
  return user;
}

Future<bool> enterBalanceWallet(var amount) async {
  bool added = false;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  var _amount = '&amount=${amount}';
  Map<String, String> map = Map<String, String>();
  map["amount"] = amount;
  map["api_token"] = _currentUser.apiToken;
  final String url = '${GlobalConfiguration().getString('api_base_url')}enter/balance$_apiToken$_amount';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(map),
  );
  if (response.statusCode == 200) {
    print(response.body);
    if(json.decode(response.body)["error"] != null &&
        (json.decode(response.body)["error"] == false || json.decode(response.body)["error"] == "false")) {
      added = true;
      User newUser = await userRepo.getUserProfile(_currentUser.id.toString());
      Map userMap = Map();
      newUser.apiToken = _currentUser.apiToken;
      userMap['user'] = newUser.toSaveJson();
      userRepo.setCurrentUser(json.encode(userMap));
    }
  }
  return added;
}