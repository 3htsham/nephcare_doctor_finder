import 'dart:convert';
import 'dart:io';
import 'package:doctor_finder/src/helpers/helper.dart';
import 'package:doctor_finder/src/models/job.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:doctor_finder/src/repositories/user_repository.dart' as userRepo;
import 'package:global_configuration/global_configuration.dart';

Future<Job> addNewJob(Job _job) async {
  Job job;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  _job.status = 1;
  var body = _job.toJson();
  final String url = '${GlobalConfiguration().getString('api_base_url')}job/store$_apiToken';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(body),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    var data = Helper.getJobData(json.decode(body));
    job = Job.fromJson(data);
  } else {
    print(response.body);
  }
  return job;
}

Future<Stream<Job>> getMyJobs() async {
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}jobs$_apiToken';
  
  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getJobsData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
      return Job.fromJson(data);});
}


Future<Job> updateJob(Job _job) async {
  Job job;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  _job.status = 1;
  final String url = '${GlobalConfiguration().getString('api_base_url')}job/update$_apiToken';
  final client = new http.Client();
  var bodyMap  = _job.toJson();
  bodyMap['job_id'] = _job.id;
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(bodyMap),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    var data = Helper.getJobData(json.decode(body));
    job = Job.fromJson(data);
  } else {
    print(response.body);
  }
  return job;
}

Future<bool> deleteJob(Job _job) async {
  bool jobDeleted = false;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  var _jobId = '&job_id=${_job.id}';
  final String url = '${GlobalConfiguration().getString('api_base_url')}job/destroy$_apiToken$_jobId';
  final client = http.Client();
  final response = await client.post(url);
  if(response.statusCode == 200) {
    if(json.decode(response.body)['error'] == false || json.decode(response.body)['error'] == "false") {
      jobDeleted = true;
    }
  }
  return jobDeleted;
}

Future<Job> getJobDetails(var jobId) async {
  Job job;
  User _currentUser = await userRepo.getCurrentUser();
  var _apiToken = '?api_token=${_currentUser.apiToken}';
  var _jobId = '&job_id=$jobId';
  final String url = '${GlobalConfiguration().getString('api_base_url')}job/details$_apiToken$_jobId';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    var data = Helper.getJobData(json.decode(body));
    job = Job.fromJson(data);
  } else {
    print(response.body);
  }
  return job;
}