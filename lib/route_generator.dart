import 'package:doctor_finder/src/models/route_argument.dart';
import 'package:doctor_finder/src/pages/chat/single_chat_widget.dart';
import 'package:doctor_finder/src/pages/hire/hire_now.dart';
import 'package:doctor_finder/src/pages/hire/hire_proposal_details.dart';
import 'package:doctor_finder/src/pages/hire/my_hire_proposals.dart';
import 'package:doctor_finder/src/pages/hire/my_job_selection.dart';
import 'package:doctor_finder/src/pages/hire/ongoing_job_details.dart';
import 'package:doctor_finder/src/pages/hire/view_ongoing_jobs_list.dart';
import 'package:doctor_finder/src/pages/job/add_new_job.dart';
import 'package:doctor_finder/src/pages/job/edit_job.dart';
import 'package:doctor_finder/src/pages/job/job_details.dart';
import 'package:doctor_finder/src/pages/job/my_jobs.dart';
import 'package:doctor_finder/src/pages/login/choose_current_loacation.dart';
import 'package:doctor_finder/src/pages/login/email_verification.dart';
import 'package:doctor_finder/src/pages/login/login.dart';
import 'package:doctor_finder/src/pages/main_pages/favorites.dart';
import 'package:doctor_finder/src/pages/other/about.dart';
import 'package:doctor_finder/src/pages/other/contact.dart';
import 'package:doctor_finder/src/pages/pages.dart';
import 'package:doctor_finder/src/pages/login/signup.dart';
import 'package:doctor_finder/src/pages/payment/add_payment_details.dart';
import 'package:doctor_finder/src/pages/payment/enter_balance.dart';
import 'package:doctor_finder/src/pages/payment/my_wallet.dart';
import 'package:doctor_finder/src/pages/profile/doctor_profile.dart';
import 'package:doctor_finder/src/pages/profile/edit_profile.dart';
import 'package:doctor_finder/src/pages/profile/update_location.dart';
import 'package:doctor_finder/src/pages/tracking/track_location.dart';
import 'package:doctor_finder/src/pages/tracking/trackings_list.dart';
import 'package:flutter/material.dart';
import 'package:doctor_finder/src/pages/login/splash_screen.dart';

class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch(settings.name)
    {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/Login':
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => Login(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Signup":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SignUp(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/UpdateCurrentLocation":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ChooseCurrentLocation(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Pages":
        return MaterialPageRoute(builder: (_) => PagesWidget(currentTab: args));
      case "/DoctorProfile":
        return MaterialPageRoute(builder: (_) => DoctorProfile(argument: args as RouteArgument,));
      case "/AddNewJob":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => AddNewJobWidget(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/MyJobs":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyJobsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/EditJob":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditJobWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/EditMyLocation":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => UpdateCurrentLocation(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/EditProfile":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditProfile(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/OpenChat":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SingleChatWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/JobDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => JobDetialsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/HireNurse":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => HireNowWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/JobSelection":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyJobsSelectionWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/Favorites":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => FavoritesWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/MyHireRequests":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyHireProposalsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/HireRequestDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => HireProposalDetails(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/TrackingsList":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => TrackingsListWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/TrackLocation":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => TrackLocationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/About":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => AboutUsWidget(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/Contact":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ContactUs(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/VerifyEmail":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EmailVerificationWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/MyWallet":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyWalletWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/AddPaymentMethod":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => AddPaymentMethodWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/AddWalletBalance":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EnterBalanceWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/ViewOnGoingJobsList":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => OngoingJobsListsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/ViewOnGoingJobDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => OngoingJobDetailsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
    }
  }

}