import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_finder/src/models/user.dart';
import 'package:flutter/material.dart';

class MyFirestore {

  static String chatsCollection = "chats";
  static String chatRoom = "chatroom";

    static String instantRequestCollection = "instantHireRequests";
    static String requestCollection = "hireRequests";
    static String trackingCCollection = "tracking";
    static String jobTrackingCollection = "JobsTracking";
    ///===================================== LOCATION STUFF =========================================================

    /*
     Map will be like
     id = requestId
     description = "Job description here"
     job = {id: 1, title: This is job title, description: This is job description, hours: 0.0} //Optional
     sender = { id: 1, name: Ehtsham, image: http://www.imagelink.com, type: user/hospital, location: GeoPoint() }
     nurse = { id: 1, name: Nurse, image: http://www.imagelink.com, category: categoryName }
     users = [ 1 (senderId) , 2 (nurseId)  ]
     status: "pending"/"accepted"/"rejected"
     scheduled: true/false
     schedule: DateTime in MilliSeconds
     pay: 0.00
    */

    static createHireRequest(Map<String, dynamic> map, String requestId) async {
      map['time'] = DateTime.now().millisecondsSinceEpoch;
      await FirebaseFirestore.instance
          .collection(requestCollection)
          .doc(requestId)
          .set(map);
    }

    static deleteHireRequest(String requestId) async {
      await FirebaseFirestore.instance
          .collection(requestCollection)
          .doc(requestId)
          .delete();
    }

    static acceptHireRequest(String requestId) async {
      await FirebaseFirestore.instance
          .collection(requestCollection)
          .doc(requestId)
          .update({'status': 'accepted'});
    }
    static rejectHireRequest(String requestId) async {
      await FirebaseFirestore.instance
          .collection(requestCollection)
          .doc(requestId)
          .update({'status': 'rejected'});
    }

  static Stream<QuerySnapshot> getHireRequests(String userId) {
    return FirebaseFirestore.instance
        .collection(requestCollection)
        .where("users", arrayContains: userId.toString())
        .orderBy('time')
        .snapshots();
  }


  static createInstantHireRequest(Map<String, dynamic> map, {String requestId}) async {
    map['time'] = DateTime.now().millisecondsSinceEpoch;
    await FirebaseFirestore.instance
        .collection(instantRequestCollection)
        .doc(requestId)
        // .doc()
        .set(map);
  }
  static deleteInstantHireRequest(String requestId) async {
    await FirebaseFirestore.instance
        .collection(instantRequestCollection)
        .doc(requestId)
        .delete();
  }

  static acceptInstantHireRequest(String requestId) async {
    await FirebaseFirestore.instance
        .collection(instantRequestCollection)
        .doc(requestId)
        .update({'status': 'accepted'});
  }
  static rejectInstantHireRequest(String requestId) async {
    await FirebaseFirestore.instance
        .collection(instantRequestCollection)
        .doc(requestId)
        .update({'status': 'rejected'});
  }

  static Stream<QuerySnapshot> getInstantHireRequests(String userId) {
    return FirebaseFirestore.instance
        .collection(instantRequestCollection)
        .where("status", isEqualTo: 'pending')
        .orderBy('time')
        .snapshots();
  }


  ///If hireRequest Accepted
    /*
      Map will be like
      id = requestId
      users = [ 1 (senderId), 2 (nurseId)]
      nursepro = { id: 1, name: nurseName, img: http://www.imagelink.com, categoryName: SomeCategory}
      senderpro = { id: 1, name: userOrHospitalName, img: http://www.imagelink.com, type: user/Hospital}
      >>>For GEoPoints (type will be GeoPoint)
      nurse = [ lat, lng]
      sender = [ lat, lng ]
     description = "Job description here"
     job = {id: 1, title: This is job title, description: This is job description, hours: 0.0} //Optional
     pay: hirePay
     hireId: Id
  */
    static createTrackingRoom(Map<String, dynamic> map, String requestID) async {
      await FirebaseFirestore.instance
          .collection(trackingCCollection)
          .doc(requestID)
          .set(map);
    }

    static updateTrackingLocation(String trackingDocId, GeoPoint geoPoint) async {
      Map<String, dynamic> map = Map<String, dynamic>();
      map['sender'] = geoPoint; // 'nurse' key for nurse
      await FirebaseFirestore.instance
          .collection(trackingCCollection)
          .doc(trackingDocId)
          .update(map);
    }

    static finishTracking(String trackingDocId) async {
      await FirebaseFirestore.instance
          .collection(trackingCCollection)
          .doc(trackingDocId)
          .delete();
    }

  static Stream<QuerySnapshot> getMyTrackingRooms(String userId) {
    return FirebaseFirestore.instance
        .collection(trackingCCollection)
        .where("users", arrayContains: userId.toString())
        .snapshots();
  }

  static Stream<DocumentSnapshot> getTrackingRoom(String trackingId) {
    return FirebaseFirestore.instance
        .collection(trackingCCollection)
        .doc(trackingId)
        .snapshots();
  }


  ///When after Tracking, A Job Started
  /*
      Map will be like
      id = requestId
      users = [ 1 (senderId), 2 (nurseId)]
      nursepro = { id: 1, name: nurseName, img: http://www.imagelink.com, categoryName: SomeCategory}
      senderpro = { id: 1, name: userOrHospitalName, img: http://www.imagelink.com, type: user/Hospital}
     description = "Job description here"
     job = {id: 1, title: This is job title, description: This is job description, hours: 0.0} //Optional
     pay: hirePay
      status: inProgress/Done
     time: 0123456 ///When Started
     hireId: Id
  */

  static createJobTrackingRoom(Map<String, dynamic> map, String requestID) async {
    map['time'] = DateTime.now().millisecondsSinceEpoch;
    map['status'] = "inProgress";
    await FirebaseFirestore.instance
        .collection(jobTrackingCollection)
        .doc(requestID)
        .set(map);
  }

  static updateJobTrackingStatus(String trackingDocId) async {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['status'] = "Done";
    await FirebaseFirestore.instance
        .collection(jobTrackingCollection)
        .doc(trackingDocId)
        .update(map);
  }

  ///When a nurse read the tracking end
  static finishJobTracking(String trackingDocId) async {
    await FirebaseFirestore.instance
        .collection(jobTrackingCollection)
        .doc(trackingDocId)
        .delete();
  }

  static Stream<QuerySnapshot> getMyJobTrackingRooms(String userId) {
    return FirebaseFirestore.instance
        .collection(jobTrackingCollection)
        .where("users", arrayContains: userId.toString())
        .snapshots();
  }

  static Stream<DocumentSnapshot> getJobTrackingRoom(String trackingId) {
    return FirebaseFirestore.instance
        .collection(jobTrackingCollection)
        .doc(trackingId)
        .snapshots();
  }




  ///============================================= CHATS STUFF ======================================================

  static createChatRoomMessage(var chatId, String lastMessage, var serverChatId, Map<String, dynamic> message, User user) async {
    var doc  = FirebaseFirestore.instance.collection(chatsCollection).doc(chatId);
    message['time'] = DateTime.now().millisecondsSinceEpoch;

    await doc.collection(chatRoom).doc().set(message).then((value) {
      Map<String, dynamic> currentUser = new Map<String, dynamic>();

      currentUser['id'] = user.id.toString();
      currentUser['name'] = user.name.toString();
      currentUser['image'] = user.image.toString();

      doc.update({
      'updatedAt': DateTime.now().millisecondsSinceEpoch,
        '${user.id}': currentUser,
        'serverChatId': serverChatId.toString(),
        'lastMessage': lastMessage,
        'lastBy': user.id.toString()
      });
    },
        onError: (e){
          print(e);
        });

  }

  static createUserChatRoom(var chatId, Map<String, dynamic> data) async  {
    await FirebaseFirestore.instance
        .collection(chatsCollection)
        .doc(chatId)
        .set(data);
  }

  static Stream<QuerySnapshot> getChatMessages(var chatId) {
    return FirebaseFirestore.instance
        .collection(chatsCollection)
        .doc(chatId)
        .collection(chatRoom)
    .orderBy("time")
        .snapshots();
  }

  static Stream<QuerySnapshot> getChatsList(var userId) {
    return FirebaseFirestore.instance
        .collection(chatsCollection)
        .where("users", arrayContains: userId.toString())
    .orderBy("updatedAt", descending: true)
        .snapshots();
  }

  static CollectionReference getChatsCollection() {
    return FirebaseFirestore.instance
        .collection(chatsCollection);
  }

  static Future<bool> checkExist(String docID) async {
    bool exists = false;
    try {
      await FirebaseFirestore.instance.collection(chatsCollection).doc("$docID").get().then((doc) {
        if (doc.exists)
          exists = true;
        else
          exists = false;
      });
      return exists;
    } catch (e) {
      return false;
    }
  }

}