import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;

class App {
  BuildContext _context;
  double _height;
  double _width;
  double _heightPadding;
  double _widthPadding;

  App(_context) {
    this._context = _context;
    MediaQueryData _queryData = MediaQuery.of(this._context);
    _height = _queryData.size.height / 100.0;
    _width = _queryData.size.width / 100.0;
    _heightPadding = _height - ((_queryData.padding.top + _queryData.padding.bottom) / 100.0);
    _widthPadding = _width - (_queryData.padding.left + _queryData.padding.right) / 100.0;
  }

  double appHeight(double v) {
    return _height * v;
  }

  double appWidth(double v) {
    return _width * v;
  }

  double appVerticalPadding(double v) {
    return _heightPadding * v;
  }

  double appHorizontalPadding(double v) {
    return _widthPadding * v;
  }
}

class Colors {
  Color _mainColor = Color(0xFF03c9e3);
  Color mainColorLight = Color(0xFF0096c7);

  Color _secondColor = Color(0xFFccccdd);
  Color _accentColor = Color(0xFF9999aa);
  Color _scaffoldColor = Color(0xFFf1f3f6);

 Color _textColor = Color(0xFF7a8aa3);

 Color backgroundLight = Color(0xFFf1f3f6);
 Color backgroundDark = Color(0xFF27282C);

  Color shadowLight = Color(0xFFffffff);
  Color shadowDark = Color(0xFFaabac8);



  Color _splashColor = Color(0xFF121212);

  Color mainColor(double opacity) {
    return this._mainColor.withOpacity(opacity);
  }

  Color secondColor(double opacity) {
    return this._secondColor.withOpacity(opacity);
  }

  Color accentColor(double opacity) {
    return this._accentColor.withOpacity(opacity);
  }

  Color scaffoldColor(double opacity) {
    return _scaffoldColor.withOpacity(opacity);
  }

  Color textColor(double opacity) {
    return _textColor.withOpacity(opacity);
  }

  Color splashColor(double opacity) {
    return this._splashColor.withOpacity(opacity);
  }

  LinearGradient bottomNavGradient(){
    return LinearGradient(
      colors: [
        backgroundLight,
        backgroundLight.withOpacity(0.6),
        backgroundLight.withOpacity(0),
      ],
      stops: [0.4, 0.7, 1],
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter
    );
  }

  List<BoxShadow> containerShadow() {
    return [
      BoxShadow(
          blurRadius: 12,
          color: shadowDark,
          offset: Offset(5, 5),
          spreadRadius: -6),
      BoxShadow(
          blurRadius: 12,
          color: shadowLight,
          offset: Offset(-5, -5),
          spreadRadius: -5),
    ];
  }

  List<BoxShadow> textContainerShadow() {
    return [
      BoxShadow(
          blurRadius: 10,
          color: shadowLight,
          offset: Offset(5, 5),
          spreadRadius: -5),
      BoxShadow(
          blurRadius: 10,
          color: shadowDark,
          offset: Offset(-5, -5),
          spreadRadius: -10),
    ];
  }

  Paint textForeground(){
    return Paint()..shader = LinearGradient(
      colors: <Color>[_mainColor, mainColorLight],
    ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));
  }

  LinearGradient iconGradient() {
    return LinearGradient(
      colors: [
        _mainColor,
        mainColorLight
      ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    );
  }

  Widget appBar(BuildContext context, String title) {
    return material.AppBar(
      title: Text(title, style: material.Theme.of(context).textTheme.headline5,),
      elevation: 0,
      backgroundColor: material.Theme.of(context).scaffoldBackgroundColor,
      centerTitle: true,
      leading: material.IconButton(
        onPressed: (){ Navigator.of(context).pop(); },
        icon: Icon(CupertinoIcons.back, color: material.Theme.of(context).accentColor),
      ),
    );
  }



}